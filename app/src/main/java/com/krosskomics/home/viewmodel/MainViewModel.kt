package com.krosskomics.home.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import com.krosskomics.common.model.Banner
import com.krosskomics.common.model.InitSet
import com.krosskomics.common.model.Main
import com.krosskomics.common.model.ReadEp
import com.krosskomics.common.viewmodel.BaseViewModel
import com.krosskomics.home.repository.MainRepository


class MainViewModel(application: Application): BaseViewModel(application) {
    private val repository = MainRepository()
    private val initSetResponseLiveData = repository.getVolumesResponseLiveData()
    private val mainResponseLiveData = repository.getMainResponseLiveData()
    private val mainBannerResponseLiveData = repository.getMainBannerResponseLiveData()
    private val readEpResponseLiveData = repository.getReadEpResponseLiveData()

    fun requestInitSet() {
        repository.requestInitSet(getApplication())
    }

    override fun requestMain() {
        repository.requestMain(getApplication())
    }

    fun requestMainBannerApi() {
        repository.requestMainBannerApi(getApplication())
    }

    fun requestReadEpApi() {
        repository.requestReadEpApi(getApplication())
    }

    fun getInitSetResponseLiveData(): LiveData<InitSet> {
        return initSetResponseLiveData
    }

    override fun getMainResponseLiveData(): LiveData<Any> {
        return mainResponseLiveData
    }

    fun getMainBannerResponseLiveData(): LiveData<Banner> {
        return mainBannerResponseLiveData
    }

    fun getReadEpResponseLiveData(): LiveData<ReadEp> {
        return readEpResponseLiveData
    }
}