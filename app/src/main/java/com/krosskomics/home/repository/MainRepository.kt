package com.krosskomics.home.repository

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.krosskomics.KJKomicsApp
import com.krosskomics.common.model.Banner
import com.krosskomics.common.model.InitSet
import com.krosskomics.common.model.Main
import com.krosskomics.common.model.ReadEp
import com.krosskomics.common.repository.CommonRepository
import com.krosskomics.util.CODE
import com.krosskomics.util.CommonUtil
import com.krosskomics.util.ServerUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainRepository : CommonRepository(){
    private lateinit var initSetLiveData: MutableLiveData<InitSet>
    private lateinit var mainBannerLiveData: MutableLiveData<Banner>
    private lateinit var readEpLiveData: MutableLiveData<ReadEp>

    fun requestInitSet(context: Context) {
        if (CommonUtil.read(context, CODE.LOCAL_token, "").isNullOrEmpty()) {
            checkFCMToken(context)
            return
        }
        val api: Call<InitSet> = ServerUtil.service.getInitSetApi(
            CommonUtil.read(context, CODE.CURRENT_LANGUAGE, "en"),
            CommonUtil.read(context, CODE.LOCAL_token, ""),
            CommonUtil.read(context, CODE.LOCAL_REF_SOURCE, ""),
            advid = KJKomicsApp.AD_Id
        )
        api.enqueue(object : Callback<InitSet> {
            override fun onResponse(call: Call<InitSet>, response: Response<InitSet>) {
                if (response.body() != null) {
                    initSetLiveData.postValue(response.body());
                }
            }

            override fun onFailure(call: Call<InitSet>, t: Throwable) {
                initSetLiveData.postValue(null)
            }
        })
    }

    fun requestMain(context: Context) {
        val api: Call<Main> = ServerUtil.service.getMainApi(
            CommonUtil.read(context, CODE.CURRENT_LANGUAGE, "en")
        )
        api.enqueue(object : Callback<Main> {
            override fun onResponse(call: Call<Main>, response: Response<Main>) {
                if (response.body() != null) {
                    mainLiveData.postValue(response.body());
                }
            }

            override fun onFailure(call: Call<Main>, t: Throwable) {
                mainLiveData.postValue(null)
            }
        })
    }

    fun requestMainBannerApi(context: Context) {
        val api: Call<Banner> = ServerUtil.service.getMainBannerApi(
            CommonUtil.read(context, CODE.CURRENT_LANGUAGE, "en")
        )
        api.enqueue(object : Callback<Banner> {
            override fun onResponse(call: Call<Banner>, response: Response<Banner>) {
                if (response.body() != null) {
                    mainBannerLiveData.postValue(response.body());
                }
            }

            override fun onFailure(call: Call<Banner>, t: Throwable) {
                mainBannerLiveData.postValue(null)
            }
        })
    }

    fun requestReadEpApi(context: Context) {
        val api: Call<ReadEp> = ServerUtil.service.getReadEpApi(
            CommonUtil.read(context, CODE.CURRENT_LANGUAGE, "en")
        )
        api.enqueue(object : Callback<ReadEp> {
            override fun onResponse(call: Call<ReadEp>, response: Response<ReadEp>) {
                if (response.body() != null) {
                    readEpLiveData.postValue(response.body());
                }
            }

            override fun onFailure(call: Call<ReadEp>, t: Throwable) {
                readEpLiveData.postValue(null)
            }
        })
    }

    fun getVolumesResponseLiveData(): LiveData<InitSet> {
        initSetLiveData = MutableLiveData()
        return initSetLiveData
    }

    fun getMainBannerResponseLiveData(): LiveData<Banner> {
        mainBannerLiveData = MutableLiveData()
        return mainBannerLiveData
    }

    fun getReadEpResponseLiveData(): LiveData<ReadEp> {
        readEpLiveData = MutableLiveData()
        return readEpLiveData
    }

    private fun checkFCMToken(context: Context) {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                val token = task.result?.token
                CommonUtil.write(context, CODE.LOCAL_token, token)
                requestInitSet(context)
            })
    }
}