package com.krosskomics.home.adapter

import android.content.Context
import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.krosskomics.util.CommonUtil

class HomeBTypeDecoration(context: Context) : RecyclerView.ItemDecoration() {
    var centerMargin = 0
    init {
        centerMargin = CommonUtil.dpToPx(context, 0)
    }
    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)

        // 가운데 여백
        // spanIndex = 0 -> 왼쪽
        // spanIndex = 1 -> 가운데
        // spanIndex = 2 -> 오른쪽
        var lp = view.layoutParams as GridLayoutManager.LayoutParams
        var spanIndex = lp.spanIndex

        when (spanIndex) {
            0 -> outRect.right = centerMargin
        }
    }
}