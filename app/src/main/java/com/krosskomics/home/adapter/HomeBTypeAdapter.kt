package com.krosskomics.home.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.krosskomics.R
import com.krosskomics.common.data.DataBook
import com.krosskomics.common.data.DataMainContents
import com.krosskomics.common.holder.BaseItemViewHolder
import com.krosskomics.event.activity.EventActivity
import com.krosskomics.mainmenu.activity.MainMenuActivity
import com.krosskomics.more.activity.MoreActivity
import com.krosskomics.util.CommonUtil
import pl.looksoft.shadowlib.ShadowLayout

class HomeBTypeAdapter(private val item: DataMainContents, private val layoutRes: Int, private val context: Context) :
    RecyclerView.Adapter<HomeBTypeAdapter.HomeETypeViewHolder>() {

    enum class VIEW_TYPE {
        VIEW_TYPE_A, VIEW_TYPE_B
    }

    private var onClickListener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeBTypeAdapter.HomeETypeViewHolder {
        return when(viewType) {
            VIEW_TYPE.VIEW_TYPE_A.ordinal ->
                HomeETypeViewHolder(LayoutInflater.from(parent.context).inflate(layoutRes, parent, false))
            VIEW_TYPE.VIEW_TYPE_B.ordinal ->
                HomeETypeViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_main_type_b_2, parent, false))
            else ->
                HomeETypeViewHolder(LayoutInflater.from(parent.context).inflate(layoutRes, parent, false))
        }
    }

    override fun getItemCount(): Int {
        return item.list?.size ?: 1
    }

    override fun onBindViewHolder(holder: HomeETypeViewHolder, position: Int) {
        holder.setData(item.list?.get(position), position)
    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0, 1 -> VIEW_TYPE.VIEW_TYPE_A.ordinal
            else -> VIEW_TYPE.VIEW_TYPE_B.ordinal
        }
    }

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener) {
        this.onClickListener = onItemClickListener
    }

    interface OnItemClickListener {
        fun onItemClick(item: Any?)
    }

    inner class HomeETypeViewHolder(itemView: View) : BaseItemViewHolder(itemView) {
        val shadowView = itemView.findViewById<ShadowLayout>(R.id.shadowView)
        val mainImageView = itemView.findViewById<ImageView>(R.id.mainImageView)
        val dimView = itemView.findViewById<View>(R.id.dimView)

        override fun setData(data: Any?, position: Int) {
            if (data is DataBook) {
                itemView.setOnClickListener {
                    onClickListener?.onItemClick(item.list?.get(position))
                }
                when (position) {
                    0, 1 -> {
                        val width = CommonUtil.getDeviceWidth(itemView.context) / 2 - CommonUtil.dpToPx(context, 18)
                        val param = shadowView.layoutParams as FrameLayout.LayoutParams
                        param.width = width
                        param.height = width
                        shadowView.layoutParams = param
                    }
                    else -> {
                        val width = CommonUtil.getDeviceWidth(itemView.context) / 4 - CommonUtil.dpToPx(context, 9)
                        val param = shadowView.layoutParams as FrameLayout.LayoutParams
                        param.width = width
                        param.height = width
                        shadowView.layoutParams = param
                    }
                }
                val roundCorner = mainImageView?.tag.toString()
                CommonUtil.setGlideImageRound(itemView.context, data.image ?: "", mainImageView, roundCorner.toInt())
                if (item.list?.size ?: 0 > 5 && position == 5) {
                    dimView?.visibility = View.VISIBLE
                    dimView.setOnClickListener {
                        when (item.more_type) {
                            //L : 기존 리스트 화면이동, W:기다무 메뉴오픈, O:연재 메뉴 오픈, R:랭킹 메뉴 오픈, G:장르 메뉴 오픈, E: 이벤트 화면 이동.
                            "L" -> {
                                val intent = Intent(itemView.context, MoreActivity::class.java)
                                intent.putExtra("title", item.layout_title)
                                intent.putExtra("more_param", item.more_param)
                                intent.putExtra("listType", "more")
                                context.startActivity(intent)
                            }
                            "W" -> {
                                val intent = Intent(context, MainMenuActivity::class.java).apply {
                                    putExtra("tabIndex", 1)
                                }
                                context.startActivity(intent)
                            }
                            "O" -> {
                                val intent = Intent(context, MainMenuActivity::class.java).apply {
                                    putExtra("tabIndex", 0)
                                }
                                context.startActivity(intent)
                            }
                            "R" -> {
                                val intent = Intent(context, MainMenuActivity::class.java).apply {
                                    putExtra("tabIndex", 3)
                                }
                                context.startActivity(intent)
                            }
                            "R" -> {
                                val intent = Intent(context, MainMenuActivity::class.java).apply {
                                    putExtra("tabIndex", 2)
                                }
                                context.startActivity(intent)
                            }
                            "E" -> {
                                val intent = Intent(itemView.context, EventActivity::class.java)
                                context.startActivity(intent)
                            }
                        }
                    }
                } else {
                    dimView?.visibility = View.GONE
                }
            }
        }
    }
}