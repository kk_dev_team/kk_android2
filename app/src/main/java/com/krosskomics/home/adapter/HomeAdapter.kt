package com.krosskomics.home.adapter

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.facebook.drawee.view.SimpleDraweeView
import com.krosskomics.R
import com.krosskomics.coin.activity.CoinActivity
import com.krosskomics.common.data.DataBook
import com.krosskomics.common.data.DataMainContents
import com.krosskomics.common.holder.BaseItemViewHolder
import com.krosskomics.event.activity.EventActivity
import com.krosskomics.home.activity.MainActivity
import com.krosskomics.invite.activity.InviteActivity
import com.krosskomics.mainmenu.activity.MainMenuActivity
import com.krosskomics.more.activity.MoreActivity
import com.krosskomics.notice.activity.NoticeActivity
import com.krosskomics.series.activity.SeriesActivity
import com.krosskomics.util.CODE
import com.krosskomics.util.CommonUtil
import com.krosskomics.webview.MopubWebViewActivity
import com.krosskomics.webview.WebViewActivity
import kotlinx.android.synthetic.main.activity_main_content.*

class HomeAdapter(private val items: ArrayList<*>) : RecyclerView.Adapter<HomeAdapter.HomeViewHolder>() {

    lateinit var context: Context

    enum class VIEW_TYPE {
//        lp:큰포스터, ts:6개세트, sp:작은포스터 re:직사각형이벤트, se:정사각형이벤트
        VIEW_TYPE_A, VIEW_TYPE_B, VIEW_TYPE_C, VIEW_TYPE_D, VIEW_TYPE_E, VIEW_TYPE_LINE, VIEW_TYPE_FOOTER
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder {
        context = parent.context
        var view = LayoutInflater.from(parent.context).inflate(R.layout.item_main_content_a, parent, false)
        when (viewType) {
            VIEW_TYPE.VIEW_TYPE_A.ordinal -> {
                view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_main_content_a, parent, false)
            }
            VIEW_TYPE.VIEW_TYPE_B.ordinal -> {
                view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_main_content_b, parent, false)
            }
            VIEW_TYPE.VIEW_TYPE_C.ordinal -> {
                view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_main_content_c, parent, false)
            }
            VIEW_TYPE.VIEW_TYPE_D.ordinal -> {
                view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_main_content_d, parent, false)
            }
            VIEW_TYPE.VIEW_TYPE_E.ordinal -> {
                view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_main_content_e, parent, false)
            }
            VIEW_TYPE.VIEW_TYPE_LINE.ordinal -> {
                view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_underbar, parent, false)
            }
            VIEW_TYPE.VIEW_TYPE_FOOTER.ordinal -> {
                view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.view_home_footer, parent, false)
            }
        }
        return HomeViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: HomeViewHolder, position: Int) {
        holder.setData(items[position], position)
    }

    override fun getItemViewType(position: Int): Int {
//        lp:큰포스터, ts:6개세트, sp:작은포스터 re:직사각형이벤트, se:정사각형이벤트
        val item = items[position]
        if (item is DataMainContents) {
            when (item.layout_type) {
                "lp" -> return VIEW_TYPE.VIEW_TYPE_A.ordinal
                "ts" -> return VIEW_TYPE.VIEW_TYPE_B.ordinal
                "sp" -> return VIEW_TYPE.VIEW_TYPE_C.ordinal
                "re" -> return VIEW_TYPE.VIEW_TYPE_D.ordinal
                "se" -> return VIEW_TYPE.VIEW_TYPE_E.ordinal
                "line" -> return VIEW_TYPE.VIEW_TYPE_LINE.ordinal
                "footer" -> return VIEW_TYPE.VIEW_TYPE_FOOTER.ordinal
            }
        }
        return VIEW_TYPE.VIEW_TYPE_A.ordinal
    }

    private fun setATypeView(list: ArrayList<DataBook>?, layout: LinearLayout) {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        if (null != list && 0 < list.size) {
            for (i in list.indices) {
                val item = list[i]
                val view = inflater.inflate(R.layout.item_main_type_a, null) as LinearLayout

                val mainImageView = view.findViewById<ImageView>(R.id.iv_main)

                val titleTextView = view.findViewById<TextView>(R.id.tv_title)
                val genreTextView = view.findViewById<TextView>(R.id.tv_genre)
                val upTagImageView = view.findViewById<ImageView>(R.id.upTagImageView)
                val newTagImageView = view.findViewById<ImageView>(R.id.newTagImageView)
                val remainTagView = view.findViewById<View>(R.id.remainTagView)
                val remainTagTextView = view.findViewById<TextView>(R.id.remainTagTextView)

                titleTextView.text = item.title
                genreTextView.text = item.genre1

                val roundCorner = mainImageView.tag.toString()
                CommonUtil.setGlideImageRound(context, item.image ?: "", mainImageView, roundCorner.toInt())

                if (item.isnew.equals("1")) {
                    newTagImageView.visibility = View.VISIBLE
                    upTagImageView.visibility = View.GONE
                } else {
                    newTagImageView.visibility = View.GONE
                    if (item.isupdate == "1") {
                        upTagImageView.visibility = View.VISIBLE
                    } else {
                        upTagImageView.visibility = View.GONE
                    }
                }

                if (item.dp_wop_term.isNullOrEmpty()) {
                    if (item.dp_pub_day.isNullOrEmpty()) {
                        remainTagView.visibility = View.GONE
                    } else if (item.dp_pub_day?.isNotEmpty() == true) {
                        remainTagView.visibility = View.VISIBLE
                        remainTagTextView.text = item.dp_pub_day
                        remainTagView.isSelected = true
                    }
                } else {
                    remainTagView.visibility = View.VISIBLE
                    remainTagTextView.text = item.dp_wop_term
                    remainTagView.isSelected = false
                }

                view.setOnClickListener {
                    val intent = Intent(context, SeriesActivity::class.java)
                    val b = Bundle()
                    b.putString("sid", item.sid)
                    b.putString("title", item.title)
                    intent.putExtras(b)
                    context.startActivity(intent)
                }

                layout.addView(view)
            }
        }
    }

    private fun setCTypeView(list: ArrayList<DataBook>?, layout: LinearLayout) {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        if (null != list && 0 < list.size) {
            for (i in list.indices) {
                val item = list[i]
                val view = inflater.inflate(R.layout.item_main_type_c, null) as LinearLayout

                val mainImageView = view.findViewById<SimpleDraweeView>(R.id.iv_main)

                val titleTextView = view.findViewById<TextView>(R.id.tv_title)

                val genreTextView = view.findViewById<TextView>(R.id.tv_genre)
                val upTagImageView = view.findViewById<ImageView>(R.id.upTagImageView)
                val newTagImageView = view.findViewById<ImageView>(R.id.newTagImageView)
                val remainTagView = view.findViewById<View>(R.id.remainTagView)
                val remainTagTextView = view.findViewById<TextView>(R.id.remainTagTextView)

                titleTextView.text = item.title

                mainImageView.controller =
                    CommonUtil.getDraweeController(context, item.image, 200, 200)

                if (item.writer1.isNullOrEmpty()) {
                    genreTextView.visibility = View.GONE
                } else {
                    genreTextView.text = item.genre1
                    genreTextView.visibility = View.VISIBLE
                }

                if (item.isnew.equals("1")) {
                    newTagImageView.visibility = View.VISIBLE
                    upTagImageView.visibility = View.GONE
                } else {
                    newTagImageView.visibility = View.GONE
                    if (item.isupdate == "1") {
                        upTagImageView.visibility = View.VISIBLE
                    } else {
                        upTagImageView.visibility = View.GONE
                    }
                }

                if (item.dp_wop_term.isNullOrEmpty()) {
                    if (item.dp_pub_day.isNullOrEmpty()) {
                        remainTagView.visibility = View.GONE
                    } else if (item.dp_pub_day?.isNotEmpty() == true) {
                        remainTagView.visibility = View.VISIBLE
                        remainTagTextView.text = item.dp_pub_day
                        remainTagView.isSelected = true
                    }
                } else {
                    remainTagView.visibility = View.VISIBLE
                    remainTagTextView.text = item.dp_wop_term
                    remainTagView.isSelected = false
                }

                view.setOnClickListener {
                    val intent = Intent(context, SeriesActivity::class.java)
                    val b = Bundle()
                    b.putString("sid", item.sid)
                    b.putString("title", item.title)
                    intent.putExtras(b)
                    context.startActivity(intent)
                }

                layout.addView(view)
            }
        }
    }

    /**
     * 이벤트
     *
     * @param list
     * @param layEvent
     */
    private fun setDTypeView(list: ArrayList<DataBook>?, layEvent: LinearLayout) {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        try {
            if (null != list && 0 < list.size) {
                for (i in list.indices) {
                    val item = list[i]
                    val view = inflater.inflate(R.layout.item_main_type_d, null) as LinearLayout
                    val ivEvent = view.findViewById<SimpleDraweeView>(R.id.iv_event)
                    ivEvent.controller = CommonUtil.getDraweeController(context, item.image, 200, 200)

                    layEvent.addView(view)

                    view.setOnClickListener {
                        // M:메인, H:작품홈, C:충전(인앱), W:웹뷰, B:브라우저, N:없슴
                        val intent: Intent
                        when (item.atype) {
                            "M" -> {
                            }
                            "H" -> {
                                intent = Intent(context, SeriesActivity::class.java)
                                val b = Bundle()
                                b.putString("sid", item.sid)
                                b.putString("title", item.title)
                                intent.putExtras(b)
                                context.startActivity(intent)
//                                val activity = BookActivity.activity
//                                activity?.finish()
                            }
                            "C" -> if (CommonUtil.read(context, CODE.LOCAL_loginYn, "N").equals("Y", ignoreCase = true)) {
                                intent = Intent(context, CoinActivity::class.java)
                                context.startActivity(intent)
                            } else {
                                (context as MainActivity).goLoginAlert(context)
                            }
                            "W" -> if ("" != item.link) {
                                intent = Intent(context, WebViewActivity::class.java)
                                intent.putExtra("title", item.subject)
                                intent.putExtra("url", item.link)
                                context.startActivity(intent)
                            }
                            "G" -> if ("" != item.link) {
                                intent = Intent(context, MopubWebViewActivity::class.java)
                                intent.putExtra("title", item.subject)
                                intent.putExtra("url", item.link)
                                context.startActivity(intent)
                            }
                            "B" -> if ("" != item.link) {
                                CommonUtil.moveBrowserChrome(context, item.link)
                            }
                            "S" -> if (!CommonUtil.read(context, CODE.LOCAL_loginYn, "N")
                                    .equals("Y", ignoreCase = true)
                            ) {
                                CommonUtil.moveSignUp(context)
                            }
                            "N" -> {
                                intent = Intent(context, NoticeActivity::class.java)
                                intent.putExtra("nseq", item.nseq)
                                context.startActivity(intent)
                            }
                            "I" -> {
                                intent = Intent(context, InviteActivity::class.java)
                                context.startActivity(intent)
                            }
                        }
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun setBTypeView(item: DataMainContents?, recyclerView: RecyclerView) {
        item?.let {
            val layoutManager = GridLayoutManager(context, 4)
            layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    return when (position) {
                        0, 1 -> 2
                        else -> 1
                    }
                }
            }
            recyclerView.layoutManager = layoutManager
//            recyclerView.addItemDecoration(HomeBTypeDecoration(context))
            recyclerView.adapter = HomeBTypeAdapter(it, R.layout.item_main_type_b_1, context)
            (recyclerView.adapter as HomeBTypeAdapter).setOnItemClickListener(object : HomeBTypeAdapter.OnItemClickListener {
                override fun onItemClick(item: Any?) {
                    if (item is DataBook) {
                        val intent = Intent(context, SeriesActivity::class.java).apply {
                            putExtra("sid", item.sid)
                            putExtra("title", item.title)
                        }
                        context.startActivity(intent)
                    }
                }
            })
        }
    }

    /**
     * 이벤트
     *
     * @param list
     * @param layEvent
     */
    private fun setETypeView(list: ArrayList<DataBook>?, layEvent: LinearLayout) {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        try {
            if (null != list && 0 < list.size) {
                for (i in list.indices) {
                    val item = list[i]
                    val view = inflater.inflate(R.layout.item_main_type_e, null) as LinearLayout
                    val ivEvent = view.findViewById<SimpleDraweeView>(R.id.iv_event)
                    ivEvent.controller = CommonUtil.getDraweeController(context, item.image, 200, 200)

                    layEvent.addView(view)

                    view.setOnClickListener {
                        // M:메인, H:작품홈, C:충전(인앱), W:웹뷰, B:브라우저, N:없슴
                        val intent: Intent
                        when (item.atype) {
                            "M" -> {
                            }
                            "H" -> {
                                intent = Intent(context, SeriesActivity::class.java)
                                val b = Bundle()
                                b.putString("sid", item.sid)
                                b.putString("title", item.title)
                                intent.putExtras(b)

                                val eventName = "af_content_view"
                                val eventValue: MutableMap<String, Any?> = HashMap()
                                eventValue["af_content"] = item.title.toString() + " (" + CommonUtil.read(context, CODE.CURRENT_LANGUAGE, "en") + ")"
                                eventValue["af_content_id"] = item.sid
                                CommonUtil.setAppsFlyerEvent(context, eventName, eventValue)

                                context.startActivity(intent)
                            }
                            "C" -> if (CommonUtil.read(context, CODE.LOCAL_loginYn, "N").equals("Y", ignoreCase = true)) {
                                intent = Intent(context, CoinActivity::class.java)
                                context.startActivity(intent)
                            } else {
                                (context as MainActivity).goLoginAlert(context)
                            }
                            "W" -> if ("" != item.link) {
                                intent = Intent(context, WebViewActivity::class.java)
                                intent.putExtra("title", item.subject)
                                intent.putExtra("url", item.link)
                                context.startActivity(intent)
                            }
                            "G" -> if ("" != item.link) {
                                intent = Intent(context, MopubWebViewActivity::class.java)
                                intent.putExtra("title", item.subject)
                                intent.putExtra("url", item.link)
                                context.startActivity(intent)
                            }
                            "B" -> if ("" != item.link) {
                                CommonUtil.moveBrowserChrome(context, item.link)
                            }
                            "S" -> if (!CommonUtil.read(context, CODE.LOCAL_loginYn, "N")
                                    .equals("Y", ignoreCase = true)
                            ) {
                                CommonUtil.moveSignUp(context)
                            }
                            "N" -> {
                                intent = Intent(context, NoticeActivity::class.java)
                                intent.putExtra("nseq", item.nseq)
                                context.startActivity(intent)
                            }
                            "I" -> {
                                intent = Intent(context, InviteActivity::class.java)
                                context.startActivity(intent)
                            }
                        }
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    inner class HomeViewHolder(itemView: View) : BaseItemViewHolder(itemView) {
        val titleTextView = itemView.findViewById<TextView>(R.id.tv_title)
        val moreButton = itemView.findViewById<ImageView>(R.id.moreImageView)
        val contentLayout = itemView.findViewById<LinearLayout>(R.id.lay_user_update)
        val eventLayout = itemView.findViewById<LinearLayout>(R.id.lay_event)
        val recyclerView = itemView.findViewById<RecyclerView>(R.id.recyclerView)

        val subTitleTextView = itemView.findViewById<TextView>(R.id.subTitleTextView)

        override fun setData(item: Any?, position: Int) {
            if (item is DataMainContents) {
                titleTextView?.text = item.layout_title
                subTitleTextView?.text = item.layout_desc
                if ("1" == item.show_more) {
                    moreButton?.visibility = View.VISIBLE
                    moreButton?.setOnClickListener {
                        when (item.more_type) {
                            //L : 기존 리스트 화면이동, W:기다무 메뉴오픈, O:연재 메뉴 오픈, R:랭킹 메뉴 오픈, G:장르 메뉴 오픈, E: 이벤트 화면 이동.
                            "L" -> {
                                val intent = Intent(itemView.context, MoreActivity::class.java)
                                intent.putExtra("title", item.layout_title)
                                intent.putExtra("more_param", item.more_param)
                                intent.putExtra("listType", "more")
                                context.startActivity(intent)
                            }
                            "W" -> {
                                val intent = Intent(context, MainMenuActivity::class.java).apply {
                                    putExtra("tabIndex", 1)
                                }
                                context.startActivity(intent)
                            }
                            "O" -> {
                                val intent = Intent(context, MainMenuActivity::class.java).apply {
                                    putExtra("tabIndex", 0)
                                }
                                context.startActivity(intent)
                            }
                            "R" -> {
                                val intent = Intent(context, MainMenuActivity::class.java).apply {
                                    putExtra("tabIndex", 3)
                                }
                                context.startActivity(intent)
                            }
                            "R" -> {
                                val intent = Intent(context, MainMenuActivity::class.java).apply {
                                    putExtra("tabIndex", 2)
                                }
                                context.startActivity(intent)
                            }
                            "E" -> {
                                val intent = Intent(itemView.context, EventActivity::class.java)
                                context.startActivity(intent)
                            }
                        }
                    }
                } else {
                    moreButton?.visibility = View.GONE
                }
                when (getItemViewType(position)) {
                    VIEW_TYPE.VIEW_TYPE_A.ordinal -> {
                        setATypeView(item.list, contentLayout)
                    }
                    VIEW_TYPE.VIEW_TYPE_B.ordinal -> {
                        setBTypeView(item, recyclerView)
                    }
                    VIEW_TYPE.VIEW_TYPE_C.ordinal -> {
                        setCTypeView(item.list, contentLayout)
                    }
                    VIEW_TYPE.VIEW_TYPE_D.ordinal -> {
                        setDTypeView(item.list, eventLayout)
                    }
                    VIEW_TYPE.VIEW_TYPE_E.ordinal -> {
                        setETypeView(item.list, eventLayout)
                    }
                    VIEW_TYPE.VIEW_TYPE_FOOTER.ordinal -> {
                        itemView.setOnClickListener {
                            (context as MainActivity).nestedScrollView?.scrollTo(0, 0)
                        }
                    }
                }
            }
        }
    }
}