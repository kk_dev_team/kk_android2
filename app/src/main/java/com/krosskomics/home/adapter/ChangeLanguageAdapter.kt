package com.krosskomics.home.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.krosskomics.R
import com.krosskomics.data.DataLanguage
import com.krosskomics.util.setStyle

class ChangeLanguageAdapter(val items: MutableList<DataLanguage>?) :
    RecyclerView.Adapter<ChangeLanguageAdapter.MainViewHolder>() {

    private var onClickListener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_change_language, null)
        return MainViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items?.size ?: 0
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        items?.let {
            val item = it[position]
            holder.languageTextView.text = item.dp_lang
            holder.languageTextView.isSelected = item.isSelect
            if (holder.languageTextView.isSelected) {
                holder.languageTextView.setStyle(1)
            } else {
                holder.languageTextView.setStyle(0)
            }
            when (position) {
                0 -> {
                    if (holder.languageTextView.isSelected) {
                        holder.languageTextView.setBackgroundResource(R.drawable.kk_language_box_on)
                    } else {
                        holder.languageTextView.setBackgroundResource(R.drawable.kk_language_box_off_left)
                    }
                }
                items.size - 1 -> {
                    if (holder.languageTextView.isSelected) {
                        holder.languageTextView.setBackgroundResource(R.drawable.kk_language_box_on)
                    } else {
                        holder.languageTextView.setBackgroundResource(R.drawable.kk_language_box_off_right)
                    }
                }
                else -> {
                    if (holder.languageTextView.isSelected) {
                        holder.languageTextView.setBackgroundResource(R.drawable.kk_language_box_on)
                    } else {
                        holder.languageTextView.setBackgroundResource(R.drawable.kk_language_box_off)
                    }
                }
            }
            holder.languageTextView.setOnClickListener { view ->
                this.onClickListener?.onItemClick(view, position)
            }
        }
    }

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener) {
        this.onClickListener = onItemClickListener
    }

    inner class MainViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val languageTextView = itemView.findViewById<TextView>(R.id.languageTextView)
    }

    interface OnItemClickListener {
        fun onItemClick(view: View, position: Int)
    }
}