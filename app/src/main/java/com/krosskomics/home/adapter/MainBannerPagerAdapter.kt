package com.krosskomics.home.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.krosskomics.R
import com.krosskomics.common.data.DataBanner
import com.krosskomics.login.activity.LoginActivity
import com.krosskomics.util.CODE
import com.krosskomics.util.CommonUtil
import java.util.*


class MainBannerPagerAdapter(
    val context: Context?,
    val items: ArrayList<DataBanner>?,
    val isMaxValue: Boolean
) : PagerAdapter() {
    private val TAG = "MainBannerPagerAdapter"

    override fun getCount(): Int {
        return if (isMaxValue) {
            Int.MAX_VALUE
        } else {
            items?.size ?: 0
        }
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view: View =
            LayoutInflater.from(context).inflate(R.layout.item_home_banner, container, false)
        val iv_main: ImageView = view.findViewById(R.id.iv_main)
        if (null != items && 0 < items.size) {
            val pos = position % items.size
            val item: DataBanner = items[pos]
            val params = iv_main.layoutParams

//            iv_main.controller = CommonUtil.getDraweeController(
//                context,
//                item.image,
//                getDeviceWidth(context!!),
//                params.height
//            )
            CommonUtil.setGlideImage(container.context, item.image ?: "", iv_main)

            iv_main.setOnClickListener { // M:메인, H:작품홈, C:충전(인앱), W:웹뷰, B:브라우저, N:없슴
                CommonUtil.setBannerAction(container.context, item)

            }
            container.addView(view, 0)
        }
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
        container.removeView(obj as View?)
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    fun goLoginAlert(context: Context?) {
        val intent = Intent(context, LoginActivity::class.java)
        intent.putExtra("pageType", CODE.LOGIN_MODE)
        context!!.startActivity(intent)
    }
}