package com.krosskomics.mynews.activity

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.krosskomics.KJKomicsApp
import com.krosskomics.R
import com.krosskomics.common.activity.ToolbarTitleActivity
import com.krosskomics.common.model.Default
import com.krosskomics.mynews.viewmodel.MyNewsViewModel
import com.krosskomics.util.CODE
import com.krosskomics.util.CommonUtil
import com.krosskomics.util.ServerUtil
import kotlinx.android.synthetic.main.activity_mynews.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MyNewsActivity : ToolbarTitleActivity() {
    private val TAG = "MyNewsActivity"

    public override val viewModel: MyNewsViewModel by lazy {
        ViewModelProvider(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return MyNewsViewModel(application) as T
            }
        }).get(MyNewsViewModel::class.java)
    }

    override fun getLayoutId(): Int {
        recyclerViewItemLayoutId = R.layout.item_mynews
        return R.layout.activity_mynews
    }

    override fun initTracker() {
        setTracker(getString(R.string.str_my_news))
    }

    override fun initLayout() {
        toolbarTitleString = getString(R.string.str_my_news)
        viewModel.listType = "mynews"
        super.initLayout()

        initPushNotiView()
    }

    private fun initPushNotiView() {
        notiSwitch.isChecked = CommonUtil.read(context, CODE.LOCAL_RECIEVE_PUSH, "1") == "1"
        notiSwitch.setOnCheckedChangeListener { _, isCheck ->
            if (isCheck) {
                requestPushNoti("S")
            } else {
                requestPushNoti("C")
            }
        }
    }

    fun setReadParams(paramString: String) {
        sendReadNewsApi(paramString)
    }

    private fun sendReadNewsApi(param: String) {
        val api: Call<Default> = ServerUtil.service.sendReadNews(
            CommonUtil.read(context, CODE.CURRENT_LANGUAGE, "en"),
            viewModel.listType,
            param
        )
        api.enqueue(object : Callback<Default> {
            override fun onResponse(call: Call<Default>, response: Response<Default>) {
                if (response.body() != null) {
                    KJKomicsApp.IS_GET_NEW_NEWS = false
                }
            }

            override fun onFailure(call: Call<Default>, t: Throwable) {
            }
        })
    }
}