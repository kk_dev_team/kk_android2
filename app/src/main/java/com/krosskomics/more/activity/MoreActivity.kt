package com.krosskomics.more.activity

import android.util.Log
import com.krosskomics.R
import com.krosskomics.common.activity.ToolbarTitleActivity

class MoreActivity : ToolbarTitleActivity() {
    private val TAG = "MoreActivity"

    override fun getLayoutId(): Int {
        recyclerViewItemLayoutId = R.layout.item_more
        return R.layout.activity_more
    }

    override fun initTracker() {
        setTracker(getString(R.string.str_more))
    }

    override fun initModel() {
        intent?.extras?.apply {
            viewModel.listType = getString("listType").toString()
            viewModel.param2 = getString("more_param").toString()
        }
        super.initModel()
    }

    override fun onResume() {
        super.onResume()
        Log.e(TAG, "onResume viewModel.isFirstEnter : " + viewModel.isFirstEnter)
        if (!viewModel.isFirstEnter) {
            viewModel.page = 1
            viewModel.isRefresh = true
            requestServer()
        }
    }
}