package com.krosskomics.webview

import android.annotation.TargetApi
import android.graphics.Bitmap
import android.os.Build
import android.os.Message
import android.util.Log
import android.webkit.*
import android.webkit.WebView.WebViewTransport
import com.krosskomics.KJKomicsApp
import com.krosskomics.R
import com.krosskomics.common.activity.ToolbarTitleActivity
import com.krosskomics.common.model.Game
import com.krosskomics.util.CODE
import com.krosskomics.util.CommonUtil
import com.krosskomics.util.CommonUtil.getAppVersion
import com.krosskomics.util.CommonUtil.read
import com.krosskomics.util.ServerUtil
import com.mopub.common.MoPub
import com.mopub.common.MoPubReward
import com.mopub.common.SdkConfiguration
import com.mopub.common.SdkInitializationListener
import com.mopub.mobileads.MoPubErrorCode
import com.mopub.mobileads.MoPubInterstitial
import com.mopub.mobileads.MoPubRewardedAdListener
import com.mopub.mobileads.MoPubRewardedAds
import kotlinx.android.synthetic.main.activity_webview.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import android.webkit.ValueCallback





class MopubWebViewActivity : ToolbarTitleActivity(), MoPubInterstitial.InterstitialAdListener {
    private val TAG = "WebViewActivity"

    var webUrl = ""

    // mopup
    val AD_UNIT_ID = "dc99aeae781c4fbe803c81167cf79082"
//    val AD_UNIT_ID = "24534e1901884e398f1253216226017e"
    val AD_REWARD_UNIT_ID = "60df0403723e4f3d8ac5f49c7eeb09f2"
//    val AD_REWARD_UNIT_ID = "920b6145fb1546cf8b5cf2ac34638bb7"
    private var mInterstitial: MoPubInterstitial? = null

    private val rewardedAdListener: MoPubRewardedAdListener? = object : MoPubRewardedAdListener {
        override fun onRewardedAdClicked(adUnitId: String) {
        }

        override fun onRewardedAdClosed(adUnitId: String) {
            webView.evaluateJavascript(
                "var event = new CustomEvent(\"closeRewardAd\", {\n detail: {\n item : '$i'\n }\n});\nwindow.dispatchEvent(event);\n",
                null
            )
        }

        override fun onRewardedAdCompleted(adUnitIds: Set<String?>, reward: MoPubReward) {
            webView.evaluateJavascript(
                "var event = new CustomEvent(\"completeRewardAd\", {\n detail: {\n item : '$i'\n }\n});\nwindow.dispatchEvent(event);\n",
                null
            )
            t = "ar"
            sendGameApi()
        }

        override fun onRewardedAdLoadFailure(adUnitId: String, errorCode: MoPubErrorCode) {
            webView.evaluateJavascript(
                "var event = new CustomEvent(\"resultCheckRewardAd\", {\n detail: {\n item : '$i',\n status : -1\n }\n});\nwindow.dispatchEvent(event);\n",
                null
            )
            t = "el"
            e = errorCode.name
            sendGameApi()
        }

        override fun onRewardedAdLoadSuccess(adUnitId: String) {
            webView.evaluateJavascript(
                "var event = new CustomEvent(\"resultCheckRewardAd\", {\n detail: {\n item : '$i',\n status : 0\n }\n});\nwindow.dispatchEvent(event);\n",
                null
            )
        }

        override fun onRewardedAdShowError(adUnitId: String, errorCode: MoPubErrorCode) {
            webView.evaluateJavascript(
                "var event = new CustomEvent(\"resultShowRewardAd\", {\n detail: {\n item : '$i',\n status : -1\n }\n});\nwindow.dispatchEvent(event);\n",
                null)
            t = "es"
            e = errorCode.name
            sendGameApi()
        }

        override fun onRewardedAdStarted(adUnitId: String) {
            webView.evaluateJavascript(
                "var event = new CustomEvent(\"resultShowRewardAd\", {\n detail: {\n item : '$i',\n status : 0\n }\n});\nwindow.dispatchEvent(event);\n",
                null
            )
        }

    }

    /*t : api 구분
    gs(게임시작)
    gc(게임종료)
    ge(게임에러)
    as(광고노출)
    ar(보상광고완료)
    g: game 명 (웹뷰에서전달)
    i: item 명 (웹뷰에서전달)
    a: 광고타입
    R (보상광고)
    I (전명광고)
    i: item 명 (웹뷰에서전달)
    e: 게임오류시 내용 (웹뷰에서 전달)
    s: 게임시작(gs) api의 리턴값*/
//    t=ar&g=game명&s=게임시작시 리턴 start_seq&a=광고타입&i=item명(없으면공백)
    var t: String? = null
    var g: String? = null
    var s: Int? = null
    var a: String? = null
    var i: String? = null
    var e: String? = null

    override fun getLayoutId(): Int {
        return R.layout.activity_webview
    }

    override fun initTracker() {
        setTracker(toolbarTitleString)
    }

    override fun initModel() {
        intent?.apply {
            toolbarTitleString = extras?.getString("title").toString()
            webUrl = extras?.getString("url").toString()
        }
    }

    override fun initMainView() {
        val webViewClientClass: WebViewClientClass =
            WebViewClientClass()

        webView.addJavascriptInterface(
            AndroidBridge(),
            "kk_app"
        )
        webView.webChromeClient = WebViewChromeClient()
        webView.webViewClient = webViewClientClass
        webView.settings.javaScriptEnabled = true
        val headers: MutableMap<String, String?> =
            HashMap()
        headers["KK-APP-KEY"] = CODE.AUTH_KEY
        headers["KK-APP-DEVICEID"] = read(context, CODE.LOCAL_Android_Id, "")
        headers["KK-APP-SECRET"] = read(context, CODE.LOCAL_APP_SECRET, "")
        headers["KK-APP-VERSION"] = getAppVersion(context)
        headers["KK-APP-TOKEN"] = KJKomicsApp.APPTOKEN
        headers["KK-U-TOKEN"] = read(context, CODE.LOCAL_ENC_USER_NO, "")
        headers["KK-LANG"] = read(context, CODE.CURRENT_LANGUAGE, "en")
        headers["KK-APP-DEVICE"] = "google"
        headers["os-version"] = Build.VERSION.RELEASE
        webView.loadUrl(webUrl, headers)

        initMopub()
    }

    override fun onDestroy() {
        super.onDestroy()
        MoPub.onDestroy(this@MopubWebViewActivity)
    }

    private fun initMopub() {
        val sdkConfiguration: SdkConfiguration = SdkConfiguration.Builder(AD_UNIT_ID).build()
        MoPub.initializeSdk(this, sdkConfiguration, initSdkListener())
    }

    private inner class WebViewClientClass : WebViewClient() {
        override fun shouldOverrideUrlLoading(
            view: WebView,
            url: String
        ): Boolean {
            webUrl = url
            try {
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return true
        }

        @TargetApi(Build.VERSION_CODES.N)
        override fun shouldOverrideUrlLoading(
            view: WebView,
            request: WebResourceRequest
        ): Boolean {
            if (!this@MopubWebViewActivity.isFinishing) {
                val url = request.url.toString()
                webUrl = url
                try {
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            return true
        }

        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)
            if (!this@MopubWebViewActivity.isFinishing) {
                showProgress(context)
            }
        }

        override fun onPageFinished(view: WebView, url: String) {
            super.onPageFinished(view, url)
            if (!this@MopubWebViewActivity.isFinishing) {
                hideProgress()
            }
        }

        override fun onReceivedError(
            view: WebView?,
            request: WebResourceRequest?,
            error: WebResourceError?
        ) {
            super.onReceivedError(view, request, error)
            if (!this@MopubWebViewActivity.isFinishing) {
                hideProgress()
            }
        }
    }

    private inner class AndroidBridge {
        //서버 api 에 게임시작 로깅
        @JavascriptInterface
        fun gameStart(arg: String?) {
            Log.e(TAG, "gameStart arg : " + arg)
            t = "gs"
            g = arg
            sendGameApi()
        }

        //광고 Load후 결과를 웹뷰에 evaluateJavascript 함수를 이용 이벤트로 전달
        @JavascriptInterface
        fun checkRewardAd(arg1: String?, arg2: String?) {
            Log.e(TAG, "checkRewardAd arg1 : " + arg1 + ", arg2 : " + arg2)
            g = arg1
            a = "R"
            i = arg2
            //Mopub SDK를 이용 광고 Load후 결과를 웹뷰에 evaluateJavascript 함수를 이용 이벤트로 전달.
            MoPubRewardedAds.setRewardedAdListener(rewardedAdListener);
            MoPubRewardedAds.loadRewardedAd(AD_REWARD_UNIT_ID)
        }

        //보상형/전면광고 노출. 결과를 웹뷰에 evaluateJavascript 함수를 이용 이벤트로 전달. 서버 api에 광고 노출 로깅
        @JavascriptInterface
        fun showRewardAd(arg1: String?, arg2: String?) {
            Log.e(TAG, "showRewardAd arg1 : " + arg1 + ", arg2 : " + arg2)
            //Mopub SDK를 이용 광고 Load후 결과를 웹뷰에 evaluateJavascript 함수를 이용 이벤트로 전달.
            MoPubRewardedAds.showRewardedAd(AD_REWARD_UNIT_ID)
            t = "as"
            g = arg1
            a = "R"
            i = arg2
            sendGameApi()
        }

        //광고 Load후 결과를 웹뷰에 evaluateJavascript 함수를 이용 이벤트로 전달
        @JavascriptInterface
        fun checkInterstitialAd(arg1: String?) {
            Log.e(TAG, "checkInterstitialAd arg1 : " + arg1)
            g = arg1
            //Mopub SDK를 이용 광고 Load후 결과를 웹뷰에 evaluateJavascript 함수를 이용 이벤트로 전달.
            mInterstitial = MoPubInterstitial(this@MopubWebViewActivity, AD_UNIT_ID)
            mInterstitial?.interstitialAdListener = this@MopubWebViewActivity
            mInterstitial?.load()
        }

        //전면광고 노출. 결과를 웹뷰에 evaluateJavascript 함수를 이용 이벤트로 전달. 서버 api에 광고 노출 로깅
        @JavascriptInterface
        fun showInterstitialAd(arg1: String?) {
            Log.e(TAG, "showInterstitialAd arg1 : " + arg1)
            if (mInterstitial?.isReady == true) {
                mInterstitial?.show()
            }
            t = "as"
            g = arg1
            a = "I"
            sendGameApi()
        }

        //게임종료 전달. 앱에서는 서버 api로 게임종료 로깅. 앱에서 웹뷰 종료
        @JavascriptInterface
        fun gameClose(arg1: String?) {
            Log.e(TAG, "gameClose arg1 : " + arg1)
            //Mopub SDK를 이용 광고 Load후 결과를 웹뷰에 evaluateJavascript 함수를 이용 이벤트로 전달.
            t = "gc"
            g = arg1
            sendGameApi()
            finish()
        }

        //게임오류 전달
        @JavascriptInterface
        fun gameError(arg1: String?, arg2: String?) {
            //Mopub SDK를 이용 광고 Load후 결과를 웹뷰에 evaluateJavascript 함수를 이용 이벤트로 전달.
            Log.e(TAG, "gameError arg1 : " + arg1 + "arg2 : " + arg2)
            t = "ge"
            g = arg1
            e = arg2
            sendGameApi()
        }

        //앱에 회원 정보 요청. 결과를 웹뷰에 evaluateJavascript 함수를 이용 이벤트로 전달.
        @JavascriptInterface
        fun getUser(arg1: String?) {
            Log.e(TAG, "getUser arg1 : " + arg1)
            webView.post {
                webView.evaluateJavascript(
                    "var event = new CustomEvent(\"resultUser\", {\n detail: {\n uid : '${read(context, CODE.LOCAL_RID, "")}',\n nick : '${read(context, CODE.LOCAL_Nickname, "")}'\n }\n});\nwindow.dispatchEvent(event);\n",
                    null
                )
            }
            t = "u"
            g = arg1
            s = 0
            sendGameApi()
        }
    }

    private fun initSdkListener(): SdkInitializationListener? {
        return SdkInitializationListener {
            /* MoPub SDK initialized.
               Check if you should show the consent dialog here, and make your ad requests. */
            Log.e(TAG, "mopup SdkInitializationListener")
        }
    }

    private inner class WebViewChromeClient : WebChromeClient() {
        override fun onCreateWindow(
            view: WebView,
            dialog: Boolean,
            userGesture: Boolean,
            resultMsg: Message
        ): Boolean {
            val newWebView = WebView(context)
            val transport = resultMsg.obj as WebViewTransport
            transport.webView = newWebView
            resultMsg.sendToTarget()
            return true
        }

        override fun onJsAlert(
            view: WebView,
            url: String,
            message: String,
            result: JsResult
        ): Boolean {
//            Log.e(TAG, "message : " + message);
            return true
        }
    }

    private fun sendGameApi() {
        val api: Call<Game> = ServerUtil.service.sendGameApi(
            CommonUtil.read(context, CODE.CURRENT_LANGUAGE, "en"),
            t,
            g,
            s,
            a,
            i,
            e
        )
        api.enqueue(object : Callback<Game> {
            override fun onResponse(call: Call<Game>, response: Response<Game>) {
                response.body()?.let {
                    s = it.start_seq
                    Log.e(TAG, "sendGameApi start_seq : " + it.start_seq)
                }
            }

            override fun onFailure(call: Call<Game>, t: Throwable) {
                Log.e(TAG, "sendGameApi onFailure : " + t.message)
            }
        })
    }

    override fun onInterstitialLoaded(p0: MoPubInterstitial?) {
        webView.evaluateJavascript(
            "var event = new CustomEvent(\"resultCheckInterstitialAd\", {\n detail: {\n status : 0\n }\n});\nwindow.dispatchEvent(event);\n",
            null
        )
    }

    override fun onInterstitialFailed(p0: MoPubInterstitial?, p1: MoPubErrorCode?) {
        webView.evaluateJavascript(
            "var event = new CustomEvent(\"resultCheckInterstitialAd\", {\n detail: {\n status : -1\n }\n});\nwindow.dispatchEvent(event);\n",
            null
        )
        t = "el"
        a = "I"
        e = p1?.name
        sendGameApi()
    }

    override fun onInterstitialShown(p0: MoPubInterstitial?) {
        webView.evaluateJavascript(
            "var event = new CustomEvent(\"resultShowInterstitialAd\", {\n detail: {\n status : 0\n }\n});\nwindow.dispatchEvent(event);\n",
            null
        )
    }

    override fun onInterstitialClicked(p0: MoPubInterstitial?) {
    }

    override fun onInterstitialDismissed(p0: MoPubInterstitial?) {
        webView.evaluateJavascript(
            "var event = new CustomEvent(\"closeInterstitialAd\", {\n});\nwindow.dispatchEvent(event);\n",
            null
        )
    }
}

