package com.krosskomics.webview

import android.annotation.TargetApi
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.util.Log
import android.webkit.*
import android.webkit.WebView.WebViewTransport
import com.krosskomics.KJKomicsApp
import com.krosskomics.R
import com.krosskomics.coin.activity.CoinActivity
import com.krosskomics.common.activity.ToolbarTitleActivity
import com.krosskomics.common.model.Default
import com.krosskomics.common.model.Game
import com.krosskomics.event.activity.EventActivity
import com.krosskomics.home.activity.MainActivity
import com.krosskomics.invite.activity.InviteActivity
import com.krosskomics.library.activity.LibraryActivity
import com.krosskomics.login.activity.LoginActivity
import com.krosskomics.mainmenu.activity.MainMenuActivity
import com.krosskomics.more.activity.MoreActivity
import com.krosskomics.notice.activity.NoticeActivity
import com.krosskomics.series.activity.SeriesActivity
import com.krosskomics.settings.activity.SettingsActivity
import com.krosskomics.util.CODE
import com.krosskomics.util.CommonUtil
import com.krosskomics.util.CommonUtil.getAppVersion
import com.krosskomics.util.CommonUtil.read
import com.krosskomics.util.ServerUtil
import com.mopub.common.MoPub
import com.mopub.mobileads.MoPubErrorCode
import com.mopub.mobileads.MoPubInterstitial
import kotlinx.android.synthetic.main.activity_webview.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import com.mopub.common.SdkConfiguration
import com.mopub.common.SdkInitializationListener
import android.webkit.ValueCallback
import com.mopub.common.MoPubReward
import com.mopub.mobileads.MoPubRewardedAdListener
import com.mopub.mobileads.MoPubRewardedAds


class WebViewActivity : ToolbarTitleActivity(), MoPubInterstitial.InterstitialAdListener, MoPubRewardedAdListener {
    private val TAG = "WebViewActivity"

    var webUrl = ""
    val GOOGLE_PLAY_STORE_PREFIX = "market://details?id="
    val INTENT_PROTOCOL_START = "intent:"
    val INTENT_PROTOCOL_INTENT = "#Intent;"
    val INTENT_PROTOCOL_END = ";end"
    val INTENT_PACKAGE_NAME = "package="

    // mopup
    val AD_UNIT_ID = "24534e1901884e398f1253216226017e"
    val AD_REWARD_UNIT_ID = "920b6145fb1546cf8b5cf2ac34638bb7"
    var mInterstitial: MoPubInterstitial? = null

    /*t : api 구분
    gs(게임시작)
    gc(게임종료)
    ge(게임에러)
    as(광고노출)
    ar(보상광고완료)
    g: game 명 (웹뷰에서전달)
    i: item 명 (웹뷰에서전달)
    a: 광고타입
    R (보상광고)
    I (전명광고)
    i: item 명 (웹뷰에서전달)
    e: 게임오류시 내용 (웹뷰에서 전달)
    s: 게임시작(gs) api의 리턴값*/
//    t=ar&g=game명&s=게임시작시 리턴 start_seq&a=광고타입&i=item명(없으면공백)
    var t: String? = null
    var g: String? = null
    var s: Int? = null
    var a: String? = null
    var i: String? = null
    var e: String? = null

    override fun getLayoutId(): Int {
        return R.layout.activity_webview
    }

    override fun initTracker() {
        setTracker(toolbarTitleString)
    }

    override fun initModel() {
        intent?.apply {
            toolbarTitleString = extras?.getString("title").toString()
            webUrl = extras?.getString("url").toString()
        }
    }

    override fun initMainView() {
        val webViewClientClass: WebViewClientClass =
            WebViewClientClass()

        webView.addJavascriptInterface(
            AndroidBridge(),
            "kk_app"
        )
        webView.webChromeClient = WebViewChromeClient()
        webView.webViewClient = webViewClientClass
        webView.settings.apply {
            javaScriptEnabled = true
            setSupportMultipleWindows(true)
            javaScriptCanOpenWindowsAutomatically = true
        }
        val headers: MutableMap<String, String?> =
            HashMap()
        headers["KK-APP-KEY"] = CODE.AUTH_KEY
        headers["KK-APP-DEVICEID"] = read(context, CODE.LOCAL_Android_Id, "")
        headers["KK-APP-SECRET"] = read(context, CODE.LOCAL_APP_SECRET, "")
        headers["KK-APP-VERSION"] = getAppVersion(context)
        headers["KK-APP-TOKEN"] = KJKomicsApp.APPTOKEN
        headers["KK-U-TOKEN"] = read(context, CODE.LOCAL_ENC_USER_NO, "")
        headers["KK-LANG"] = read(context, CODE.CURRENT_LANGUAGE, "en")
        headers["KK-APP-DEVICE"] = "google"
        headers["os-version"] = Build.VERSION.RELEASE
        webView.loadUrl(webUrl, headers)
    }

    private inner class WebViewClientClass : WebViewClient() {
        override fun shouldOverrideUrlLoading(
            view: WebView,
            url: String
        ): Boolean {
            webUrl = url
            try {
                if ((url.startsWith("http://") || url.startsWith("https://")) && (url.contains("market.android.com") || url.contains(
                        "m.ahnlab.com/kr/site/download"
                    ))
                ) {
                    val uri = Uri.parse(url)
                    val intent = Intent(Intent.ACTION_VIEW, uri)
                    return try {
                        startActivity(intent)
                        true
                    } catch (e: ActivityNotFoundException) {
                        false
                    }
                } else if (url.contains(GOOGLE_PLAY_STORE_PREFIX)) { // 마켓 설치 url
                    startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
                } else if (url.startsWith("mailto:")) {
                    val intent = Intent(Intent.ACTION_SENDTO, Uri.parse(url))
                    startActivity(intent)
                } else {
                    view.loadUrl(url)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return true
        }

        @TargetApi(Build.VERSION_CODES.N)
        override fun shouldOverrideUrlLoading(
            view: WebView,
            request: WebResourceRequest
        ): Boolean {
            if (!this@WebViewActivity.isFinishing) {
                val url = request.url.toString()
                webUrl = url
                try {
                    if (url.startsWith(INTENT_PROTOCOL_START)) {
                        val customUrlStartIndex: Int = INTENT_PROTOCOL_START.length
                        val customUrlEndIndex: Int =
                            url.indexOf(INTENT_PROTOCOL_INTENT)
                        return if (customUrlEndIndex < 0) {
                            false
                        } else {
                            try {
                                val intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME)
                                val uri =
                                    Uri.parse(intent.dataString)
                                startActivity(Intent(Intent.ACTION_VIEW, uri))
                            } catch (e: ActivityNotFoundException) {
                                Log.e(
                                    TAG,
                                    "shouldOverrideUrlLoading + ActivityNotFoundException$e"
                                )
                                val packageStartIndex: Int =
                                    url.indexOf(INTENT_PACKAGE_NAME) +INTENT_PACKAGE_NAME.length
                                val packageEndIndex: Int =
                                    url.indexOf(INTENT_PROTOCOL_END)
                                val packageName = url.substring(
                                    packageStartIndex,
                                    if (packageEndIndex < 0) url.length else packageEndIndex
                                )
                                startActivity(
                                    Intent(
                                        Intent.ACTION_VIEW,
                                        Uri.parse(GOOGLE_PLAY_STORE_PREFIX + packageName)
                                    )
                                )
                            }
                            true
                        }
                    } else if (url.contains(GOOGLE_PLAY_STORE_PREFIX)) { // 마켓 설치 url
                        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
                    } else if (url.startsWith("mailto:")) {
                        val intent =
                            Intent(Intent.ACTION_SENDTO, Uri.parse(url))
                        startActivity(intent)
                    } else {
                        view.loadUrl(url)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            return true
        }

        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)
            if (!this@WebViewActivity.isFinishing) {
                showProgress(context)
            }
        }

        override fun onPageFinished(view: WebView, url: String) {
            super.onPageFinished(view, url)
            if (!this@WebViewActivity.isFinishing()) {
                hideProgress()
            }
        }

        override fun onReceivedError(
            view: WebView?,
            request: WebResourceRequest?,
            error: WebResourceError?
        ) {
            super.onReceivedError(view, request, error)
            if (!this@WebViewActivity.isFinishing()) {
                hideProgress()
            }
        }
    }

    private inner class AndroidBridge {
        @JavascriptInterface
        fun goAppView(arg: String) { // must be final
            Handler().post(Runnable {
                Log.e("HybridApp", "setMessage($arg)")
                val intent: Intent
                val bundle = Bundle()
                when (arg) {
                    "main" -> {
                        intent = Intent(context, MainActivity::class.java)
                        startActivity(intent)
                        finish()
                    }
                    "my" -> {
                        intent = Intent(context, SettingsActivity::class.java)
                        startActivity(intent)
                        finish()
                    }
                    "login" -> {
                        if (read(context, CODE.LOCAL_loginYn, "N").equals(
                                "Y",
                                ignoreCase = true)) {
                            intent = Intent(context, MainActivity::class.java)
                        } else {
                            intent = Intent(context, LoginActivity::class.java)
                            bundle.putString("pageType", CODE.LOGIN_MODE)
                            intent.putExtras(bundle)
                        }
                        startActivity(intent)
                        finish()
                    }
                    "signup" -> {
                        if (read(context, CODE.LOCAL_loginYn, "N").equals(
                                "Y",
                                ignoreCase = true)) {
                            intent = Intent(context, MainActivity::class.java)
                        } else {
                            intent = Intent(context, LoginActivity::class.java)
                            bundle.putString("pageType", CODE.SIGNUP_MODE)
                            intent.putExtras(bundle)
                        }
                        startActivity(intent)
                        finish()
                    }
                    "gift" -> {
                        intent = Intent(context, LibraryActivity::class.java)
                        startActivity(intent)
                        finish()
                    }
                    "invite" -> {
                        intent = Intent(context, InviteActivity::class.java)
                        startActivity(intent)
                        finish()
                    }
                    "promo" -> {
                        intent = Intent(context, EventActivity::class.java)
                        startActivity(intent)
                        finish()
                    }
                    "library" -> {
                        intent = Intent(context, LibraryActivity::class.java)
                        startActivity(intent)
                        finish()
                    }
                    "ongoing" -> {
                        intent = Intent(context, MainMenuActivity::class.java).apply {
                            putExtra("tabIndex", 0)
                        }
                        startActivity(intent)
                        finish()
                    }
                    "waitorpay" -> {
                        intent = Intent(context, MainMenuActivity::class.java).apply {
                            putExtra("tabIndex", 1)
                        }
                        startActivity(intent)
                        finish()
                    }
                    "ranking" -> {
                        intent = Intent(context, MainMenuActivity::class.java).apply {
                            putExtra("tabIndex", 3)
                        }
                        startActivity(intent)
                        finish()
                    }
                    "genre" -> {
                        intent = Intent(context, MainMenuActivity::class.java).apply {
                            putExtra("tabIndex", 2)
                        }
                        startActivity(intent)
                        finish()
                    }
                    "cash" -> {
                        if (read(context, CODE.LOCAL_loginYn, "N").equals("N", ignoreCase = true)) {
                            goLoginAlert(context)
                            finish()
                            return@Runnable
                        }
                        intent = Intent(context, CoinActivity::class.java)
                        startActivity(intent)
                        finish()
                    }
                }
            })
        }

        @JavascriptInterface
        fun goAppView(arg1: String, arg2: String?) { // must be final
            Handler().post(Runnable {
                Log.e("HybridApp", "setMessage($arg1)")
                val intent: Intent
                when (arg1) {
                    "series" -> {
                        intent = Intent(context, SeriesActivity::class.java)
                        val b = Bundle()
                        b.putString("sid", arg2)
                        intent.putExtras(b)
                        startActivity(intent)
                        finish()
                    }
                    "notice" -> {
                        intent = Intent(context, NoticeActivity::class.java).apply {
                            putExtra("nseq", arg2)
                        }
                        startActivity(intent)
                        finish()
                    }
                    "serieslist" -> {
                        intent = Intent(context, MoreActivity::class.java).apply {
                            putExtra("listType", "more")
                            putExtra("more_param", arg2)
                        }
                        startActivity(intent)
                        finish()
                    }
                }
            })
        }

        @JavascriptInterface
        fun openWebview(title: String?, url: String?) {
            Handler().post(Runnable {
                val intent = Intent(context, WebViewActivity::class.java)
                intent.putExtra("title", title)
                intent.putExtra("url", url)
                startActivity(intent)
            })
        }

        //서버 api 에 게임시작 로깅
        @JavascriptInterface
        fun gameStart(arg: String?) {
            Log.e(TAG, "gameStart arg : " + arg)
            t = "gs"
            g = arg
            sendGameApi()
        }

        //광고 Load후 결과를 웹뷰에 evaluateJavascript 함수를 이용 이벤트로 전달
        @JavascriptInterface
        fun checkRewardAd(arg1: String?, arg2: String?) {
            Log.e(TAG, "checkRewardAd arg1 : " + arg1 + ", arg2 : " + arg2)
            g = arg1
            i = arg2
            //Mopub SDK를 이용 광고 Load후 결과를 웹뷰에 evaluateJavascript 함수를 이용 이벤트로 전달.
            val sdkConfiguration: SdkConfiguration = SdkConfiguration.Builder(AD_REWARD_UNIT_ID)
//                .withMediationSettings("MEDIATION_SETTINGS")
//                .withAdditionalNetworks(CustomAdapterConfiguration::class.java.getName())
//                .withMediatedNetworkConfiguration(
//                    CustomAdapterConfiguration1::class.java.getName(),
//                    mediatedNetworkConfiguration
//                )
//                .withMediatedNetworkConfiguration(
//                    CustomAdapterConfiguration2::class.java.getName(),
//                    mediatedNetworkConfiguration
//                )
//                .withMediatedNetworkConfiguration(
//                    CustomAdapterConfiguration1::class.java.getName(),
//                    mediatedNetworkConfiguration1
//                )
//                .withMediatedNetworkConfiguration(
//                    CustomAdapterConfiguration2::class.java.getName(),
//                    mediatedNetworkConfiguration2
//                )
//                .withLogLevel(LogLevel.Debug)
                .withLegitimateInterestAllowed(true)
                .build()
            runOnUiThread {
                MoPub.initializeSdk(context, sdkConfiguration, initSdkListener())
            }
        }

        //보상형/전면광고 노출. 결과를 웹뷰에 evaluateJavascript 함수를 이용 이벤트로 전달. 서버 api에 광고 노출 로깅
        @JavascriptInterface
        fun showRewardAd(arg1: String?, arg2: String?) {
            Log.e(TAG, "showRewardAd arg1 : " + arg1 + ", arg2 : " + arg2)
            //Mopub SDK를 이용 광고 Load후 결과를 웹뷰에 evaluateJavascript 함수를 이용 이벤트로 전달.
            MoPubRewardedAds.showRewardedAd(AD_REWARD_UNIT_ID)
            t = "as"
            g = arg1
            a = "R"
            i = arg2
            sendGameApi()
        }

        //광고 Load후 결과를 웹뷰에 evaluateJavascript 함수를 이용 이벤트로 전달
        @JavascriptInterface
        fun checkInterstitialAd(arg1: String?) {
            Log.e(TAG, "checkInterstitialAd arg1 : " + arg1)
            g = arg1
            //Mopub SDK를 이용 광고 Load후 결과를 웹뷰에 evaluateJavascript 함수를 이용 이벤트로 전달.
            val sdkConfiguration: SdkConfiguration = SdkConfiguration.Builder(AD_UNIT_ID)
//                .withMediationSettings("MEDIATION_SETTINGS")
//                .withAdditionalNetworks(CustomAdapterConfiguration::class.java.getName())
//                .withMediatedNetworkConfiguration(
//                    CustomAdapterConfiguration1::class.java.getName(),
//                    mediatedNetworkConfiguration
//                )
//                .withMediatedNetworkConfiguration(
//                    CustomAdapterConfiguration2::class.java.getName(),
//                    mediatedNetworkConfiguration
//                )
//                .withMediatedNetworkConfiguration(
//                    CustomAdapterConfiguration1::class.java.getName(),
//                    mediatedNetworkConfiguration1
//                )
//                .withMediatedNetworkConfiguration(
//                    CustomAdapterConfiguration2::class.java.getName(),
//                    mediatedNetworkConfiguration2
//                )
//                .withLogLevel(LogLevel.Debug)
                .withLegitimateInterestAllowed(false)
                .build()
            mInterstitial = MoPubInterstitial(this@WebViewActivity, AD_UNIT_ID)
            mInterstitial?.interstitialAdListener = this@WebViewActivity
            mInterstitial?.load()
        }

        //전면광고 노출. 결과를 웹뷰에 evaluateJavascript 함수를 이용 이벤트로 전달. 서버 api에 광고 노출 로깅
        @JavascriptInterface
        fun showInterstitialAd(arg1: String?) {
            Log.e(TAG, "showInterstitialAd arg1 : " + arg1)
            if (mInterstitial?.isReady() == true) {
                mInterstitial?.show()
            } else {
                // Caching is likely already in progress if `isReady()` is false.
                // Avoid calling `load()` here and instead rely on the callbacks as suggested below.
            }
            t = "as"
            g = arg1
            a = "I"
            sendGameApi()
        }

        //게임종료 전달. 앱에서는 서버 api로 게임종료 로깅. 앱에서 웹뷰 종료
        @JavascriptInterface
        fun gameClose(arg1: String?) {
            Log.e(TAG, "gameClose arg1 : " + arg1)
            //Mopub SDK를 이용 광고 Load후 결과를 웹뷰에 evaluateJavascript 함수를 이용 이벤트로 전달.
            t = "gc"
            g = arg1
            sendGameApi()
        }

        //게임오류 전달
        @JavascriptInterface
        fun gameError(arg1: String?, arg2: String?) {
            //Mopub SDK를 이용 광고 Load후 결과를 웹뷰에 evaluateJavascript 함수를 이용 이벤트로 전달.
            Log.e(TAG, "gameError arg1 : " + arg1 + "arg2 : " + arg2)
            t = "ge"
            g = arg1
            e = arg2
            sendGameApi()
        }
    }

    private fun initSdkListener(): SdkInitializationListener? {
        return SdkInitializationListener {
        /* MoPub SDK initialized.
           Check if you should show the consent dialog here, and make your ad requests. */
            Log.e(TAG, "mopup SdkInitializationListener")
            MoPubRewardedAds.loadRewardedAd(AD_REWARD_UNIT_ID)
        }
    }

    private inner class WebViewChromeClient : WebChromeClient() {
        override fun onCreateWindow(
            view: WebView,
            dialog: Boolean,
            userGesture: Boolean,
            resultMsg: Message
        ): Boolean {
            val newWebView = WebView(context)
            val transport = resultMsg.obj as WebViewTransport
            transport.webView = newWebView
            resultMsg.sendToTarget()
            return true
        }

        override fun onJsAlert(
            view: WebView,
            url: String,
            message: String,
            result: JsResult
        ): Boolean {
//            Log.e(TAG, "message : " + message);
            return true
        }
    }

    private fun sendGameApi() {
        val api: Call<Game> = ServerUtil.service.sendGameApi(
            CommonUtil.read(context, CODE.CURRENT_LANGUAGE, "en"),
            t,
            g,
            s,
            a,
            i,
            e
        )
        api.enqueue(object : Callback<Game> {
            override fun onResponse(call: Call<Game>, response: Response<Game>) {
                response.body()?.let {
                    s = it.start_seq
                    Log.e(TAG, "sendGameApi start_seq : " + it.start_seq)
                }
            }

            override fun onFailure(call: Call<Game>, t: Throwable) {
                Log.e(TAG, "sendGameApi onFailure : " + t.message)
            }
        })
    }

    override fun onInterstitialLoaded(p0: MoPubInterstitial?) {
        Log.e(TAG, "onInterstitialLoaded")
        webView.evaluateJavascript(
            "var event = new CustomEvent(\"resultCheckInterstitialAd\", {\n detail: {\n status : 0\n }\n});\nwindow.dispatchEvent(event);\n",
            null)
    }

    override fun onInterstitialFailed(p0: MoPubInterstitial?, p1: MoPubErrorCode?) {
        Log.e(TAG, "onInterstitialFailed")
        webView.evaluateJavascript(
            "var event = new CustomEvent(\"resultCheckInterstitialAd\", {\n detail: {\n status : -1\n }\n});\nwindow.dispatchEvent(event);\n",
            null)
    }

    override fun onInterstitialShown(p0: MoPubInterstitial?) {
        Log.e(TAG, "onInterstitialShown")
        webView.evaluateJavascript(
            "var event = new CustomEvent(\"resultShowInterstitialAd\", {\n detail: {\n status : 0\n }\n});\nwindow.dispatchEvent(event);\n",
            null)
    }

    override fun onInterstitialClicked(p0: MoPubInterstitial?) {
        Log.e(TAG, "onInterstitialClicked")
    }

    override fun onInterstitialDismissed(p0: MoPubInterstitial?) {
        Log.e(TAG, "onInterstitialDismissed")
        webView.evaluateJavascript(
            "var event = new CustomEvent(\"closeInterstitialAd\", {\n detail: \n});\nwindow.dispatchEvent(event);\n",
            null)
    }


    override fun onRewardedAdClicked(adUnitId: String) {
        Log.e(TAG, "onRewardedAdClicked")
    }

    //evaluateJavascript 함수를 이용 이벤트로 전달
    override fun onRewardedAdClosed(adUnitId: String) {
        Log.e(TAG, "onRewardedAdClosed")
        webView.evaluateJavascript(
            "var event = new CustomEvent(\"closeRewardAd\", {\n detail: {\n item : $i\n }\n});\nwindow.dispatchEvent(event);\n",
            null)
    }

    //evaluateJavascript 함수를 이용 이벤트로 전달. 서버 api에 보상지급 로깅.
    override fun onRewardedAdCompleted(adUnitIds: Set<String?>, reward: MoPubReward) {
        Log.e(TAG, "onRewardedAdCompleted")
        webView.evaluateJavascript(
            "var event = new CustomEvent(\"completeRewardAd\", {\n detail: {\n item : $i\n }\n});\nwindow.dispatchEvent(event);\n",
            null)
        sendGameApi()
    }

    override fun onRewardedAdLoadFailure(adUnitId: String, errorCode: MoPubErrorCode) {
        Log.e(TAG, "onRewardedAdLoadFailure")
        webView.evaluateJavascript(
            "var event = new CustomEvent(\"resultCheckRewardAd\", {\n detail: {\n item : $i,\n status : -1\n }\n});\nwindow.dispatchEvent(event);\n",
            null)
    }

    override fun onRewardedAdLoadSuccess(adUnitId: String) {
        Log.e(TAG, "onRewardedAdLoadSuccess")
        webView.evaluateJavascript(
            "var event = new CustomEvent(\"resultCheckRewardAd\", {\n detail: {\n item : $i,\n status : 0\n }\n});\nwindow.dispatchEvent(event);\n",
            null)
    }

    override fun onRewardedAdShowError(adUnitId: String, errorCode: MoPubErrorCode) {
        Log.e(TAG, "onRewardedAdShowError")
        webView.evaluateJavascript(
            "var event = new CustomEvent(\"resultShowRewardAd\", {\n detail: {\n item : $i,\n status : -1\n }\n});\nwindow.dispatchEvent(event);\n",
            null)
    }

    override fun onRewardedAdStarted(adUnitId: String) {
        Log.e(TAG, "onRewardedAdStarted")
        webView.evaluateJavascript(
            "var event = new CustomEvent(\"resultShowRewardAd\", {\n detail: {\n item : $i,\n status : 0\n }\n});\nwindow.dispatchEvent(event);\n",
            null)
    }
}

