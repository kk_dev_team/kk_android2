package com.krosskomics.restful

import com.krosskomics.common.model.*
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface InterfaceRestful {
    // 버전 체크
    @get:POST("app/google/50/version")
    val getVersion: Call<Version>

    // 앱 체크
    @FormUrlEncoded
    @POST("app/google/50/app")
    fun getCheckApp(@Field("deviceid") deviceid: String?): Call<AppToken>

    // 쿠키가져오기
//    @FormUrlEncoded
//    @POST("app/google/37/getdata.php")
//    fun getCookieData(
//        @Field("get_type") get_type: String?,
//        @Field("pm") pm: String?
//    ): Call<Cookie>

    // 크로스 코믹스 가입 회원
    @FormUrlEncoded
    @POST("app/google/50/login")
    fun setLoginKrossApi(
        @Field("lang") lang: String?,
        @Field("t") login_type: String?,
        @Field("st") sns_token: String? = "",
        @Field("ft") firebase_token: String?,
        @Field("e") email: String? = "",
        @Field("p") passwd: String? = "",
        @Field("advid") advid: String?
    ): Call<Login>

    // 계정 체크
    @FormUrlEncoded
    @POST("app/google/50/account")
    fun setAccountKross(
        @Field("lang") lang: String?,
        @Field("t") t: String?,
        @Field("p") p: String?
    ): Call<Default>

    // 크로스 코믹스 회원가입
    @FormUrlEncoded
    @POST("app/google/50/signup")
    fun signUpKrossApi(
        @Field("lang") lang: String?,
        @Field("t") login_type: String?,
        @Field("e") email: String?,
        @Field("p") passwd: String?,
        @Field("st") sns_token: String?,
        @Field("n") nick: String?,
        @Field("g") gender: String?,
        @Field("a") age: String?,
        @Field("gr") genre: String?,
        @Field("ft") firebase_token: String?,
        @Field("rid") rid: String?
    ): Call<Login>

    // 초기화 데이터
    @FormUrlEncoded
    @POST("app/google/106/init")
    fun getInitSetApi(
        @Field("lang") lang: String?,
        @Field("ft") firebase_token: String?,
        @Field("rs") ref_source: String?,
        @Field("advid") advid: String?
    ): Call<InitSet>

    // main
    @FormUrlEncoded
    @POST("app/google/104/main")
    fun getMainApi(
        @Field("lang") lang: String?
    ): Call<Main>

    @FormUrlEncoded
    @POST("app/google/104/main/banner")
    fun getMainBannerApi(
        @Field("lang") lang: String?
    ): Call<Banner>

    @FormUrlEncoded
    @POST("app/google/104/main/read/episode")
    fun getReadEpApi(
        @Field("lang") lang: String?
    ): Call<ReadEp>

    // seriesmenu
    @FormUrlEncoded
    @POST("/app/google/106/series/menu")
    fun getSeriesMenu(
        @Field("lang") lang: String?,
        @Field("m") m: String,
        @Field("p") p: String?,
        @Field("page") page: Int
    ): Call<More>

    @FormUrlEncoded
    @POST("/app/google/50/seriesmunu.php")
    fun getSeriesGenre(
        @Field("lang") lang: String?,
        @Field("m") m: String,
        @Field("p") p: String?
    ): Call<Genre>

    // more 리스트
    @FormUrlEncoded
    @POST("/app/google/50/series/list")
    fun getMoreList(
        @Field("lang") lang: String?,
        @Field("t") t: String?,
        @Field("p") p: String?,
        @Field("page") page: Int
    ): Call<More>

    // news 리스트
    @FormUrlEncoded
    @POST("/app/google/50/user/list")
    fun getNewsList(
        @Field("lang") lang: String?,
        @Field("t") t: String?,
        @Field("page") page: Int
    ): Call<News>

    // news 읽음처리
    @FormUrlEncoded
    @POST("app/google/50/user/set")
    fun sendReadNews(
        @Field("lang") lang: String?,
        @Field("t") t: String?,
        @Field("p") p: String
    ): Call<Default>

    // comment 리스트, 신고
//    t	list
//    sid	시리즈 아이디
//    eid	에피소드 아이디
//    s	sort
//    page	페이지
//    c	댓글 내용 (300자 이내)
//    p	댓글 번호 (list 상 seq)
//    r	신고 구분
    @FormUrlEncoded
    @POST("app/google/50/comment")
    fun getCommentList(
        @Field("lang") lang: String?,
        @Field("t") t: String,
        @Field("sid") sid: String,
        @Field("eid") eid: String?,
        @Field("s") sort: String,
        @Field("page") page: Int,
        @Field("c") c: String?,
        @Field("p") p: String?,
        @Field("r") r: String?
    ): Call<Comment>

    // 시리즈 데이터 호출
    @FormUrlEncoded
    @POST("app/google/50/series")
    fun getSeriesData(
        @Field("lang") lang: String?,
        @Field("sid") sid: String?
    ): Call<Episode>

    // ep 데이터 호출
    @FormUrlEncoded
    @POST("/app/google/50/episode")
    fun getEpList(
        @Field("lang") lang: String?,
        @Field("sid") sid: String?,
        @Field("s") s: String?,
        @Field("page") page: Int
    ): Call<EpisodeMore>

    // 에피소드 소장체크
    @FormUrlEncoded
    @POST("/app/google/106/checkepisode")
    fun checkEpisode(
        @Field("lang") lang: String?,
        @Field("eid") eid: String?
    ): Call<Episode>

    //에피소드 단독구매
    @FormUrlEncoded
    @POST("app/google/106/unlockepisode")
    fun setPurchaseEpisodeApi(
        @Field("lang") lang: String?,
        @Field("eid") eids: String?,
        @Field("ut") unlock_typ: String?
    ): Call<PurchaseEpisode>

    //에피소드 선택구매
    @FormUrlEncoded
    @POST("app/google/106/unlockepisode/bulk")
    fun setPurchaseSelectEpisodeApi(
        @Field("lang") lang: String?,
        @Field("eids") eids: String?,
        @Field("ut") unlock_typ: String?
    ): Call<PurchaseEpisode>

    // 뷰어 호출
    @FormUrlEncoded
    @POST("app/google/50/view")
    fun getEpisodeViewer(
        @Field("lang") lang: String?,
        @Field("eid") eid: String?
    ): Call<Episode>

    // library list
    @FormUrlEncoded
    @POST("app/google/50/user/list")
    fun getLibraryList(
        @Field("lang") lang: String?,
        @Field("t") list_type: String?,
        @Field("page") page: Int
    ): Call<More>

    // 유저 정보 설정
    @FormUrlEncoded
    @POST("app/google/50/user/set")
    fun setUserProfileApi(
        @Field("lang") lang: String?,
        @Field("t") t: String?,
        @Field("p") p: String? = null,
        @Field("c") c: String? = null,
        @Field("sid") sid: String? = null,
        @Field("advid") advid: String? = null
    ): Call<Default>

    // 에피소드 삭제
    @FormUrlEncoded
    @POST("app/google/37/user/set")
    fun setDeleteEpisodes(
        @Field("lang") lang: String?,
        @Field("set_type") set_type: String?,
        @Field("sid") sid: String?,
        @Field("eids") eids: String?
    ): Call<Default>

    // 쿠폰
    @FormUrlEncoded
    @POST("app/google/50/user/set")
    fun setPromotionCodeApi(
        @Field("lang") lang: String?,
        @Field("t") t: String?,
        @Field("p") p: String?
    ): Call<Coin>

    // 뷰어 최종 인덱스
    @FormUrlEncoded
    @POST("app/google/50/user/set")
    fun setImageIndexApi(
        @Field("lang") lang: String?,
        @Field("t") t: String?,
        @Field("p") p: String?,
        @Field("sid") sid: String?,
        @Field("eid") eid: String?,
        @Field("evid") evid: String?
    ): Call<Default>

    // 로그아웃시 호출
    @FormUrlEncoded
    @POST("app/google/50/user")
    fun postLogoutApi(
        @Field("lang") lang: String?,
        @Field("t") t: String?,
        @Field("p") p: Long?
    ): Call<Login>

    // 비밀번호 체크
    @FormUrlEncoded
    @POST("app/google/50/user")
    fun checkCurrentPwApi(
        @Field("lang") lang: String?,
        @Field("t") t: String?,
        @Field("p") p: String?
    ): Call<Default>

    // 초대
    @FormUrlEncoded
    @POST("app/google/50/user")
    fun getInviteApi(
        @Field("lang") lang: String?,
        @Field("t") t: String?
    ): Call<Invite>

    // 앱 종료시 호출
    @FormUrlEncoded
    @POST("app/google/50/edata")
    fun postFinishApp(
        @Field("lang") lang: String?,
        @Field("run_seq") run_seq: Long,
        @Field("login_seq") login_seq: Long
    ): Call<Default>

    // 검색 초기 데이터
    @FormUrlEncoded
    @POST("app/google/50/search")
    fun getSearchMain(
        @Field("lang") lang: String?,
        @Field("page") page: Int,
        @Field("k") k: String?
    ): Call<Search>

    // 선물함
    @FormUrlEncoded
    @POST("app/google/50/user/list")
    fun getGiftBox(
        @Field("lang") lang: String?,
        @Field("t") list_type: String?,
        @Field("page") page: Int
    ): Call<Gift>

    // 선물 받기
    @FormUrlEncoded
    @POST("app/google/50/user/set")
    fun getGiftApi(
        @Field("lang") lang: String?,
        @Field("t") t: String?,
        @Field("p") p: String?
    ): Call<Gift>

    // 인앱 리스트
    @FormUrlEncoded
    @POST("app/google/50/product")
    fun getInappData(
        @Field("lang") lang: String?
    ): Call<Coin>

    // paytm
    @FormUrlEncoded
    @POST("app/google/50/paytm/init")
    fun getPaytmData(
        @Field("lang") lang: String?,
        @Field("productid") productid: String?
    ): Call<Coin>

    // paytm
    @FormUrlEncoded
    @POST("app/google/50/paytm/status")
    fun getPaytmStatus(
        @Field("lang") lang: String?,
        @Field("orderid") orderid: String?
    ): Call<Coin>

    // 인앱 결제 후 코인 지급
    @FormUrlEncoded
    @POST("app/google/50/purchase")
    fun getInappPurchase(
        @Field("lang") lang: String?,
        @Field("productid") productid: String?,
        @Field("orderid") orderid: String?,
        @Field("purchasetime") purchasetime: Long,
        @Field("purchasestate") purchasestate: Int,
        @Field("purchasetoken") purchasetoken: String?
    ): Call<Coin>

    //post (utoken(parameter 이름 확인), orderid, productid, purchasetime, purchasestate, purchasetoken) 로 전송.
    @FormUrlEncoded
    @POST("app/google/50/repurchase")
    fun getInappRePurchase(
        @Field("lang") lang: String?,
        @Field("utoken") utoken: String?,
        @Field("productid") productid: String?,
        @Field("orderid") orderid: String?,
        @Field("purchasetime") purchasetime: Long,
        @Field("purchasestate") purchasestate: Int,
        @Field("purchasetoken") purchasetoken: String?
    ): Call<Coin>

    // 에피소드 유효성 체크
    @FormUrlEncoded
    @POST("app/google/50/checkdata")
    fun getCheckData(
        @Field("lang") lang: String?,
        @Field("check_type") check_type: String?,
        @Field("sid") sid: String?
    ): Call<CheckData>

    // download ep
    @FormUrlEncoded
    @POST("app/google/50/download")
    fun getDownloadEpisodeApi(
        @Field("lang") lang: String?,
        @Field("eid") eid: String?
    ): Call<Episode>

    // 다운로드 episode expired 체크
    @FormUrlEncoded
    @POST("app/google/50/user/set")
    fun getUserApi(
        @Field("t") lang: String?
    ): Call<User>

    // 뷰어 체류시간 체크
    @FormUrlEncoded
    @POST("app/google/37/set")
    fun setEpViewOut(
        @Field("lang") lang: String?,
        @Field("set_type") set_type: String?,
        @Field("ep_view_id") ep_view_id: String?,
        @Field("img_index") img_index: String?
    ): Call<Default>

    // 다운로드 완료 알림
    @FormUrlEncoded
    @POST("app/google/50/user/set")
    fun sendDownloadCompleteApi(
        @Field("lang") lang: String?,
        @Field("t") t: String?,
        @Field("eid") eid: String?
    ): Call<Default>

    // 캐시 히스토리
    @FormUrlEncoded
    @POST("app/google/50/user/list")
    fun getCashHistoryApi(
        @Field("lang") lang: String?,
        @Field("t") type: String?,
        @Field("page") page: Int
    ): Call<CashHistory>

    // 티 히스토리
    @FormUrlEncoded
    @POST("app/google/50/user/list")
    fun getTicketHistory(
        @Field("lang") lang: String?,
        @Field("t") type: String?,
        @Field("page") page: Int
    ): Call<CashHistory>

    // 이벤트
    @FormUrlEncoded
    @POST("/app/google/50/event")
    fun getEventApi(
        @Field("lang") lang: String?,
        @Field("t") type: String?,
        @Field("page") page: Int
    ): Call<Event>

    // 게임시작 로깅 요청
    @FormUrlEncoded
    @POST("/app/google/106/game")
    fun sendGameApi(
        @Field("lang") lang: String?,
        @Field("t") t: String?,
        @Field("g") g: String?,
        @Field("s") startSeq: Int? = null,
        @Field("a") a: String?,
        @Field("i") i: String?,
        @Field("e") e: String?
    ): Call<Game>
}