package com.krosskomics.event.activity

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.krosskomics.R
import com.krosskomics.common.activity.ToolbarTitleActivity
import com.krosskomics.common.model.Coin
import com.krosskomics.event.viewmodel.EventViewModel
import com.krosskomics.util.CODE
import com.krosskomics.util.CommonUtil
import com.krosskomics.util.ServerUtil
import kotlinx.android.synthetic.main.activity_event.*
import kotlinx.android.synthetic.main.view_action_item.view.*
import kotlinx.android.synthetic.main.view_network_error.*
import kotlinx.android.synthetic.main.view_toolbar.*
import kotlinx.android.synthetic.main.view_toolbar.view.*
import retrofit2.Call
import retrofit2.Response

class EventActivity : ToolbarTitleActivity() {
    private val TAG = "EventActivity"

    public override val viewModel: EventViewModel by lazy {
        ViewModelProvider(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return EventViewModel(application) as T
            }
        }).get(EventViewModel::class.java)
    }

    override fun getLayoutId(): Int {
        recyclerViewItemLayoutId = R.layout.item_event
        return R.layout.activity_event
    }

    override fun initTracker() {
        setTracker(getString(R.string.str_event))
    }

    override fun initLayout() {
        toolbarTitleString = getString(R.string.str_event)
        viewModel.listType = "list"
        super.initLayout()
        initPromotionView()
    }

    override fun initToolbar() {
        super.initToolbar()
        toolbar.apply {
            actionItem.visibility = View.VISIBLE
            actionItem.searchImageView.visibility = View.GONE
            actionItem.giftboxImageView.visibility = View.GONE
            actionItem.eventCodeImageView.visibility = View.VISIBLE
            actionItem.eventCodeImageView.setOnClickListener {
                actionItem.eventCodeImageView.visibility = View.GONE
                actionItem.doneTextView.visibility = View.VISIBLE
                promotionCodeView.visibility = View.VISIBLE
                promotionCodeView.setOnClickListener { return@setOnClickListener }
            }
            actionItem.doneTextView.setOnClickListener {
                promotionCodeView.visibility = View.GONE
                actionItem.eventCodeImageView.visibility = View.VISIBLE
                actionItem.doneTextView.visibility = View.GONE
            }
        }
    }

    private fun initPromotionView() {
        procodeEditText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable) {
                if (s.length < 8) {
                    promotionCodeView.isSelected = false
                    registerButton.isEnabled = false
                } else {
                    promotionCodeView.isSelected = true
                    registerButton.isEnabled = true
                }
            }
        })

        registerButton.setOnClickListener {
            // request server
            requestPromotionCode()
        }
    }

    private fun requestPromotionCode() {
        val api = ServerUtil.service.setPromotionCodeApi(
            CommonUtil.read(context, CODE.CURRENT_LANGUAGE, "en"),
            "coupon", procodeEditText.text.toString())
        api.enqueue(object : retrofit2.Callback<Coin> {
            override fun onResponse(call: Call<Coin>, response: Response<Coin>) {
                try {
                    if (response.isSuccessful) {
                        val item = response.body()
                        if ("00" == item?.retcode) {
                            val coin = item.cash.toString()
                            CommonUtil.write(context, CODE.LOCAL_coin, coin)
                            if ("" != item.msg) {
                                CommonUtil.showToast(item.msg, context)
                                procodeEditText.text = null
                            }
                        } else if ("203" == item?.retcode) {
                            goLoginAlert(context)
                        } else {
                            if ("" != item?.msg) {
                                CommonUtil.showToast(item?.msg, context)
                            }
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<Coin>, t: Throwable) {
                try {
                    checkNetworkConnection(context, t, errorView)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }
}