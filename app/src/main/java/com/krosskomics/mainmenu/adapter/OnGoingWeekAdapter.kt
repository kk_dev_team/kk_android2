package com.krosskomics.mainmenu.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.krosskomics.R
import com.krosskomics.common.data.DataWeek
import com.krosskomics.common.holder.BaseItemViewHolder

class OnGoingWeekAdapter(private val items: ArrayList<*>) : RecyclerView.Adapter<OnGoingWeekAdapter.OnGoingWeekViewHolder>() {

    private var onClickListener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OnGoingWeekViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_ongoing_week, parent, false)

        return OnGoingWeekViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: OnGoingWeekViewHolder, position: Int) {
        holder.setData(items[position], position)
    }

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener) {
        this.onClickListener = onItemClickListener
    }

    inner class OnGoingWeekViewHolder(itemView: View) : BaseItemViewHolder(itemView) {
        val weekTextView = itemView.findViewById<TextView>(R.id.weekTextView)

        override fun setData(item: Any?, position: Int) {
            if (item is DataWeek) {
                itemView.apply {
                    setOnClickListener {
                        onClickListener?.onItemClick(item, position)
                    }
                    weekTextView.text = item.title
                    isSelected = item.isSelect
                }
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(item: Any?, position: Int)
    }
}