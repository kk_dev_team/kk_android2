package com.krosskomics.mainmenu.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.krosskomics.common.adapter.RecyclerViewBaseAdapter
import com.krosskomics.common.data.*
import com.krosskomics.util.CommonUtil

import kotlinx.android.synthetic.main.item_waitfree.view.*

class WaitFreeAdapter(private val items: ArrayList<*>, private val layoutRes: Int) :
    RecyclerViewBaseAdapter(items, layoutRes) {

    private var onClickListener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewBaseAdapter.BaseItemHolder {
        val view = LayoutInflater.from(parent.context).inflate(layoutRes, parent, false)
        return BaseItemHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerViewBaseAdapter.BaseItemHolder, position: Int) {
        holder.setData(items[position], position)
//        val item = items[position]
//        holder.itemView.apply {
//            setOnClickListener {
//                onClickListener?.onItemClick(item, position)
//            }
//            when (item) {
//                is DataBook -> {
//                    waitMainImageView?.let {
//                        if (it.tag == null) {
//                            CommonUtil.setGlideImage(context, item.image ?: "", it)
//                        } else {
//                            val roundCorner = it.tag.toString()
//                            CommonUtil.setGlideImageRound(
//                                context,
//                                item.image ?: "",
//                                it,
//                                roundCorner.toInt()
//                            )
//                        }
//                    }
//
////                    titleTextView?.text = item.title
////                    writerTextView?.text = item.writer1
////                    if (item.genre1.isNullOrEmpty()) {
////                        genreDotTextView?.visibility = View.GONE
////                    } else {
////                        genreDotTextView?.visibility = View.VISIBLE
////                    }
////                    genreTextView?.text = item.genre1
////                    descTextView?.text = item.dp_txt
////
////                    if (item.like_cnt.isNullOrEmpty()) {
////                        contentLikeView?.visibility = View.GONE
////                    } else {
////                        contentLikeView?.visibility = View.VISIBLE
////                        likeCountTextView?.text = item.like_cnt
////                        tv_like_count?.text = item.like_cnt
////                    }
////                    if (item.isnew == "1") {
////                        newTagImageView?.visibility = View.VISIBLE
////                        upTagImageView?.visibility = View.GONE
////                    } else {
////                        newTagImageView?.visibility = View.GONE
////                        if (item.isupdate == "1") {
////                            upTagImageView?.visibility = View.VISIBLE
////                        } else {
////                            upTagImageView?.visibility = View.GONE
////                        }
////                    }
////                    if (item.epCount == 0) {
////                        epDownloadCntTextView?.visibility = View.GONE
////                    } else {
////                        epDownloadCntTextView?.text = itemView.context.getString(R.string.str_episodes_seq_format_small1, item.epCount)
////                        epDownloadCntTextView?.visibility = View.VISIBLE
////                    }
////                    // 기다무
////                    if (item.iswop == "1") {
////                        remainTagView?.visibility = View.VISIBLE
////                        remainTagView?.isSelected = false
////                        remainTagTextView?.text = item.dp_wop_term
////                    } else {
////                        if (item.dp_pub_day.isNullOrEmpty()) {
////                            remainTagView?.visibility = View.GONE
////                        } else {
////                            remainTagView?.isSelected = true
////                            remainTagTextView?.text = item.dp_pub_day
////                            remainTagView?.visibility = View.VISIBLE
////                        }
////                    }
////                    // ranking
////                    if (context is MainMenuActivity) {
////                        when (position) {
////                            0 -> rankingImageView?.setImageResource(R.drawable.kk_ranking_1)
////                            1 -> rankingImageView?.setImageResource(R.drawable.kk_ranking_2)
////                            2 -> {
////                                rankingImageView?.setImageResource(R.drawable.kk_ranking_3)
////                                defaultUnderLineView?.visibility = View.GONE
////                                dummyView?.visibility = View.VISIBLE
////                            }
////                        }
////                        rankingTextView?.text = (position + 1).toString()
////                    }
//                }
//            }
//        }
    }

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener) {
        this.onClickListener = onItemClickListener
    }

    interface OnItemClickListener {
        fun onItemClick(item: Any?)
    }
}