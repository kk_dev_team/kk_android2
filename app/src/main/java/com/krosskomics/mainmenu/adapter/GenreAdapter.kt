package com.krosskomics.mainmenu.adapter

import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.krosskomics.R
import com.krosskomics.common.data.DataGenre
import com.krosskomics.common.holder.BaseItemViewHolder

class GenreAdapter(private val items: ArrayList<*>) : RecyclerView.Adapter<GenreAdapter.GenreViewHolder>() {
    private var onClickListener: GenreAdapter.OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenreAdapter.GenreViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_genre_select, parent, false)

        return GenreViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: GenreAdapter.GenreViewHolder, position: Int) {
        holder.setData(items[position], position)
    }

    fun setOnItemClickListener(onItemClickListener: GenreAdapter.OnItemClickListener) {
        this.onClickListener = onItemClickListener
    }

    inner class GenreViewHolder(itemView: View) : BaseItemViewHolder(itemView) {
        val genreTextView = itemView.findViewById<TextView>(R.id.genreTextView)

        override fun setData(item: Any?, position: Int) {
            if (item is DataGenre) {
                itemView.apply {
                    setOnClickListener {
                        onClickListener?.onItemClick(item, position)
                    }
                    if (item.dp_genre.isNullOrEmpty()) {
                        genreTextView.visibility = View.GONE
                    } else {
                        genreTextView.visibility = View.VISIBLE
                        genreTextView.text = item.dp_genre
                    }
                    isSelected = item.isSelect
                    if (isSelected) {
                        genreTextView.typeface = Typeface.DEFAULT_BOLD
                    } else {
                        genreTextView.typeface = Typeface.DEFAULT
                    }
                }
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(item: Any?, position: Int)
    }
}