package com.krosskomics.mainmenu.activity

import android.content.Intent
import android.util.Log
import android.view.View
import androidx.viewpager2.widget.ViewPager2
import com.krosskomics.BuildConfig
import com.krosskomics.KJKomicsApp
import com.krosskomics.R
import com.krosskomics.common.activity.ToolbarViewPagerActivity
import com.krosskomics.library.activity.LibraryActivity
import com.krosskomics.mainmenu.fragment.RankingFragment
import com.krosskomics.search.activity.SearchActivity
import com.krosskomics.util.CODE
import com.krosskomics.util.CommonUtil
import kotlinx.android.synthetic.main.activity_main_menu.*
import kotlinx.android.synthetic.main.view_main_action_item.*
import kotlinx.android.synthetic.main.view_mainmenu_tab.*
import kotlinx.android.synthetic.main.view_ranking_floating_dim.*
import kotlinx.android.synthetic.main.view_toolbar_main_menu.*
import kotlinx.android.synthetic.main.view_toolbar_main_menu.view.*

class MainMenuActivity : ToolbarViewPagerActivity() {
    private val TAG = "MainMenuActivity"

    private var tabButtonItems: List<View>? = null

    override fun requestServer() {}

    override fun getLayoutId(): Int {
        recyclerViewItemLayoutId = R.layout.item_ongoing
        return R.layout.activity_main_menu
    }

    override fun initToolbar() {
        super.initToolbar()
        toolbar.apply {
            actionItem?.visibility = View.VISIBLE
        }
    }

    override fun onResume() {
        super.onResume()
        if (KJKomicsApp.IS_GET_NEW_GIFT) {
            newGiftPointView.visibility = View.VISIBLE
        } else {
            newGiftPointView.visibility = View.GONE
        }
    }

    override fun initTracker() {}

    override fun initLayout() {
        adapterType = 3
        super.initLayout()
        viewPager.currentItem = viewModel.tabIndex
        viewPager.offscreenPageLimit = 4
        viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback(){
            override fun onPageSelected(position: Int) {
                viewModel.tabIndex = position
                replaceFragmentTabIndex()
            }
        })
        replaceFragmentTabIndex()
    }

    override fun initTabItems() {
        tabItems = listOf(getString(R.string.str_ongoing),
            getString(R.string.str_waitforfree),
            getString(R.string.str_genre),
            getString(R.string.str_ranking)
        )
    }

    override fun initModel() {
        viewModel.tabIndex = intent.getIntExtra("tabIndex", 0)
        tabButtonItems = listOf(onGoingButton, waitButton, genreButton, rankingButton)
    }

    override fun onClick(v: View?) {
        when(v?.id) {
            R.id.searchImageView -> startActivity(Intent(context, SearchActivity::class.java))
            R.id.giftboxImageView -> {
                if (CommonUtil.read(context, CODE.LOCAL_loginYn, "N").equals("Y", ignoreCase = true)) {
                    val intent = Intent(context, LibraryActivity::class.java)
                    intent.putExtra("tabIndex", 1)
                    startActivity(intent)
                } else {
                    goLoginAlert(context)
                }
            }
            // tabview
            R.id.homeButton -> finish()
            R.id.onGoingButton -> {
                viewModel.tabIndex = 0
                viewPager.currentItem = viewModel.tabIndex
                replaceFragmentTabIndex()
            }
            R.id.waitButton -> {
                viewModel.tabIndex = 1
                viewPager.currentItem = viewModel.tabIndex
                replaceFragmentTabIndex()
            }
            R.id.genreButton -> {
                viewModel.tabIndex = 2
                viewPager.currentItem = viewModel.tabIndex
                replaceFragmentTabIndex()
            }
            R.id.rankingButton -> {
                viewModel.tabIndex = 3
                viewPager.currentItem = viewModel.tabIndex
                replaceFragmentTabIndex()
            }
        }
    }

    fun replaceFragmentTabIndex() {
        when (viewModel.tabIndex) {
            0 -> {
                mainMenuTabView.setBackgroundResource(R.drawable.kk_gradient_tabmenu_ongoing_rect)
                toolbar.toolbarLottieView.apply {
                    setAnimation("kk_lottie_ongoing.json")
                    playAnimation()
                }
            }
            1 -> {
                mainMenuTabView.setBackgroundResource(R.drawable.kk_gradient_tabmenu_waitfree_rect)
                toolbar.toolbarLottieView.apply {
                    setAnimation("kk_lottie_waitfree.json")
                    playAnimation()
                }
            }
            2 -> {
                mainMenuTabView.setBackgroundResource(R.drawable.kk_gradient_tabmenu_genre_rect)
                toolbar.toolbarLottieView.apply {
                    setAnimation("kk_lottie_genre.json")
                    playAnimation()
                }
            }
            3 -> {
                RankingFragment.currentFilterIndex = 0
                mainMenuTabView.setBackgroundResource(R.drawable.kk_gradient_tabmenu_ranking_rect)
                toolbar.toolbarLottieView.apply {
                    setAnimation("kk_lottie_ranking.json")
                    playAnimation()
                }
            }
        }
        resetTabButtonItem()
    }

    private fun resetTabButtonItem() {
        tabButtonItems?.forEachIndexed { index, relativeLayout ->
            relativeLayout.isSelected = index == viewModel.tabIndex
        }
    }

    fun onRankSortClick(currentFilterIndex: Int) {
        rankingDimView.visibility = View.VISIBLE
        when (currentFilterIndex) {
            0 -> {
                allRankingView.visibility = View.GONE
                maleRankingView.visibility = View.VISIBLE
                femaleRankingView.visibility = View.VISIBLE
            }
            1 -> {
                allRankingView.visibility = View.VISIBLE
                maleRankingView.visibility = View.GONE
                femaleRankingView.visibility = View.VISIBLE
            }
            2 -> {
                allRankingView.visibility = View.VISIBLE
                maleRankingView.visibility = View.VISIBLE
                femaleRankingView.visibility = View.GONE
            }
        }
    }
}