package com.krosskomics.mainmenu.repository

import android.content.Context
import com.krosskomics.common.model.Genre
import com.krosskomics.common.model.More
import com.krosskomics.common.repository.CommonRepository
import com.krosskomics.util.CODE
import com.krosskomics.util.CommonUtil
import com.krosskomics.util.ServerUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainMenuRepository : CommonRepository(){
    fun requestMain(context: Context, menu: String, p: String?, page: Int) {
        val api: Call<More> = ServerUtil.service.getSeriesMenu(
            CommonUtil.read(context, CODE.CURRENT_LANGUAGE, "en"),
            menu, p, page)
        api.enqueue(object : Callback<More> {
            override fun onResponse(call: Call<More>, response: Response<More>) {
                if (response.body() != null) {
                    mainLiveData.postValue(response.body());
                }
            }

            override fun onFailure(call: Call<More>, t: Throwable) {
                mainLiveData.postValue(null)
            }
        })
    }

    fun requestGenre(context: Context, menu: String, p: String?) {
        val api: Call<Genre> = ServerUtil.service.getSeriesGenre(
            CommonUtil.read(context, CODE.CURRENT_LANGUAGE, "en"),
            menu, p)
        api.enqueue(object : Callback<Genre> {
            override fun onResponse(call: Call<Genre>, response: Response<Genre>) {
                if (response.body() != null) {
                    mainLiveData.postValue(response.body());
                }
            }

            override fun onFailure(call: Call<Genre>, t: Throwable) {
                mainLiveData.postValue(null)
            }
        })
    }
}