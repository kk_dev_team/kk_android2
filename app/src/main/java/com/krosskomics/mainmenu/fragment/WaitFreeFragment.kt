package com.krosskomics.mainmenu.fragment

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.krosskomics.KJKomicsApp
import com.krosskomics.R
import com.krosskomics.coin.activity.CashHistoryActivity
import com.krosskomics.common.data.DataBook
import com.krosskomics.common.data.DataWaitFreeTerm
import com.krosskomics.common.fragment.RecyclerViewBaseFragment
import com.krosskomics.common.model.*
import com.krosskomics.library.activity.LibraryActivity
import com.krosskomics.library.viewmodel.LibraryViewModel
import com.krosskomics.mainmenu.viewmodel.MainMenuViewModel
import kotlinx.android.synthetic.main.fragment_genre.*
import kotlinx.android.synthetic.main.fragment_genre.nestedScrollView
import kotlinx.android.synthetic.main.fragment_genre.recyclerView
import kotlinx.android.synthetic.main.fragment_genre_detail.*

class WaitFreeFragment : RecyclerViewBaseFragment() {

    override val viewModel: MainMenuViewModel by lazy {
        ViewModelProvider(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return MainMenuViewModel(requireContext()) as T
            }
        }).get(MainMenuViewModel::class.java)
    }

    override fun getLayoutId(): Int {
        recyclerViewItemLayoutId = R.layout.item_waitfree
        return R.layout.fragment_waitfree
    }

    override fun initLayout() {
        viewModel.tabIndex = 1
        viewModel.param1 = "waitorpay"
        super.initLayout()
    }

    override fun initModel() {
        arguments?.let {
            viewModel.listType = it.getString("listType").toString()
            val position = it.getInt("position")
            viewModel.param2 = KJKomicsApp.INIT_SET.genre?.get(position)?.p_genre
        }
        super.initModel()
    }

//    override fun setMainContentView(body: Any) {
//        if (viewModel.isRefresh) {
//            viewModel.items.clear()
//        }
//        when (body) {
//            is More -> {
//                if (body.list.isNullOrEmpty()) {
//                    showEmptyView(body.msg)
//                    return
//                }
//                viewModel.totalPage = body.tot_pages
//                body.list?.let {
//                    showMainView()
//                    viewModel.items.addAll(it)
//                    recyclerView.adapter?.notifyDataSetChanged()
//
//                    viewModel.isFirstEnter = false
//                }
//                if (viewModel.isRefresh) {
//                    if (nestedScrollView != null) {
//                        nestedScrollView.scrollTo(0, 0)
//                    } else {
//                        recyclerView?.layoutManager?.scrollToPosition(0)
//                    }
//                }
//                // 기다무
//                if (viewModel.isRefresh) return
//                when (viewModel.tabIndex) {
//                    1 -> {
//                        body.wop_term?.let {
//                            if (viewModel is MainMenuViewModel) {
//                                if ((viewModel as MainMenuViewModel).waitFreeTermItems.isNullOrEmpty()) {
//                                    (viewModel as MainMenuViewModel).waitFreeTermItems = arrayListOf()
//                                    val item = DataWaitFreeTerm()
//                                    item.dp_wop_term_text = ""
//                                    item.isSelect = true
//                                    item.p_wop_term = ""
//                                    item.dp_wop_term_num = "All"
//                                    (viewModel as MainMenuViewModel).waitFreeTermItems?.add(item)
//                                    (viewModel as MainMenuViewModel).waitFreeTermItems?.addAll(it)
//                                    initWaitFreeTermRecyclerView()
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    }
}