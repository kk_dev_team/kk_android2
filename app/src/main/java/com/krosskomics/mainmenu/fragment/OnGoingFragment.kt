package com.krosskomics.mainmenu.fragment

import android.view.MotionEvent
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.krosskomics.KJKomicsApp
import com.krosskomics.R
import com.krosskomics.common.data.DataWeek
import com.krosskomics.common.fragment.RecyclerViewBaseFragment
import com.krosskomics.mainmenu.adapter.OnGoingWeekAdapter
import com.krosskomics.mainmenu.viewmodel.MainMenuViewModel
import com.krosskomics.util.CommonUtil
import kotlinx.android.synthetic.main.fragment_ongoing.*

class OnGoingFragment : RecyclerViewBaseFragment() {

    override val viewModel: MainMenuViewModel by lazy {
        ViewModelProvider(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return MainMenuViewModel(requireContext()) as T
            }
        }).get(MainMenuViewModel::class.java)
    }

    lateinit var dateViewItems: ArrayList<DataWeek>
    override fun getLayoutId(): Int {
        recyclerViewItemLayoutId = R.layout.item_ongoing
        return R.layout.fragment_ongoing
    }

    override fun requestServer() {
        if (viewModel.param2.isNullOrEmpty()) return
        super.requestServer()
    }

    override fun initModel() {
        arguments?.let {
            viewModel.listType = it.getString("listType").toString()
            val position = it.getInt("position")
            viewModel.param2 = KJKomicsApp.INIT_SET.genre?.get(position)?.p_genre
        }
        super.initModel()
    }

    override fun initLayout() {
        viewModel.tabIndex = 0
        viewModel.param1 = "ongoing"
        super.initLayout()
        initDateView()
    }

    private fun initDateView() {
        dateViewItems = arrayListOf(
                DataWeek("MON", "mon", false),
                DataWeek("TUE", "tue", false),
                DataWeek("WED", "wed", false),
                DataWeek("THU", "thu", false),
                DataWeek("FRI", "fri", false),
                DataWeek("SAT", "sat", false),
                DataWeek("SUN", "sun", false),
                DataWeek("FIN", "fin", false)
        )
        // 오늘 요일 얻어오기
        val selectIndex = if (CommonUtil.getDayWeek() == 1) {
            6
        } else {
            CommonUtil.getDayWeek() - 2
        }
        resetDateViewItems(selectIndex)
        initOnGoingWeekRecyclerView()
        viewModel.param2 = dateViewItems[selectIndex].param as String
    }

    private fun resetDateViewItems(selectIndex: Int = -1) {
        dateViewItems.forEachIndexed { index, _ ->
            val item = dateViewItems[index]
            item.isSelect = selectIndex == index
        }
    }

    private fun initOnGoingWeekRecyclerView() {
        weekRecyclerView.apply {
            adapter = OnGoingWeekAdapter(dateViewItems)
            (adapter as OnGoingWeekAdapter).setOnItemClickListener(object : OnGoingWeekAdapter.OnItemClickListener {
                override fun onItemClick(item: Any?, position: Int) {
                    if (item is DataWeek) {
                        resetDateViewItems(position)
                        adapter?.notifyDataSetChanged()
                        viewModel.isRefresh = true
                        viewModel.page = 1
                        viewModel.param2 = item.param as String
//                        convertDayToString(index)
                        requestServer()
                    }
                }
            })
            addOnItemTouchListener(object : RecyclerView.OnItemTouchListener {
                override fun onInterceptTouchEvent(recyclerView: RecyclerView, event: MotionEvent): Boolean {
                    when (event.action) {
                        MotionEvent.ACTION_DOWN -> recyclerView.parent
                            .requestDisallowInterceptTouchEvent(true)
                    }
                    return false
                }

                override fun onTouchEvent(p0: RecyclerView, p1: MotionEvent) {}

                override fun onRequestDisallowInterceptTouchEvent(p0: Boolean) {}

            })
        }
    }
}