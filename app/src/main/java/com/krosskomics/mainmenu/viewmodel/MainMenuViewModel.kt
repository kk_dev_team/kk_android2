package com.krosskomics.mainmenu.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import com.krosskomics.common.data.DataGenre
import com.krosskomics.common.data.DataWaitFreeTerm
import com.krosskomics.common.viewmodel.FragmentBaseViewModel
import com.krosskomics.mainmenu.repository.MainMenuRepository

class MainMenuViewModel(context: Context): FragmentBaseViewModel(context) {
    private val repository =
        MainMenuRepository()
    private val mainResponseLiveData = repository.getMainResponseLiveData()

    var param1: String = ""

    var waitFreeTermItems: ArrayList<DataWaitFreeTerm>? = null
    var genreItems: ArrayList<DataGenre>? = null

    override fun requestMain() {
        repository.requestMain(context, param1, param2, page)
    }

    override fun getMainResponseLiveData(): LiveData<Any> {
        return mainResponseLiveData
    }
}