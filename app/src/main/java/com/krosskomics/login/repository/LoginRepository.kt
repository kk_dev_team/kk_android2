package com.krosskomics.login.repository

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.krosskomics.KJKomicsApp
import com.krosskomics.common.model.Default
import com.krosskomics.common.model.Login
import com.krosskomics.common.repository.CommonRepository
import com.krosskomics.util.CODE
import com.krosskomics.util.CommonUtil
import com.krosskomics.util.ServerUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class LoginRepository(val context: Context) : CommonRepository(){
    private lateinit var loginLiveData: MutableLiveData<Login>
    private lateinit var findPasswordLiveData: MutableLiveData<Default>

    var pageType = CODE.LOGIN_MODE
    var id = ""
    var password = ""
    var loginType = ""
    var oprofile = ""
    var snsToken = ""
    var fbEmail = ""
    var fbName = ""

    var language = "en"
    var signOutInfoStep = 1

    fun requestLogin() {
        if (CommonUtil.read(context, CODE.LOCAL_token, "").isNullOrEmpty()) {
            checkFCMToken(context)
            return
        }
        val api: Call<Login> = ServerUtil.service.setLoginKrossApi(
            CommonUtil.read(context, CODE.CURRENT_LANGUAGE, "en"),
            loginType,
            snsToken,
            CommonUtil.read(context, CODE.LOCAL_token, ""),
            id,
            password,
            advid = KJKomicsApp.AD_Id
        )
        api.enqueue(object : Callback<Login> {
            override fun onResponse(call: Call<Login>, response: Response<Login>) {
                if (response.body() != null) {
                    loginLiveData.postValue(response.body());
                }
            }

            override fun onFailure(call: Call<Login>, t: Throwable) {
                loginLiveData.postValue(null)
            }
        })
    }

//    fun requestSNSLogin() {
//        val api: Call<Login> = ServerUtil.service.setLoginSNS(
//            CommonUtil.read(context, CODE.CURRENT_LANGUAGE, "en"),
//            loginType,
//            CommonUtil.read(context, CODE.LOCAL_token, ""),
//            snsToken
//        )
//        api.enqueue(object : Callback<Login> {
//            override fun onResponse(call: Call<Login>, response: Response<Login>) {
//                if (response.body() != null) {
//                    loginLiveData.postValue(response.body());
//                }
//            }
//
//            override fun onFailure(call: Call<Login>, t: Throwable) {
//                loginLiveData.postValue(null)
//            }
//        })
//    }

    fun requestSignUp() {
        val api: Call<Login> = ServerUtil.service.signUpKrossApi(
            CommonUtil.read(context, CODE.CURRENT_LANGUAGE, "en"),
            loginType,
            id,
            password,
            snsToken,
            KJKomicsApp.LOGIN_DATA?.nickname,
            KJKomicsApp.LOGIN_DATA?.gender,
            KJKomicsApp.LOGIN_DATA?.age,
            KJKomicsApp.LOGIN_DATA?.genreString,
            "",
            KJKomicsApp.DEEPLINK_RID
        )
        api.enqueue(object : Callback<Login> {
            override fun onResponse(call: Call<Login>, response: Response<Login>) {
                KJKomicsApp.DEEPLINK_RID = ""
                if (response.body() != null) {
                    loginLiveData.postValue(response.body());
                }
            }

            override fun onFailure(call: Call<Login>, t: Throwable) {
                loginLiveData.postValue(null)
            }
        })
    }

    fun requestFindPassword() {
        val api: Call<Default> = ServerUtil.service.setAccountKross(
            CommonUtil.read(context, CODE.CURRENT_LANGUAGE, "en"),
            "reset_passwd",
            id
        )
        api.enqueue(object : Callback<Default> {
            override fun onResponse(call: Call<Default>, response: Response<Default>) {
                if (response.body() != null) {
                    findPasswordLiveData.postValue(response.body());
                }
            }

            override fun onFailure(call: Call<Default>, t: Throwable) {
                loginLiveData.postValue(null)
            }
        })
    }

    fun getLoginResponseLiveData(): LiveData<Login> {
        loginLiveData = MutableLiveData()
        return loginLiveData
    }

    fun getFindPasswordResponseLiveData(): LiveData<Default> {
        findPasswordLiveData = MutableLiveData()
        return findPasswordLiveData
    }

    private fun checkFCMToken(context: Context) {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                val token = task.result?.token
                CommonUtil.write(context, CODE.LOCAL_token, token)
                requestLogin()
            })
    }
}