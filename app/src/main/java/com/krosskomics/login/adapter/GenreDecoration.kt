package com.krosskomics.login.adapter

import android.content.Context
import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.krosskomics.util.CommonUtil

class GenreDecoration(context: Context) : RecyclerView.ItemDecoration() {
    var topMargin = 0
    var centerMargin = 0
    var centerMargin1 = 0
    init {
        topMargin = CommonUtil.dpToPx(context, 10)
        centerMargin = CommonUtil.dpToPx(context, 7)
        centerMargin1 = CommonUtil.dpToPx(context, 14)
    }
    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)

        // 상하 설정
        outRect.top = topMargin

        // 가운데 여백
        // spanIndex = 0 -> 왼쪽
        // spanIndex = 1 -> 가운데
        // spanIndex = 2 -> 오른쪽
        var lp = view.layoutParams as GridLayoutManager.LayoutParams
        var spanIndex = lp.spanIndex

        when (spanIndex) {
            0 -> {
                outRect.right = centerMargin
            }
            1 -> {
                outRect.left = centerMargin
                outRect.right = centerMargin
            }
            2 -> {
                outRect.left = centerMargin1
            }
            3 -> {
                outRect.right = centerMargin
            }
            4 -> {
                outRect.left = centerMargin
                outRect.right = centerMargin
            }
            5 -> {
                outRect.left = centerMargin1
            }
            6 -> {
                outRect.right = centerMargin
            }
            7 -> {
                outRect.left = centerMargin
                outRect.right = centerMargin
            }
            8 -> {
                outRect.left = centerMargin1
            }
        }
    }
}