package com.krosskomics.login.adapter

import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.krosskomics.R
import com.krosskomics.common.data.DataLoginGenre
import com.krosskomics.common.holder.BaseItemViewHolder
import com.krosskomics.util.CommonUtil
import kotlinx.android.synthetic.main.item_info_genre.view.*

class InfoGenreAdapter(private val items: ArrayList<*>) :
    RecyclerView.Adapter<InfoGenreAdapter.CustomItemHolder>() {

    private var onClickListener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomItemHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_info_genre, parent, false)
        return CustomItemHolder(view)
    }

    override fun getItemCount(): Int {
        return if (items.size > 9) 9 else items.size
    }

    override fun onBindViewHolder(holder: CustomItemHolder, position: Int) {
        holder.setData(items[position], position)
    }

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener) {
        this.onClickListener = onItemClickListener
    }

    inner class CustomItemHolder(itemView: View) : BaseItemViewHolder(itemView) {
        override fun setData(item: Any?, position: Int) {
            item?.let {
                if (it is DataLoginGenre) {
                    itemView.apply {
                        val width = ((CommonUtil.getDeviceWidth(itemView.context) - 80 - 28) * 0.26).toFloat()
                        val param = genreView.layoutParams as LinearLayout.LayoutParams
                        param.width = width.toInt()
                        param.height = width.toInt()
                        genreView.layoutParams = param

                        isSelected = it.isSelect
                        genreTextView.text = it.dp_genre
                        genreTextView.isSelected = it.isSelect
                        if (it.isSelect) {
                            genreTextView.typeface = Typeface.DEFAULT_BOLD
                        } else {
                            genreTextView.typeface = Typeface.DEFAULT
                        }
                        when (it.p_genre) {
                            "romance" -> {
                                if (isSelected) {
                                    genreImageView.setImageResource(R.drawable.kk_ic_signup_genre_romance_on)
                                } else {
                                    genreImageView.setImageResource(R.drawable.kk_ic_signup_genre_romance)
                                }
                            }
                            "action" -> {
                                if (isSelected) {
                                    genreImageView.setImageResource(R.drawable.kk_ic_signup_genre_action_on)
                                } else {
                                    genreImageView.setImageResource(R.drawable.kk_ic_signup_genre_action)
                                }
                            }
                            "drama" -> {
                                if (isSelected) {
                                    genreImageView.setImageResource(R.drawable.kk_ic_signup_genre_drama_on)
                                } else {
                                    genreImageView.setImageResource(R.drawable.kk_ic_signup_genre_drama)
                                }
                            }
                            "comedy" -> {
                                if (isSelected) {
                                    genreImageView.setImageResource(R.drawable.kk_ic_signup_genre_comedy_on)
                                } else {
                                    genreImageView.setImageResource(R.drawable.kk_ic_signup_genre_comedy)
                                }
                            }
                            "fantasy" -> {
                                if (isSelected) {
                                    genreImageView.setImageResource(R.drawable.kk_ic_signup_genre_fantasy_on)
                                } else {
                                    genreImageView.setImageResource(R.drawable.kk_ic_signup_genre_fantasy)
                                }
                            }
                            "horror" -> {
                                if (isSelected) {
                                    genreImageView.setImageResource(R.drawable.kk_ic_signup_genre_horror_on)
                                } else {
                                    genreImageView.setImageResource(R.drawable.kk_ic_signup_genre_horror)
                                }
                            }
                            "life" -> {
                                if (isSelected) {
                                    genreImageView.setImageResource(R.drawable.kk_ic_signup_genre_life_on)
                                } else {
                                    genreImageView.setImageResource(R.drawable.kk_ic_signup_genre_life)
                                }
                            }
                            "mystery" -> {
                                if (isSelected) {
                                    genreImageView.setImageResource(R.drawable.kk_ic_signup_genre_mystery_on)
                                } else {
                                    genreImageView.setImageResource(R.drawable.kk_ic_signup_genre_mystery)
                                }
                            }
                            "thriller" -> {
                                if (isSelected) {
                                    genreImageView.setImageResource(R.drawable.kk_ic_signup_genre_thriller_on)
                                } else {
                                    genreImageView.setImageResource(R.drawable.kk_ic_signup_genre_thriller)
                                }
                            }
                            "bl" -> {
                                if (isSelected) {
                                    genreImageView.setImageResource(R.drawable.kk_ic_signup_genre_bl_on)
                                } else {
                                    genreImageView.setImageResource(R.drawable.kk_ic_signup_genre_bl_off)
                                }
                            }
                        }

                        setOnClickListener {
                            onClickListener?.onItemClick(item, position)
                        }
                    }
                }
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(item: Any?, position: Int)
    }
}