package com.krosskomics.coin.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.android.billingclient.api.*
import com.android.billingclient.api.BillingClient.BillingResponseCode.*
import com.google.gson.Gson
import com.krosskomics.BuildConfig
import com.krosskomics.R
import com.krosskomics.coin.adapter.CoinAdapter
import com.krosskomics.coin.inapp.BillingModule
import com.krosskomics.coin.viewmodel.CoinViewModel
import com.krosskomics.common.activity.ToolbarTitleActivity
import com.krosskomics.common.adapter.RecyclerViewBaseAdapter
import com.krosskomics.common.data.DataCoin
import com.krosskomics.common.data.DataPurchase
import com.krosskomics.common.model.Coin
import com.krosskomics.util.CODE
import com.krosskomics.util.CommonUtil
import com.krosskomics.util.CommonUtil.read
import com.krosskomics.util.CommonUtil.setAppsFlyerEvent
import com.krosskomics.util.CommonUtil.toNumFormat
import com.krosskomics.util.CommonUtil.write
import com.krosskomics.util.ServerUtil
import com.krosskomics.util.ServerUtil.service
import com.paytm.pgsdk.PaytmOrder
import com.paytm.pgsdk.PaytmPaymentTransactionCallback
import com.paytm.pgsdk.TransactionManager
import kotlinx.android.synthetic.main.activity_coin.*
import kotlinx.android.synthetic.main.view_coin_select.*
import kotlinx.android.synthetic.main.view_network_error.*
import kotlinx.android.synthetic.main.view_toolbar_trans.*
import kotlinx.android.synthetic.main.view_toolbar_trans.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.set


class CoinActivity : ToolbarTitleActivity() {
    private val TAG = "CoinActivity"

    override val viewModel: CoinViewModel by lazy {
        ViewModelProvider(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return CoinViewModel(application) as T
            }
        }).get(CoinViewModel::class.java)
    }

    var productID: String = ""
    var paytmOrderId: String? = ""
    var currentSkuDetails: SkuDetails? = null
    private var mPaymentRetryCount = 1
    var ActivityRequestCode = 2

    private lateinit var bm: BillingModule
    private var skuDetailItems = listOf<SkuDetails>()
        set(value) {
            field = value
            skuDetailItems
        }

    override fun getLayoutId(): Int {
        recyclerViewItemLayoutId = R.layout.item_coin
        return R.layout.activity_coin
    }

    override fun initTracker() {
        setTracker(getString(R.string.str_cash_shop))
    }

    override fun initLayout() {
        toolbarTitleString = getString(R.string.str_cash_shop)
        super.initLayout()
        initHeaderView()
        initFooterView()
    }

    override fun onResume() {
        super.onResume()
        if (::bm.isInitialized) bm.onResume(BillingClient.SkuType.INAPP)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ActivityRequestCode && data != null) {
            Log.e(TAG, "Payment Transaction response " +
                    data.getStringExtra("nativeSdkForMerchantMessage") + data.getStringExtra("response"))
            requestPaytmStatus(paytmOrderId)
//            Toast.makeText(
//                this,
//                data.getStringExtra("nativeSdkForMerchantMessage") + data.getStringExtra("response"),
//                Toast.LENGTH_SHORT
//            ).show()
        }
    }

    private fun initFooterView() {
        emailTextView.setOnClickListener {
            CommonUtil.sendEmail(context)
        }
    }

    override fun initMainView() {
        super.initMainView()

        initNestedScrollView()
        initInfoView()
    }

    private fun initInfoView() {
        closeInfoImageView.setOnClickListener {
            coinInfoView.visibility = View.GONE
        }
        coinInfoView.setOnClickListener { coinInfoView.visibility = View.GONE }
    }

    private fun initHeaderView() {
        keyTextView.text = read(context, CODE.LOCAL_coin, "0")
    }

    fun setCashView(cash: Int = 0, bonusCash: Int = 0, cashInfo: String?) {
        totalCashTextView.text = "${CommonUtil.toNumFormat2(cash + bonusCash)}"
        originCashTextView.text = " ${CommonUtil.toNumFormat2(cash)}"
        bonusCashTextView.text = " ${CommonUtil.toNumFormat2(bonusCash)}"
        cashInfoTextView.text = cashInfo
    }

    override fun initToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowTitleEnabled(false)
            setHomeAsUpIndicator(R.drawable.kk_icon_back_white)
        }
        toolbarTitle.text = toolbarTitleString
        toolbar.toolbarInfo.setOnClickListener {
            if (coinInfoView.isShown) {
                coinInfoView.visibility = View.GONE
            } else {
                coinInfoView.visibility = View.VISIBLE
            }
        }
    }

    private fun initNestedScrollView() {
        nestedScrollView?.setOnScrollChangeListener { v: NestedScrollView?, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int ->
            if (scrollY > 0) {
                toolbar?.setBackgroundColor(ContextCompat.getColor(context, R.color.black))
            } else {
                toolbar?.setBackgroundColor(ContextCompat.getColor(context, R.color.trans))
            }
        }
    }

    private fun initInApp() {
//        coinSelectView.setOnClickListener {
//            it.visibility = View.GONE
//        }
        cancelButton.setOnClickListener {
            coinSelectView.visibility = View.GONE
        }
        googlePlaySelectView.setOnClickListener {
            currentSkuDetails?.let { skuDetails ->
                bm.purchase(skuDetails)
            }
            coinSelectView.visibility = View.GONE
        }
        paytmSelectView.setOnClickListener {
            requestPaytm()
            coinSelectView.visibility = View.GONE
        }
        initGoogleInapp()
        intPaytm()
    }

    private fun intPaytm() {

    }

    private fun initGoogleInapp() {
        bm = BillingModule(this, lifecycleScope, object : BillingModule.BillingCallback {
            override fun onBillingModulesIsReady() {
                val skuList = ArrayList<String>()
                (viewModel.items as ArrayList<DataCoin>).forEach {
                    skuList.add(it.product_id)
                }
                bm.querySkuDetail(BillingClient.SkuType.INAPP, skuList) { skuDetails ->
                    skuDetailItems = skuDetails
                }
            }

            override fun onSuccess(purchase: Purchase) {
                storePurchaseData(purchase)
                savePayment(purchase)
            }

            override fun onFailure(errorCode: Int, errorMsg: String?) {
                var eventName = "af_purchase_fail"
                val eventValue: MutableMap<String, Any?> =
                    HashMap()
                eventValue["af_item_id"] = productID

                when (errorCode) {
//                    BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED -> {
//                        showInAppAlert(errorMsg ?: "")
//                    }
                    BillingClient.BillingResponseCode.USER_CANCELED -> {
                        Toast.makeText(context, viewModel.cancelMsg, Toast.LENGTH_SHORT)
                            .show()
                        eventName = "af_purchase_cancel"
                    }
                    else -> {
                        viewModel.failMsg?.let {
                            showInAppAlert(it)
                        } ?: run {
                            showInAppAlert(errorMsg ?: "")
                        }
                    }
                }
                setAppsFlyerEvent(context, eventName, eventValue)
            }
        })
    }

    override fun setMainContentView(body: Any) {
        super.setMainContentView(body)
        initInApp()
    }

    override fun initRecyclerViewAdapter() {
        recyclerView.adapter = CoinAdapter(viewModel.items, recyclerViewItemLayoutId)
        (recyclerView.adapter as RecyclerViewBaseAdapter).setOnItemClickListener(object :
            RecyclerViewBaseAdapter.OnItemClickListener {
            override fun onItemClick(item: Any?, position: Int) {
                if (item is DataCoin) {
                    if (read(context, CODE.LOCAL_loginYn, "N").equals("Y", ignoreCase = true)) {
                        skuDetailItems.forEach {
                            if (item.product_id == it.sku) {
//                                bm.purchase(it)
                                currentSkuDetails = it
                                productID = it.sku
                                coinSelectView.visibility = View.VISIBLE
                                return
                            }
                        }
                    } else {
                        goLoginAlert(context)
                    }
                }
            }
        })
    }

    private fun storePurchaseData(purchase: Purchase) {
        //    u_token(헤더로보내는 유저키), orderid, productid, purchasetime, purchasestate, purchasetoken
        val storeData = DataPurchase().apply {
            u_token = read(context, CODE.LOCAL_user_no, "")
            orderid = purchase.orderId
            productid = productID
            purchasetime = purchase.purchaseTime
            purchasestate = purchase.purchaseState
            purchasetoken = purchase.purchaseToken
        }
        val storeDataJson = Gson().toJson(storeData)
        val storeDataString = storeDataJson.toString()
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "storeDataJson : " + storeDataJson)
        }
        write(context, CODE.LOCAL_PURCHASE_DATA, storeDataString)
    }

    private fun savePayment(purchase: Purchase?) {
        Log.e(TAG, "savePayment enter purchase : " + purchase)
//        showToast("savePayment enter purchase : " + purchase, context)
//            showProgress(context)
        if (purchase == null) {
//            commonAlert(context, "구매결과정보가 없어서 코인 충전 요청을 하지 않습니다.");
            return
        }
//        commonAlert(context, "결제 성공(purchase 있음), 코인 충전 request 시작");
//            logger.info("결제 성공(purchase 있음), 코인 충전 request 시작");
        val api = service.getInappPurchase(
            read(context, CODE.CURRENT_LANGUAGE, "en"),
            productID, purchase.orderId,
            purchase.purchaseTime, purchase.purchaseState, purchase.purchaseToken
        )
        api.enqueue(object : Callback<Coin> {
            override fun onResponse(call: Call<Coin>, response: Response<Coin>) {
                try {
                    if (response.isSuccessful) {
                        val body = response.body()
                        if ("00" == body?.retcode) {
//                                logger.debug("구매, 코인 충전 성공했으며 현재 " + body.user_coin + " 코인이 있습니다.");
                            write(context, CODE.LOCAL_PURCHASE_DATA, null)
                            val coin = body.cash.toString()
                            var totalCoin = body.cash
//                            commonAlert(context, "구매, 코인 충전 성공했으며 현재 ${coin} 코인이 있습니다.")
                            if ("" != coin) {
//                                    Intent intent = new Intent(CODE.LB_CHARGE_COIN);
//                                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                                if (body.bonus_cash ?: 0 > 0) {
                                    totalCoin = coin.toInt().plus(body.bonus_cash ?: 0)
                                }
                                write(context, CODE.LOCAL_coin, totalCoin.toString())
                                val numFormatCoin = toNumFormat(coin.toInt())
                                keyTextView.text =
                                    numFormatCoin + " " + getString(R.string.str_coins)
                            }
                            var eventName = "af_purchase"
                            val eventValue: MutableMap<String, Any?> =
                                HashMap()
                            eventValue["af_revenue"] = body.price
                            eventValue["af_currency"] = body.currency
                            eventValue["af_item_id"] = productID
                            eventValue["af_order_id"] = body.order_id
                            eventValue["af_method"] = "googlepay"
                            if ("1" == body.first_order) { // 첫구매
                                eventName = "first_purchase"
                            }
                            setAppsFlyerEvent(context, eventName, eventValue)

                            viewModel.isRefresh = true
                            requestServer()
                        } else if ("999" == body?.retcode) {
                        } else {
                            write(context, CODE.LOCAL_PURCHASE_DATA, null)
//                            commonAlert(
//                                this@CoinActivity,
//                                "구매 성공, 코인 충전 실패했습니다. 리턴코드는 :" + body?.retcode + "입니다."
//                            )
//                                logger.error("구매 성공, 코인 충전 실패했습니다. 리턴코드는 : " + body.retcode + "입니다.");
                        }
                        if ("" != body?.msg) {
//                            commonAlert(this@CoinActivity, body?.msg)
                            showInAppAlert(body?.msg ?: "")
                        }
                    } else {
//                            logger.error("코인 충전 서버와의 통신은 성공했지만 응답이 실패했습니다.");
                        write(context, CODE.LOCAL_PURCHASE_DATA, null)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    //                        logger.error("코인 충전중 예외상황발생 msg : " + e.getMessage());
//                    commonAlert(
//                        this@CoinActivity,
//                        getString(R.string.msg_fail_inapp_give_coin)
//                    )
                    showInAppAlert(getString(R.string.msg_fail_inapp_give_coin))
                }
            }

            override fun onFailure(call: Call<Coin?>, t: Throwable) {
                t.printStackTrace()
                if (mPaymentRetryCount <= 3) {
//                        logger.error(mPaymentRetryCount + "번째 결제 실패했습니다. 다시 코인 충전 요청합니다. 에러메시지 : " + t.getMessage());
                    savePayment(purchase)
                    mPaymentRetryCount++
                } else {
                    try {
                        checkNetworkConnection(this@CoinActivity, t, errorView)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        })
    }

    fun requestPaytm() {
        val api: Call<Coin> = ServerUtil.service.getPaytmData(
            read(context, CODE.CURRENT_LANGUAGE, "en"),
            productID
        )
        api.enqueue(object : Callback<Coin> {
            override fun onResponse(call: Call<Coin>, response: Response<Coin>) {
                response.body()?.let {
                    if (it.retcode == "00") {
                        val paytmOrder = PaytmOrder(
                            it.orderid,
                            it.mid,
                            it.txnToken,
                            it.amount,
                            it.callbackurl
                        )
                        paytmOrderId = it.orderid
                        val transactionManager = TransactionManager(
                            paytmOrder,
                            object : PaytmPaymentTransactionCallback {
                                override fun onTransactionResponse(response: Bundle?) {
//                                    Toast.makeText(context, "Payment Transaction response " + response.toString(), Toast.LENGTH_LONG).show()
                                    Log.e(TAG, "Payment Transaction response " + response.toString())
                                    requestPaytmStatus(paytmOrderId)
                                }

                                override fun networkNotAvailable() {
                                    Log.e(TAG, "networkNotAvailable")
                                }

                                override fun onErrorProceed(error: String?) {
                                    Log.e(TAG, "onErrorProceed error : " + error)
                                }

                                override fun clientAuthenticationFailed(error: String?) {
                                    Log.e(TAG, "clientAuthenticationFailed error : " + error)
                                }

                                override fun someUIErrorOccurred(error: String?) {
                                    Log.e(TAG, "someUIErrorOccurred error : " + error)
                                }

                                override fun onErrorLoadingWebPage(p0: Int, p1: String?, p2: String?) {
                                    Log.e(TAG, "onErrorLoadingWebPage")
                                }

                                override fun onBackPressedCancelTransaction() {
                                    Log.e(TAG, "onBackPressedCancelTransaction")
                                }

                                override fun onTransactionCancel(p0: String?, p1: Bundle?) {
                                    Log.e(TAG, "onTransactionCancel")
                                }

                            })
                        transactionManager.startTransaction(this@CoinActivity, ActivityRequestCode);
//                    PaytmOrder paytmOrder = new PaytmOrder(orderid, mid, txnToken, amount, callbackurl)
//                    TransactionManager transactionManager = new TransactionManager(paytmOrder, new PaytmPaymentTransactionCallback() // code statement);
                    }
                }
            }

            override fun onFailure(call: Call<Coin>, t: Throwable) {
                t.printStackTrace()
            }
        })
    }

    private fun requestPaytmStatus(orderid: String?) {
        val api: Call<Coin> = ServerUtil.service.getPaytmStatus(
            read(context, CODE.CURRENT_LANGUAGE, "en"),
            orderid
        )
        api.enqueue(object : Callback<Coin> {
            override fun onResponse(call: Call<Coin>, response: Response<Coin>) {
                response.body()?.let { body ->
                    try {
                        if ("00" == body?.retcode) {
//                                logger.debug("구매, 코인 충전 성공했으며 현재 " + body.user_coin + " 코인이 있습니다.");
                            write(context, CODE.LOCAL_PURCHASE_DATA, null)
                            val coin = body.cash?.toString()
                            var totalCoin = body.cash
//                            commonAlert(context, "구매, 코인 충전 성공했으며 현재 ${coin} 코인이 있습니다.")
                            if (coin?.isNotEmpty() == true) {
//                                    Intent intent = new Intent(CODE.LB_CHARGE_COIN);
//                                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                                if (body.bonus_cash ?: 0 > 0) {
                                    totalCoin = coin.toInt().plus(body.bonus_cash ?: 0)
                                }
                                write(context, CODE.LOCAL_coin, totalCoin.toString())
                                val numFormatCoin = toNumFormat(coin.toInt())
                                keyTextView.text =
                                    numFormatCoin + " " + getString(R.string.str_coins)
                            }
                            var eventName = "af_purchase"
                            val eventValue: MutableMap<String, Any?> =
                                HashMap()
                            eventValue["af_revenue"] = body.price
                            eventValue["af_currency"] = body.currency
                            eventValue["af_item_id"] = productID
                            eventValue["af_order_id"] = body.order_id
                            eventValue["af_method"] = "paytm"
                            if ("1" == body.first_order) { // 첫구매
                                eventName = "first_purchase"
                            }
                            setAppsFlyerEvent(context, eventName, eventValue)

                            viewModel.isRefresh = true
                            requestServer()
                        } else if ("999" == body?.retcode) {
                        } else {
                            write(context, CODE.LOCAL_PURCHASE_DATA, null)
//                            commonAlert(
//                                this@CoinActivity,
//                                "구매 성공, 코인 충전 실패했습니다. 리턴코드는 :" + body?.retcode + "입니다."
//                            )
//                                logger.error("구매 성공, 코인 충전 실패했습니다. 리턴코드는 : " + body.retcode + "입니다.");
                        }
                        if ("" != body?.msg) {
//                            commonAlert(this@CoinActivity, body?.msg)
                            showInAppAlert(body?.msg ?: "")
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<Coin>, t: Throwable) {
                t.printStackTrace()
            }
        })
    }

    private fun showInAppAlert(msg: String) {
        try {
            val innerView =
                layoutInflater.inflate(R.layout.dialog_default, null)
            val dialog = initDialog(innerView)
            val tvTitle = innerView.findViewById<TextView>(R.id.tv_title)
            val msgTextView = innerView.findViewById<TextView>(R.id.tv_msg)
            val btnConfirm =
                innerView.findViewById<Button>(R.id.btn_confirm)
            val btnCancel =
                innerView.findViewById<Button>(R.id.btn_cancel)
            tvTitle.text = "Payment Result"
            msgTextView.text = msg
            btnCancel.visibility = View.GONE
            btnConfirm.setOnClickListener {
                dialog.dismiss()
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }
}