package com.krosskomics.coin.activity

import com.krosskomics.R
import com.krosskomics.common.activity.ToolbarViewPagerActivity
import com.krosskomics.util.CODE
import com.krosskomics.util.CommonUtil
import kotlinx.android.synthetic.main.activity_cash_history.*
import kotlinx.android.synthetic.main.view_toolbar_black.*

class CashHistoryActivity : ToolbarViewPagerActivity() {
    private val TAG = "CashHistoryActivity"

    override var tabIndex = 0

    override fun getLayoutId(): Int {
        return R.layout.activity_cash_history
    }

    override fun requestServer() {}

    override fun initTracker() {
        setTracker(getString(R.string.str_cash_history))
    }

    override fun initLayout() {
        toolbarTitleString = getString(R.string.str_cash_history)
        adapterType = 2
        super.initLayout()

        var cash = CommonUtil.read(context, CODE.LOCAL_coin, "0") ?: "0"
        cash = CommonUtil.toNumFormat2(cash.toInt()).toString()
        myCashTextView.text = getString(R.string.str_my_cash_format, cash)
        setCashView()
    }

    override fun initModel() {
        tabIndex = intent?.getIntExtra("tabIndex", 0) ?: 0
        super.initModel()
    }

    override fun initToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowTitleEnabled(false)
            setHomeAsUpIndicator(R.drawable.kk_icon_back_white)
        }
        toolbarTitle.text = toolbarTitleString
    }

    override fun initTabItems() {
        tabItems = listOf(getString(R.string.str_charged), getString(R.string.str_used))
    }

    fun setCashView(cash: String? = "0", bonusCash: String? = "0") {
        val myCash = cash?.toInt()?.plus(bonusCash?.toInt() ?: 0)
        myCashTextView.text = getString(R.string.str_my_cash_format, CommonUtil.toNumFormat2(myCash ?: 0))
        cashTextView.text = " ${CommonUtil.toNumFormat2(cash?.toInt() ?: 0)}"
        bonusCashTextView.text = " ${CommonUtil.toNumFormat2(bonusCash?.toInt() ?: 0)}"
    }
}