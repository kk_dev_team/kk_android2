package com.krosskomics.coin.repository

import android.content.Context
import com.krosskomics.common.model.Coin
import com.krosskomics.common.repository.CommonRepository
import com.krosskomics.util.CODE
import com.krosskomics.util.CommonUtil
import com.krosskomics.util.ServerUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class CoinRepository : CommonRepository() {
    fun requestMain(context: Context) {
        val api: Call<Coin> = ServerUtil.service.getInappData(
            CommonUtil.read(context, CODE.CURRENT_LANGUAGE, "en")
        )
        api.enqueue(object : Callback<Coin> {
            override fun onResponse(call: Call<Coin>, response: Response<Coin>) {
                if (response.body() != null) {
                    mainLiveData.postValue(response.body());
                }
            }

            override fun onFailure(call: Call<Coin>, t: Throwable) {
                mainLiveData.postValue(null)
            }
        })
    }
}