package com.krosskomics.invite.activity

import android.content.Intent
import android.graphics.drawable.Animatable
import android.net.Uri
import android.view.View
import android.widget.FrameLayout
import androidx.annotation.Nullable
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.controller.BaseControllerListener
import com.facebook.drawee.controller.ControllerListener
import com.facebook.drawee.interfaces.DraweeController
import com.facebook.imagepipeline.common.Priority
import com.facebook.imagepipeline.common.ResizeOptions
import com.facebook.imagepipeline.image.ImageInfo
import com.facebook.imagepipeline.request.ImageRequestBuilder
import com.krosskomics.R
import com.krosskomics.common.activity.RecyclerViewBaseActivity
import com.krosskomics.common.model.Invite
import com.krosskomics.util.CODE
import com.krosskomics.util.CommonUtil
import com.krosskomics.util.ServerUtil
import kotlinx.android.synthetic.main.activity_invite.*
import kotlinx.android.synthetic.main.view_network_error.*
import retrofit2.Call
import retrofit2.Response

class InviteActivity : RecyclerViewBaseActivity() {
    private val TAG = "InviteActivity"

    var inviteLink: String? = null
    var widthMap: HashMap<String, Float> =
        HashMap()
    var heightMap: HashMap<String, Float> =
        HashMap()

    override fun getLayoutId(): Int {
        return R.layout.activity_invite
    }

    override fun initTracker() {
        setTracker(getString(R.string.str_invite))
    }

    override fun initLayout() {
        toolbarTitleString = getString(R.string.str_invite)
        super.initLayout()

        initView()
    }

    override fun requestServer() {
        requestInviteData()
    }

    private fun initView() {
        if (CommonUtil.read(context, CODE.LOCAL_loginYn, "N").equals("N", ignoreCase = true)) {
            loginButton.visibility = View.VISIBLE
            inviteButton.visibility = View.GONE
        } else {
            loginButton.visibility = View.GONE
            inviteButton.visibility = View.VISIBLE
        }
        loginButton.setOnClickListener {
            goLoginAlert(context)
        }
        backImageView.setOnClickListener { finish() }

        inviteButton.setOnClickListener {
            inviteLink?.let { link ->
                Intent(Intent.ACTION_SEND).apply {
                    type = "text/plain"
                    putExtra(Intent.EXTRA_TEXT, link)
                    startActivity(Intent.createChooser(this, "Share"))
                }
            }
        }
    }

    private fun requestInviteData() {
        val api = ServerUtil.service.getInviteApi(
            CommonUtil.read(context, CODE.CURRENT_LANGUAGE, "en"),
            t = "invite"
        )
        api.enqueue(object : retrofit2.Callback<Invite> {
            override fun onResponse(call: Call<Invite>, response: Response<Invite>) {
                try {
                    if (response.isSuccessful) {
                        val item = response.body()
                        if ("00" == item?.retcode) {
                            item.back_image?.let {
                                setBgImage(it)
                            }
                            inviteLink = item.invite_link
                            inviteNotice.text = item.info
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<Invite>, t: Throwable) {
                hideProgress()
                try {
                    checkNetworkConnection(context, null, errorView)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun setBgImage(item: String) {
        val controllerListener: ControllerListener<ImageInfo> = object :
            BaseControllerListener<ImageInfo>() {
            override fun onFinalImageSet(
                id: String,
                @Nullable imageInfo: ImageInfo?,
                @Nullable anim: Animatable?
            ) {
                if (imageInfo == null) {
                    return
                }
                val qualityInfo = imageInfo.qualityInfo
                if (qualityInfo.isOfGoodEnoughQuality) {
                    val heightTarget = getTargetHeight(
                        imageInfo.width.toFloat(),
                        imageInfo.height.toFloat(),
                        imageItemView,
                        item
                    ) as Float
                    if (heightTarget <= 0) return
                    heightMap[item] = heightTarget
                    updateItemHeight(heightTarget, imageItemView)
                }
                bottomView.visibility = View.VISIBLE
            }

            override fun onIntermediateImageSet(
                id: String,
                @Nullable imageInfo: ImageInfo?
            ) {
            }

            override fun onFailure(
                id: String,
                throwable: Throwable
            ) {
                throwable.printStackTrace()
            }
        }

        if (heightMap.containsKey(item)) {
            val height: Float = heightMap[item]!!
            if (height > 0) {
                updateItemHeight(height, imageItemView)
                bgImageView.setImageURI(Uri.parse(item))
                return
            }
        }

        val requestBuilder =
            ImageRequestBuilder.newBuilderWithSource(Uri.parse(item))
        if (CommonUtil.getDeviceWidth(context) <= 720) {
            requestBuilder.resizeOptions = ResizeOptions(
                CommonUtil.getDeviceWidth(
                    context
                ), CommonUtil.getDeviceWidth(context) * 3
            )
        }
        requestBuilder
            .setRequestPriority(Priority.HIGH).isProgressiveRenderingEnabled =
            true

        val request =
            requestBuilder.build()

        val controller: DraweeController =
            Fresco.newDraweeControllerBuilder()
                .setUri(Uri.parse(item))
                .setOldController(bgImageView.controller)
                .setImageRequest(request)
                .setControllerListener(controllerListener)
                .setTapToRetryEnabled(true)
                .build()

        bgImageView.controller = controller
    }

    private fun getTargetHeight(
        width: Float,
        height: Float,
        view: View,
        url: String
    ): Float {
        val child = view.findViewById<View>(R.id.bgImageView)
        val widthTarget: Float

        if (widthMap.containsKey(url)) {
            widthTarget = widthMap[url]?.toFloat()!!
        } else {
            widthTarget = child.measuredWidth.toFloat()
            if (widthTarget > 0) {
                widthMap[url] = widthTarget
            }
        }
        return height * (widthTarget / width)
    }

    private fun updateItemHeight(height: Float, view: View) {
        val frameLayout = view.findViewById<FrameLayout>(R.id.imageItemView)
        val child = view.findViewById<View>(R.id.bgImageView)
        val layoutParams =
            child.layoutParams as FrameLayout.LayoutParams
        layoutParams.height = height.toInt()
        frameLayout.updateViewLayout(child, layoutParams)
    }
}