package com.krosskomics.library.fragment

import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.krosskomics.KJKomicsApp
import com.krosskomics.R
import com.krosskomics.common.adapter.CommonRecyclerViewAdapter
import com.krosskomics.common.adapter.RecyclerViewBaseAdapter
import com.krosskomics.common.data.DataBook
import com.krosskomics.common.fragment.RecyclerViewBaseFragment
import com.krosskomics.common.model.Default
import com.krosskomics.common.model.User
import com.krosskomics.library.activity.DownloadEpActivity
import com.krosskomics.library.activity.LibraryActivity
import com.krosskomics.library.viewmodel.LibraryViewModel
import com.krosskomics.series.activity.SeriesActivity
import com.krosskomics.util.CODE
import com.krosskomics.util.CommonUtil
import com.krosskomics.util.FileUtils
import com.krosskomics.util.ServerUtil
import kotlinx.android.synthetic.main.fragment_genre.recyclerView
import kotlinx.android.synthetic.main.fragment_genre_detail.*
import kotlinx.android.synthetic.main.fragment_library.*
import kotlinx.android.synthetic.main.fragment_library.nestedScrollView
import kotlinx.android.synthetic.main.item_coin_first.*
import kotlinx.android.synthetic.main.item_underline_1dp.*
import kotlinx.android.synthetic.main.view_empty_common.*
import kotlinx.android.synthetic.main.view_empty_common.emptyView
import kotlinx.android.synthetic.main.view_empty_common.view.*
import kotlinx.android.synthetic.main.view_empty_download.*
import kotlinx.android.synthetic.main.view_mytoon_category.*
import kotlinx.android.synthetic.main.view_mytoon_filter.*
import kotlinx.android.synthetic.main.view_network_error.*
import kotlinx.android.synthetic.main.view_network_error_library.*
import kotlinx.android.synthetic.main.view_network_state.*
import kotlinx.android.synthetic.main.view_toolbar_black.*
import kotlinx.android.synthetic.main.view_toolbar_black.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class LibraryFragment : RecyclerViewBaseFragment() {

    var currentCategory = 0

    override val viewModel: LibraryViewModel by lazy {
        ViewModelProvider(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return LibraryViewModel(requireContext()) as T
            }
        }).get(LibraryViewModel::class.java)
    }

    override fun onResume() {
        super.onResume()
        when ((activity as LibraryActivity).currentCategory) {
            2 -> {
                viewModel.items.clear()
                getDownloadedData()
            }
            else -> {
                if (!viewModel.isFirstEnter) {
                    viewModel.page = 1
                    viewModel.isRefresh = true
                    requestServer()
                }
            }
        }
    }

    override fun getLayoutId(): Int {
        recyclerViewItemLayoutId = R.layout.item_content
        return R.layout.fragment_library
    }

    override fun initLayout() {
        initMainView()
    }

    override fun requestServer() {
        if (CommonUtil.getNetworkInfo(requireContext()) == null && currentCategory != 2) {
            libNetworkErrorView?.visibility = View.VISIBLE
            return
        }
        if (viewModel.isShowProgress) showProgress(requireContext())
        viewModel.requestMain()
    }

    private fun requestEpDeleteFile(sid: String) {
        var eids = viewModel.mEpExpireDateList?.toString()
        eids = eids?.trim { it <= ' ' }?.replace(" ", "")
        eids = eids?.substring(1, eids.length - 1)

        val api = ServerUtil.service.setDeleteEpisodes(CommonUtil.read(context, CODE.CURRENT_LANGUAGE, "en"),
            "delete_download_episode", sid, eids)
        api.enqueue(object : Callback<Default> {
            override fun onResponse(call: Call<Default>, response: Response<Default>) {
                try {
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<Default>, t: Throwable) {
                try {
                    t.printStackTrace()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    /**
     * 삭제
     */
    private fun requestDeleteLibrary() {
        var sids = viewModel.mSeriesList.toString()
        sids = sids.trim { it <= ' ' }.replace(" ", "")
        sids = sids.substring(1, sids.length - 1)
            val api = ServerUtil.service.setUserProfileApi(CommonUtil.read(context, CODE.CURRENT_LANGUAGE, "en"),
                "delete_recent", sids)
            api.enqueue(object : Callback<Default> {
                override fun onResponse(call: Call<Default>, response: Response<Default>) {
                    try {
                        if (response.isSuccessful) {
                            if ("00" == response.body()!!.retcode) {
                                viewModel.page = 1
                                viewModel.isRefresh = true
                                viewModel.mSeriesList.clear()
                                viewModel.isDeletMode = true
                                requestServer()

//                                activity?.toolbarDone?.visibility = View.GONE
//                                activity?.toolbarTrash?.visibility = View.VISIBLE
                            } else {
                                if ("" != response.body()!!.msg) {
                                    CommonUtil.showToast(response.body()!!.msg, context)
                                }
                            }
                        } else {
                            CommonUtil.showToast(getString(R.string.msg_fail_dataloading), context)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

                override fun onFailure(call: Call<Default>, t: Throwable) {
                    try {
                        t.printStackTrace()
//                        checkNetworkConnection(context, t, actBinding.viewError)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            })
    }

    /**
     * 삭제
     */
    private fun requestDeleteFile() {
            var sids = viewModel.mSeriesList.toString()
            sids = sids.trim { it <= ' ' }.replace(" ", "")
            sids = sids.substring(1, sids.length - 1)

            val api = ServerUtil.service.setUserProfileApi(
                "delete_download_series", sids)
            api.enqueue(object : Callback<Default> {
                override fun onResponse(call: Call<Default>, response: Response<Default>) {
                    try {
                        if (response.isSuccessful) {
                            if ("00" == response.body()!!.retcode) {
//                                viewModel.isRefresh = true
                                viewModel.mSeriesList.clear()
//                                getDownloadedData()
                            }
                            if ("" != response.body()!!.msg) {
                                CommonUtil.showToast(response.body()!!.msg, context)
                            }
                        } else {
                            CommonUtil.showToast(getString(R.string.msg_fail_dataloading), context)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

                override fun onFailure(call: Call<Default>, t: Throwable) {
                    try {
                        t.printStackTrace()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            })
    }

    override fun initMainView() {
        super.initMainView()
        activity?.apply {
            toolbarTrash?.setOnClickListener { _ ->
                toolbarTrash.visibility = View.GONE
                toolbarDone.visibility = View.VISIBLE
                viewModel.items.forEach {
                    if (it is DataBook) {
                        it.isCheckVisible = true
                        it.isChecked = true
                    }
                }
                recyclerView.adapter?.notifyDataSetChanged()
            }
            toolbarDone?.setOnClickListener { _ ->
                toolbarTrash.visibility = View.VISIBLE
                toolbarDone.visibility = View.GONE
                viewModel.items.forEach {
                    if (it is DataBook) {
                        it.isCheckVisible = false
                        it.isChecked = false
                    }
                }
                recyclerView.adapter?.notifyDataSetChanged()
            }
        }
        initfilter()
        initCategory()
    }

//    private fun requestLibraryApi() {
//        if (CommonUtil.getNetworkInfo(requireContext()) == null && currentCategory != 2) {
//            libNetworkErrorView?.visibility = View.VISIBLE
//            return
//        }
//        viewModel.requestMain()
//    }

    private fun initfilter() {
        recentCheckView.isSelected = true
        recentCheckView.setOnClickListener {
            subsCheckView.isSelected = false
            recentCheckView.isSelected = true
            viewModel.repository.listType = "recent"
            viewModel.isRefresh = true
            viewModel.page = 1
            if (activity is LibraryActivity) {
                viewModel.isDeletMode = (activity as LibraryActivity).isVisibleToolbarDone()
            }
            requestServer()
        }
        subsCheckView.setOnClickListener {
            subsCheckView.isSelected = true
            recentCheckView.isSelected = false
            viewModel.repository.listType = "subscribe"
            viewModel.isRefresh = true
            viewModel.page = 1
            if (activity is LibraryActivity) {
                viewModel.isDeletMode = (activity as LibraryActivity).isVisibleToolbarDone()
            }
            requestServer()
        }
    }

    private fun initCategory() {
        if ((context as LibraryActivity).currentCategory == 2) {
            currentCategory = 2
        }
        setCategoryView()
        allTextView.setOnClickListener {
            currentCategory = 0
            setCategoryView()
        }
        unlockTextView.setOnClickListener {
            currentCategory = 1
            setCategoryView()
        }
        downloadTextView.setOnClickListener {
            currentCategory = 2
            setCategoryView()
        }
    }

    private fun setCategoryView() {
        when (currentCategory) {
            0 -> {
                networkStateView.visibility = View.GONE
                emptyDownloadView?.visibility = View.GONE
                nestedScrollView.visibility = View.GONE
                resetCategory()
                allTextView.isSelected = true
                allTextView.setTypeface(null, Typeface.BOLD)
                unlockTextView.setTypeface(null, Typeface.NORMAL)
                downloadTextView.setTypeface(null, Typeface.NORMAL)
                activity?.toolbarDone?.visibility = View.GONE
//                activity?.toolbarTrash?.visibility = View.VISIBLE
                if ((activity as LibraryActivity).tabIndex == 0) {
                    activity?.toolbarTrash?.visibility = View.VISIBLE
                } else {
                    activity?.toolbarTrash?.visibility = View.GONE
                }
                filterView.visibility = View.VISIBLE
                (activity as LibraryActivity).currentCategory = 0

                categoryView.setBackgroundColor(Color.WHITE)
                if (recentCheckView.isSelected) {
                    viewModel.repository.listType = "recent"
                } else {
                    viewModel.repository.listType = "subscribe"
                }
                viewModel.page = 1
                viewModel.isRefresh = true
                if (!viewModel.isFirstEnter) {
                    requestServer()
                }
            }
            1 -> {
                emptyDownloadView?.visibility = View.GONE
                networkStateView.visibility = View.GONE
                nestedScrollView.visibility = View.GONE
                resetCategory()
                unlockTextView.isSelected = true
                allTextView.setTypeface(null, Typeface.NORMAL)
                unlockTextView.setTypeface(null, Typeface.BOLD)
                downloadTextView.setTypeface(null, Typeface.NORMAL)

                activity?.toolbarDone?.visibility = View.GONE
                activity?.toolbarTrash?.visibility = View.GONE
                filterView.visibility = View.GONE
                (activity as LibraryActivity).currentCategory = 1

                categoryView.setBackgroundColor(Color.WHITE)
                viewModel.repository.listType = "purchase"
                viewModel.page = 1
                viewModel.isRefresh = true
                requestServer()
            }
            2 -> {
                resetCategory()
                downloadTextView.isSelected = true
                allTextView.setTypeface(null, Typeface.NORMAL)
                unlockTextView.setTypeface(null, Typeface.NORMAL)
                downloadTextView.setTypeface(null, Typeface.BOLD)
                activity?.toolbarDone?.visibility = View.GONE
//                activity?.toolbarTrash?.visibility = View.VISIBLE
                if ((activity as LibraryActivity).tabIndex == 0) {
                    activity?.toolbarTrash?.visibility = View.VISIBLE
                } else {
                    activity?.toolbarTrash?.visibility = View.GONE
                }
                emptyView.visibility = View.GONE
                filterView.visibility = View.GONE
                nestedScrollView.visibility = View.GONE
                (activity as LibraryActivity).currentCategory = 2
                networkStateView.visibility = View.VISIBLE
                categoryView.setBackgroundColor(Color.parseColor("#f7f8fa"))

                libNetworkErrorView?.visibility = View.GONE

                if (CommonUtil.getNetworkInfo(requireContext()) != null) {
                    requestExpireEpisode()
                } else {
                    getDownloadedData()
                }
            }
        }
    }

    private fun resetCategory() {
        allTextView.isSelected = false
        unlockTextView.isSelected = false
        downloadTextView.isSelected = false

        viewModel.items.clear()
        viewModel.mSeriesList.clear()
    }

    override fun initRecyclerViewAdapter() {
        val layoutId = if ((activity as LibraryActivity).currentCategory == 2) R.layout.item_content_download
            else recyclerViewItemLayoutId
        recyclerView.adapter =
            CommonRecyclerViewAdapter(
                viewModel.items,
                layoutId
            )
        (recyclerView.adapter as RecyclerViewBaseAdapter).apply {
            setOnItemClickListener(object : RecyclerViewBaseAdapter.OnItemClickListener {
                override fun onItemClick(item: Any?, position: Int) {
                    if (item is DataBook) {
                        var intent: Intent
                        if ((activity as LibraryActivity).currentCategory == 2) {     // download
                            intent = Intent(context, DownloadEpActivity::class.java)
                            val bundle = Bundle().apply {
                                putString("path", item.filePath)
                                putString("thumbnail", KJKomicsApp.DOWNLOAD_ROOT_PATH + CommonUtil.convertUno(CommonUtil.read(context, CODE.LOCAL_RID, ""))
                                        + "/thumbnail/" + item.sid + "/")
                                putString("sid", item.sid)
                                putString("title", item.title)
                            }
                            intent.putExtras(bundle)
                        } else {
                            intent = Intent(context, SeriesActivity::class.java).apply {
                                putExtra("sid", item.sid)
                                putExtra("title", item.title)
                            }
                        }
                        startActivity(intent)
                    }
                }
            })

            setOnDelteItemClickListener(object : RecyclerViewBaseAdapter.OnDeleteItemClickListener {
                override fun onItemClick(item: Any, position: Int) {
                    if (viewModel.items.size == 0) {
                        return
                    }
                    if (CommonUtil.getNetworkInfo(requireContext()) == null) {
                        CommonUtil.showToast(getString(R.string.msg_disable_remove_file), context)
                        return
                    }
                    if (item is DataBook) {
                        // remove request
//                        if (item.isChecked) {
//                            item.isChecked = false;
//                            viewModel.mSeriesList.remove(item.sid);
//                        } else {
//                            item.isChecked = true;
//                            viewModel.mSeriesList.add(item.sid);
//                        }
                        item.isChecked = true;
                        viewModel.mSeriesList.add(item.sid);
                        when ((activity as LibraryActivity).currentCategory) {
                            0 -> {
                                requestDeleteLibrary()
                            }
                            2 -> {
                                removeFile(position)
                            }
                        }
                    }
                }
            })

            setOnSubscribeClickListener(object : RecyclerViewBaseAdapter.OnSubscribeClickListener {
                override fun onItemClick(item: Any, position: Int, selected: Boolean) {
                    if (item is DataBook) {
                        val action = if (selected) {
                            "S"
                        } else {
                            "C"
                        }
                        requestSubscribe(item.sid ?: "", action)
                    }
                }
            })
        }
    }

    /**
     * 파일 삭제
     */
    private fun removeFile(position: Int) {
        val item = viewModel.items[position] as DataBook
        FileUtils.deleteDir(item.filePath)
        viewModel.items.removeAt(position)
        if (viewModel.items.size > 0) {
//            getDownloadedData()
        } else {
            showEmptyView(null)
            viewModel.mFile.delete()
        }

        recyclerView.adapter?.notifyDataSetChanged()

        requestDeleteFile()
        removeThumbnailFile()
    }

    private fun removeThumbnailFile() {
        var seriesPath = KJKomicsApp.DOWNLOAD_ROOT_PATH +
                CommonUtil.convertUno(CommonUtil.read(context, CODE.LOCAL_RID, "")) +
                "/thumbnail/"
        viewModel.mSeriesList.forEach {
            seriesPath = seriesPath + it
            FileUtils.deleteDir(seriesPath)
        }
    }

    override fun showEmptyView(msg: String?) {
        super.showEmptyView(msg)
        emptyView?.apply {
            when((activity as LibraryActivity).currentCategory) {
                0, 1 -> {
//                    categoryView?.setBackgroundColor(Color.WHITE)
                    emptyView.visibility = View.VISIBLE
                    emptyDownloadView.visibility = View.GONE
                    errorTitle.text = msg
                    errorMsg.visibility = View.GONE
                    goSeriesButton.text = getString(R.string.str_go_to_series)
                    goSeriesButton.visibility = View.GONE
                }
                2 -> {
//                    categoryView?.setBackgroundColor(Color.parseColor("#f7f8fa"))
                    emptyDownloadView.visibility = View.VISIBLE
                    emptyView.visibility = View.GONE
                    errorTitle.text = getString(R.string.msg_empty_download)
                    errorMsg.text = getString(R.string.msg_empty_download2)
                    errorMsg.visibility = View.VISIBLE
//                    goSeriesButton.text = getString(R.string.str_go_to_download)
//                    goSeriesButton.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun requestExpireEpisode() {
        val api = ServerUtil.service.getUserApi(
            "expire_episode")
        api.enqueue(object : retrofit2.Callback<User> {
            override fun onResponse(call: Call<User>, response: Response<User>) {
                try {
                    if (response.isSuccessful) {
                        val item = response.body()
                        if ("00" == item!!.retcode) {
                            viewModel.mEpExpireDateList = item.expire_episode
                            getDownloadedData()
                        } else {
                            if ("" != item.msg) {
                                CommonUtil.showToast(item.msg, context)
                            }
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<User>, t: Throwable) {
                try {
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getDownloadedData() {
        try {
            viewModel.apply {
                mPath = KJKomicsApp.DOWNLOAD_ROOT_PATH +
                        CommonUtil.convertUno(CommonUtil.read(context, CODE.LOCAL_RID, "")!!) + "/" +
                        CommonUtil.read(context, CODE.CURRENT_LANGUAGE, "en")
                mFile = File(mPath)
                if (mFile.length() == 0L) {
                    showEmptyView(null)
                    return
                }
                mFile.listFiles().forEach { it ->
                    if (!"thumbnail".equals(it.name)) {
                        val item = DataBook()
                        val sid = it.name.split("_")[0]
                        val title = it.name.split("_")[1]
                        val genre = it.name.split("_")[2]
                        val writer = it.name.split("_")[3]

                        item.filePath = it.absolutePath

                        mFileName = CommonUtil.convertDownloadTitle(context, it.name)

                        item.sid = sid
                        item.title = CommonUtil.convertDownloadTitle(context, title)
                        item.genre1 = genre.split(" ")[0]
                        item.genre2 = genre.split(" ")[1]
                        item.genre3 = genre.split(" ")[2]
                        item.writer1 = CommonUtil.convertDownloadTitle(context, writer)

                        item.epCount = it.listFiles()?.size ?: 0
                        if (mEpExpireDateList?.isEmpty() == true) {
                            // check expire date
                            it.listFiles().forEach {
                                // 2019-12-19 19:50:15
                                if (TextUtils.isEmpty(it.name)) return@forEach
                                val expireDateString = it.name.split("_")[3]

                                val formatter = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
                                val expireDate = formatter.parse(expireDateString)

                                // Calendar 객체 생성
                                val cal = Calendar.getInstance()
                                val todayMil = cal.timeInMillis     // 현재 시간(밀리 세컨드)

                                // 파일의 마지막 수정시간 가져오기
//                    val fileDate = Date(it.lastModified())
                                val fileCal = Calendar.getInstance()
                                // 현재시간과 파일 수정시간 시간차 계산(단위 : 밀리 세컨드)
                                fileCal.setTime(expireDate)
                                val diffMil = todayMil - fileCal.getTimeInMillis()

                                //날짜로 계산
                                val diffDay = (diffMil / ONE_DAY_MIL).toInt()
                                if (diffDay > 0 && it.exists()) {
                                    // 파일 삭제
                                    FileUtils.deleteDir(it.absolutePath)
                                }
                            }
                        } else {
                            it.listFiles().forEachIndexed { index, file ->
                                val expireEid = it.name.split("_")[0]
                                mEpExpireDateList?.forEach {
                                    if (it == expireEid) {
                                        // 파일 삭제
                                        FileUtils.deleteDir(file.absolutePath)
                                    }
                                }
                            }
                            requestEpDeleteFile(item.sid!!)
                        }

                        val thumbNailPath = KJKomicsApp.DOWNLOAD_ROOT_PATH +
                                CommonUtil.convertUno(
                                    CommonUtil.read(
                                        context,
                                        CODE.LOCAL_RID,
                                        ""
                                    )!!
                                ) + "/thumbnail/" + item.sid + "/"
                        val thumbFile = File(thumbNailPath)
                        thumbFile.listFiles()?.forEach {
                            if (it.name.equals(item.sid + ".png")) {
                                item.image = it.absolutePath
                            }
                        }

                        items.add(item)
                    }
                }

                if (items.size > 0) {
                    showMainView()
                    (activity as LibraryActivity).toolbar.toolbarTrash.visibility = View.VISIBLE
                } else {
                    showEmptyView(null)
                    (activity as LibraryActivity).toolbar.toolbarTrash.visibility = View.GONE
                }
//                recyclerView.adapter?.notifyDataSetChanged()
                initRecyclerViewAdapter()
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    override fun showMainView() {
//        categoryView?.setBackgroundColor(Color.WHITE)
        mainView?.visibility = View.VISIBLE
        nestedScrollView?.visibility = View.VISIBLE
        recyclerView.visibility = View.VISIBLE
        emptyView?.visibility = View.GONE
        emptyDownloadView?.visibility = View.GONE
    }
}