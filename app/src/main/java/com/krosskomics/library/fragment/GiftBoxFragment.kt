package com.krosskomics.library.fragment

import android.view.View
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.krosskomics.KJKomicsApp
import com.krosskomics.R
import com.krosskomics.common.adapter.RecyclerViewBaseAdapter
import com.krosskomics.common.data.DataGift
import com.krosskomics.common.fragment.RecyclerViewBaseFragment
import com.krosskomics.common.model.Gift
import com.krosskomics.library.activity.LibraryActivity
import com.krosskomics.library.viewmodel.GiftBoxViewModel
import com.krosskomics.util.CODE
import com.krosskomics.util.CommonUtil
import com.krosskomics.util.CommonUtil.read
import com.krosskomics.util.CommonUtil.showToast
import com.krosskomics.util.CommonUtil.write
import com.krosskomics.util.ServerUtil
import kotlinx.android.synthetic.main.fragment_genre.*
import kotlinx.android.synthetic.main.view_network_error_gift.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GiftBoxFragment : RecyclerViewBaseFragment() {

    override val viewModel: GiftBoxViewModel by lazy {
        ViewModelProvider(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return GiftBoxViewModel(requireContext()) as T
            }
        }).get(GiftBoxViewModel::class.java)
    }

    override fun getLayoutId(): Int {
        recyclerViewItemLayoutId = R.layout.item_gift
        return R.layout.fragment_giftbox
    }

    override fun requestServer() {
        if (CommonUtil.getNetworkInfo(requireContext()) == null) {
            giftErrorView?.visibility = View.VISIBLE
            return
        }
        viewModel.requestMain()
    }

    override fun initRecyclerViewAdapter() {
        super.initRecyclerViewAdapter()
        (recyclerView.adapter as RecyclerViewBaseAdapter).apply {
            setOnGiftGetClickListener(object : RecyclerViewBaseAdapter.OnGiftGetClickListener {
                override fun onItemClick(item: Any, position: Int) {
                    if (item is DataGift) {
                        requestGetGift(item.seq)
                    }
                }
            })
        }
        KJKomicsApp.IS_GET_NEW_GIFT = false
    }

    /**
     * 선물 받기
     */
    private fun requestGetGift(seq: String?) {
        if (isAdded) {
            val getGift = ServerUtil.service.getGiftApi(
                read(context, CODE.CURRENT_LANGUAGE, "en"),
                "get_gift", seq
            )
            getGift.enqueue(object : Callback<Gift> {
                override fun onResponse(
                    call: Call<Gift>,
                    response: Response<Gift>
                ) {
                    try {
                        if ("00" == response.body()?.retcode) {
                            val coin = response.body()?.cash
                            if ("" != coin && coin != null) {
                                write(context, CODE.LOCAL_coin, coin)
                            }
//                            showRecieveGiftDialog()
                            viewModel.isRefresh = true
                            requestServer()
                            (activity as LibraryActivity).requestGiftApi(requireContext(), 1)
//                            requestHistory(true)
                        } else {
                            if ("" != response.body()?.msg) {
                                showToast(response.body()?.msg, context)
                            }
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

                override fun onFailure(call: Call<Gift>, t: Throwable) {
                    try {
                        t.printStackTrace()
//                        checkNetworkConnection(context, t, actBinding.viewError)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            })
        }
    }
}