package com.krosskomics.library.activity

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.transition.Fade
import android.view.View
import android.view.Window
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.tabs.TabLayoutMediator
import com.krosskomics.R
import com.krosskomics.common.activity.ToolbarViewPagerActivity
import com.krosskomics.common.model.Gift
import com.krosskomics.util.CODE
import com.krosskomics.util.CommonUtil
import com.krosskomics.util.ServerUtil
import kotlinx.android.synthetic.main.activity_genre_detail.*
import kotlinx.android.synthetic.main.view_toolbar_black.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LibraryActivity : ToolbarViewPagerActivity() {
    private val TAG = "LibraryActivity"

    override var tabIndex = 0
    var currentCategory = 0 // all, unlock, download
    var badgeDrawable: BadgeDrawable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            with(window) {
                requestFeature(Window.FEATURE_CONTENT_TRANSITIONS)
                // set an slide transition
                enterTransition = Fade()
                exitTransition = Fade()
            }
        }
        super.onCreate(savedInstanceState)
    }

    override fun requestServer() {
        requestGiftApi(context, 1)
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_library
    }

    override fun initTracker() {
        setTracker(getString(R.string.str_library))
    }

    override fun initLayout() {
        toolbarTitleString = getString(R.string.str_library)
        adapterType = 1
        super.initLayout()

        viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback(){
            override fun onPageSelected(position: Int) {
                when (position) {
                    0 -> {
                        tabIndex = 0
                        when (currentCategory) {
                            0 -> {
                                toolbarDone?.visibility = View.GONE
                                toolbarTrash?.visibility = View.VISIBLE
                            }
                            1 -> {
                                toolbarDone?.visibility = View.GONE
                                toolbarTrash?.visibility = View.GONE
                            }
                            2 -> {
                                toolbarDone?.visibility = View.GONE
                                toolbarTrash?.visibility = View.VISIBLE
                            }
                        }
                    }
                    1 -> {
                        tabIndex = 1
                        toolbarDone?.visibility = View.GONE
                        toolbarTrash?.visibility = View.GONE
                    }
                }
            }
        })
    }

    override fun initModel() {
        tabIndex = intent?.getIntExtra("tabIndex", 0) ?: 0
        currentCategory = intent?.getIntExtra("currentCategory", 0) ?: 0
        super.initModel()
    }

    override fun initToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowTitleEnabled(false)
            setHomeAsUpIndicator(R.drawable.kk_icon_back_white)
        }
        toolbarTitle.text = toolbarTitleString
        if (tabIndex == 0) {
            toolbarDone?.visibility = View.GONE
            toolbarTrash?.visibility = View.VISIBLE
        } else if (tabIndex == 1) {
            toolbarDone?.visibility = View.GONE
            toolbarTrash?.visibility = View.GONE
        }
    }

    fun isVisibleToolbarDone(): Boolean {
        return toolbarDone.isShown
    }

    override fun initTabItems() {
        tabItems = listOf(getString(R.string.str_my_comic), getString(R.string.str_gift_box))
    }

    fun setTabLayoutMediator(badgeNumber: Int = 0) {
        if (badgeNumber == 0) {
            badgeDrawable?.isVisible = false
            return
        }
        val tabConfigurationStrategy =
            TabLayoutMediator.TabConfigurationStrategy { tab, position ->
                tab.text = tabItems[position]
                when (position) {
                    1 -> {
                        badgeDrawable = tab.orCreateBadge
                        badgeDrawable?.backgroundColor = Color.RED
                        badgeDrawable?.isVisible = true
                        badgeDrawable?.number = badgeNumber
                    }
                }
            }
        TabLayoutMediator(tabLayout, viewPager, tabConfigurationStrategy).attach()
    }

    fun requestGiftApi(context: Context, page: Int) {
        val api: Call<Gift> = ServerUtil.service.getGiftBox(
            CommonUtil.read(context, CODE.CURRENT_LANGUAGE, "en"),
            "gift", page
        )
        api.enqueue(object : Callback<Gift> {
            override fun onResponse(call: Call<Gift>, response: Response<Gift>) {
                response.body()?.let {
                    it.list?.let { items ->
                        var unReadSize = 0
                        items.forEach { item ->
                            if (item.isget == "0") unReadSize++
                        }
                        setTabLayoutMediator(unReadSize)
                    }
                }
            }

            override fun onFailure(call: Call<Gift>, t: Throwable) {
            }
        })
    }
}