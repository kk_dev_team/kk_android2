package com.krosskomics.genre.fragment

import com.krosskomics.KJKomicsApp
import com.krosskomics.R
import com.krosskomics.common.fragment.RecyclerViewBaseFragment

class GenreDetailFragment() : RecyclerViewBaseFragment() {
    override fun getLayoutId(): Int {
        recyclerViewItemLayoutId = R.layout.item_content
        return R.layout.fragment_genre_detail
    }

    override fun initModel() {
        arguments?.let {
            viewModel.listType = it.getString("listType").toString()
            val position = it.getInt("position")
            viewModel.param2 = KJKomicsApp.INIT_SET.genre?.get(position)?.p_genre
        }
        super.initModel()
    }
}