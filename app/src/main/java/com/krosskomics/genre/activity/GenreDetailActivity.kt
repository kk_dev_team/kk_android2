package com.krosskomics.genre.activity

import android.graphics.Typeface
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.krosskomics.KJKomicsApp
import com.krosskomics.R
import com.krosskomics.common.activity.ToolbarViewPagerActivity
import com.krosskomics.common.adapter.CommonPagerAdapter
import kotlinx.android.synthetic.main.activity_genre_detail.*

class GenreDetailActivity : ToolbarViewPagerActivity() {
    private val TAG = "GenreDetailActivity"

    var prevSelectPosition = 0

    override var tabIndex: Int
        get() = super.tabIndex
        set(value) {}

    override fun getLayoutId(): Int {
        return R.layout.activity_genre_detail
    }

    override fun initTracker() {
        setTracker(getString(R.string.str_genre))
    }

    override fun initLayout() {
        toolbarTitleString = getString(R.string.str_genre)
        adapterType = 0
        super.initLayout()
    }

    override fun initTabItems() {}

    override fun initModel() {
        intent?.extras?.apply {
            viewModel.listType = getString("listType").toString()
            viewModel.param2 = getString("more_param").toString()
        }
        super.initModel()
    }

    override fun initViewPager() {
        KJKomicsApp.INIT_SET.genre?.let {
            viewPager.adapter = CommonPagerAdapter(
                this,
                it.size,
                adapterType
            )
            TabLayoutMediator(tabLayout, viewPager){ tab, position->
                tab.text = it[position].dp_genre
                tab.tag = it[position].p_genre
            }.attach()
        }

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                val tabView = ((tabLayout.getChildAt(0)) as ViewGroup).getChildAt(tab?.position ?: 0) as LinearLayout
                val tabTextView = tabView.getChildAt(1) as TextView
                tabTextView.setTypeface(null, Typeface.BOLD)

                prevSelectPosition = tab?.position ?: 0
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                val tabView = ((tabLayout.getChildAt(0)) as ViewGroup).getChildAt(prevSelectPosition) as LinearLayout
                val tabTextView = tabView.getChildAt(1) as TextView
                tabTextView.setTypeface(null, Typeface.NORMAL)
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }
        })

        val tabView = ((tabLayout.getChildAt(0)) as ViewGroup).getChildAt(0) as LinearLayout
        val tabTextView = tabView.getChildAt(1) as TextView
        tabTextView.setTypeface(null, Typeface.BOLD)
    }
}