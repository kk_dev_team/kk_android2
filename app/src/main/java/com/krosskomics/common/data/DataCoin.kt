package com.krosskomics.common.data

class DataCoin {
    //        "product_id": "kk_coin_30",
//        "product_name": "3300",
//        "coin": 3000,
//        "bonus_coin": 300,
//        "product_text": "Popular",
//        "image": "https://cdn.krosskomics.com/i/asset/cash_3.png",
//        "price": "₹ 90"
    var product_id: String = ""
    var product_name: String? = null
    var coin = "0"
    var bonus_coin = "0"
    var price = "0"
//    var currency: String? = null
    var product_text: String? = null
    var image: String? = null
    var original_price: String? = null  //₹ 90
    var discount = 0
    var limit_product: String? = null
}