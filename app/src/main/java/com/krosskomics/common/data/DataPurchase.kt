package com.krosskomics.common.data

class DataPurchase {
//    u_token(헤더로보내는 유저키), orderid, productid, purchasetime, purchasestate, purchasetoken
    var u_token: String? = ""
    var orderid: String? = null
    var productid: String? = null
    var purchasetime: Long? = 0L
    var purchasestate: Int? = 0
    var purchasetoken: String? = null
}