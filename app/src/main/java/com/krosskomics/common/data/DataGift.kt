package com.krosskomics.common.data

class DataGift {
    var sid: String? = null
    var title: String? = null
    var image: String? = null
    var writer1: String? = null
    var gift_type: String? = null   //R : 지정작품 대여권 해당 작품으로 이동 C: 캐시, 비지정작품 대여권 클릭시 아무동작 안함.
    var seq: String? = null
    var dp_dday: String? = null
    var dp_date: String? = null
    var dp_gift: String? = null
    var isget: String? = null   //0:안받음, 1:받음
}