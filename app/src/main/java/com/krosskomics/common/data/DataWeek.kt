package com.krosskomics.common.data

data class DataWeek(
    var title: String? = null,
    var param: String? = null,
    var isSelect: Boolean = false
)