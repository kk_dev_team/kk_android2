package com.krosskomics.common.data

import java.util.*

class DataEpisode {
    var eid: String = "0"
    var ep_seq = "0"
    var ep_title: String = ""
    var ep_show_date: String = ""
    var isupdate: String? = null
    var ep_store_price = 0
    var ep_rent_price = 0
    var image: String? = null
    var isunlocked: String? = null  //unlock 여부 (구매시 0 인것만 구매가능)
    var str_type // B: black, O: orange
            : String? = null
    var show_str: String? = null
    var ep_view_type: String? = null
    var ep_rating: String? = null
    var ep_rent_term: String? = null
    var isdownload: String? = "0"
    var isChecked = false
    var isCheckVisible = false
    var possibility_allbuy = false
    var pre_ep_unlock: String? = null  // "pre_ep_unlock": "1",			이전 unlocked 0:잠김, 1:풀림 (2021.02.24)
    var next_ep_unlock: String? = null  // "next_ep_unlock": "1",			다음 unlocked 0:잠김, 1:풀림 (2021.02.24)

    // episode check
    var sid: String? = null
    var allow_store: String? = null
    var allow_rent: String? = null
    var except_ep_seq = "0"
    var sticket = 0
    var rticket = 0
    var able_store: String? = null
    var able_rent: String? = null
    var rent_text: String? = null
    var store_text: String? = null
    var coin: String? = null
    var download_progress = 0
    var download_max = 0
    var wop_use: String? = null

    // check ep
    var isexcept_ep = "0" //"0",				기다무 / 티켓 사용 제외 여부(0:제외아님(사용가능), 1:제외(사용불가))
    var dp_except_ep: String? = null //"Latest 10 episodes do not apply for Wait or Pay.",				기다무 / 티켓 사용불가시 표시
    var iswop = "0" //"1",				기다무 여부 (0: 기다무 아님, 1:기다무)
    var reset_wop: String? = null //"11hours",				기다무 리셋 표시
    var reset_wop_ratio = 0 //99,				기다무 리셋 남은기간 백분율
    var user_cash = "0" //0,				cash
    var user_bonus_cash = "0" //"0"				bonus cash
    var dp_ep_txt: String? = null
    var dp_type: String? = null     //E: dp_ep_txt 만 노출, W: dp_ep_txt, reset_wop, reset_wop_ratio 노출, N: 영역삭제 (2021.03.01)
    var able_wop: String? = null     //able_wop == '1'

//    "isexcept_ep": "0",				기다무 / 티켓 사용 제외 여부(0:제외아님(사용가능), 1:제외(사용불가))
//    "dp_except_ep": "Latest 10 episodes do not apply for Wait or Pay.",				기다무 / 티켓 사용불가시 표시
//    "iswop": "1",				기다무 여부 (0: 기다무 아님, 1:기다무)
//    "reset_wop": "11hours",				기다무 리셋 표시
//    "reset_wop_ratio": 99,				기다무 리셋 남은기간 백분율
//    "user_cash": 0,				cash
//    "user_bonus_cash": "0"				bonus cash

    // viewer
    var ep_contents_domain: String? = null
    var ep_contents_path: String? = null
    var ep_contents: String = ""
    var vviewer: String? = null
    var hviewer: String? = null
    var allow_comment: String? = null
    var title: String? = null
    var pre_eid: String? = null
    var next_eid: String? = null
    var liked: String? = null
    var comment_url: String? = null
    var read_ep_img_index: String? = null
    var share_url: String? = null
    var fb_share_url: String? = null
    var share_image: String? = null
    var ep_view_id: String? = null
    var download_expire: String = ""
    var isEpSelect = false

    var isread: String? = null  //    "isread": "0",										에피소드 뷰 여부 (0:안봄, 1:봄)
    var ep_type: String? = null  //    "F",										에피소드 구분 (F: 무료, W:기다리면 무료, C:유료)
    var dp_list_txt: String? = null  //   "December 01, 2019",										리스트 형일때 표시
    var dp_tile_txt: String? = null  //   "12.01.2019"										타일 형일때 표시


    companion object {
        var seq = Comparator<DataEpisode> { s1, s2 ->
            val rollno1 = s1.ep_seq.toInt()
            val rollno2 = s2.ep_seq.toInt()
            rollno1 - rollno2
        }
    }
}