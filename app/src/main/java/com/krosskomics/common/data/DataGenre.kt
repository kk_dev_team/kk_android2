package com.krosskomics.common.data

import java.util.*

class DataGenre {
//    "dp_genre": "Romance",			표시 장르
//    "p_genre": "romance",			파라메터 장르
    var dp_genre: String? = null
    var p_genre: String? = null
    var genre_list: ArrayList<DataBook>? = null

    var image: String? = null
    var isSelect = false
}