package com.krosskomics.common.data

class DataSeriesLanguage {
//    "description": "Description",
//    "close": "Close",
//    "rental_ticket": "RENTAL TICKET",
//    "ticket": "ticket",
//    "continue": "First Episode"

//    check ep
//    "save": "Save",
//    "total": "Total",
//    "cancel": "Cancel",
//    "buy": "Buy",
//    "charge": "Charge"
    var description: String? = null
    var close: String? = null
    var rental_ticket: String? = null
    var ticket: String? = null
    var continue_txt: String? = null

    var save: String? = null
    var total: String? = null
    var cancel: String? = null
    var buy: String? = null
    var charge: String? = null
    var nubc: String? = null    //소장시 보너스캐시 사용 불가능일때 표시할 텍스트.
}