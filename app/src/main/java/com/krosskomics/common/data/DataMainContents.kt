package com.krosskomics.common.data

import java.util.*

class DataMainContents {
//    "layout_title": "Top featured",		제목
//    "layout_desc": "",		설명
//    "layout_type": "lp",		레이아웃 타입 lp:큰포스터, sp:작은포스터, st:6개세트, re:직사각형이벤트, se:정사각형이벤트
//    "show_more": "1",
//    "more_param": 1,
//    "list": [		리스트
    var layout_title: String? = null
    var layout_desc: String? = null
    var layout_type: String? = null
    var show_more: String? = null
    var more_param: String? = null
    var more_type: String? = null   //L : 기존 리스트 화면이동, W:기다무 메뉴오픈, O:연재 메뉴 오픈, R:랭킹 메뉴 오픈, G:장르 메뉴 오픈, E: 이벤트 화면 이동.
    var title: String? = null
    var list: ArrayList<DataBook>? = null
}