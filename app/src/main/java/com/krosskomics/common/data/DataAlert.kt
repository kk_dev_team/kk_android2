package com.krosskomics.data

//"lang": "en",
//"msg": "Data charge may occur in 3G/LTE settings."
data class DataAlert(val lang: String, val msg: String)