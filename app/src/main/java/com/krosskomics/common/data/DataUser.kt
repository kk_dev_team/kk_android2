package com.krosskomics.common.data

class DataUser {
    // output : login_type이 kk인경우 login_type, email, nick, lang, rid, login_seq, profile_picture, ng(선물 0:선물없음, 1:새선물), st(알림 0:알림없음, 1:새알림), cash
    // output : login_type이 kk가 아닌경우 login_type, email, sns_profile(account 화면사용), nick, lang, rid, login_seq, profile_picture, ng(선물 0:선물없음, 1:새선물), st(알림 0:알림없음, 1:새알림), cash
    var u_token: String? = null
    var login_type: String? = null
    var nick: String? = null
    var email: String? = null
    var cash: String? = null
    var lang: String? = null
    var rid: String? = null
    var sns_profile: String? = null
    var profile_picture: String? = null
    var login_seq: Long = 0
    var ng: String? = "0"  // ng(선물 0:선물없음, 1:새선물)
    var st: String? = "0"  // (알림 0:알림없음, 1:새알림)
    var sn: String? = ""  // "Please set your Nickname"
}