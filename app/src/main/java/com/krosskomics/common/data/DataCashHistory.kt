package com.krosskomics.common.data

class DataCashHistory {
//    "cashtype": "B",
//    "cash": "200 CASH",
//    "dp_txt": "Expired",
//    "reg_date": "01.19.2021"
    var cashtype: String? = null
    var cash: String? = null
    var dp_txt: String? = null
    var reg_date: String? = null

    var title: String? = null

    // ticket
    var ticket: String? = null
    var episode: String? = null
    var rent_type: String? = null   //T: ticket 색상 #b9d511, W:기다무 색상 #6236ff
}