package com.krosskomics.common.activity

import android.content.Intent
import android.util.Log
import android.view.View
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.analytics.GoogleAnalytics
import com.google.android.gms.analytics.HitBuilders
import com.krosskomics.KJKomicsApp
import com.krosskomics.R
import com.krosskomics.coin.activity.CoinActivity
import com.krosskomics.coin.activity.TicketHistoryActivity
import com.krosskomics.coin.viewmodel.CoinViewModel
import com.krosskomics.comment.activity.CommentActivity
import com.krosskomics.comment.activity.CommentReportActivity
import com.krosskomics.comment.viewmodel.CommentViewModel
import com.krosskomics.common.adapter.CommonRecyclerViewAdapter
import com.krosskomics.common.adapter.RecyclerViewBaseAdapter
import com.krosskomics.common.data.*
import com.krosskomics.common.model.*
import com.krosskomics.common.viewmodel.BaseViewModel
import com.krosskomics.library.activity.LibraryActivity
import com.krosskomics.more.activity.MoreActivity
import com.krosskomics.mynews.activity.MyNewsActivity
import com.krosskomics.series.activity.SeriesActivity
import com.krosskomics.util.CODE
import com.krosskomics.util.CommonUtil
import com.krosskomics.util.CommonUtil.getNetworkInfo
import com.krosskomics.util.CommonUtil.read
import com.krosskomics.util.CommonUtil.setAppsFlyerEvent
import com.krosskomics.util.CommonUtil.showToast
import com.krosskomics.util.ServerUtil
import com.krosskomics.viewer.activity.ViewerActivity
import kotlinx.android.synthetic.main.activity_comment.*
import kotlinx.android.synthetic.main.activity_event.*
import kotlinx.android.synthetic.main.activity_main_content.nestedScrollView
import kotlinx.android.synthetic.main.activity_main_content.recyclerView
import kotlinx.android.synthetic.main.activity_more.*
import kotlinx.android.synthetic.main.activity_series.*
import kotlinx.android.synthetic.main.activity_settings.*
import kotlinx.android.synthetic.main.fragment_genre.*
import kotlinx.android.synthetic.main.view_empty_common.*
import kotlinx.android.synthetic.main.view_main_tab.*
import kotlinx.android.synthetic.main.view_network_error.*
import kotlinx.android.synthetic.main.view_network_error.view.*
import kotlinx.android.synthetic.main.view_toolbar.*
import kotlinx.android.synthetic.main.view_toolbar.toolbar
import kotlinx.android.synthetic.main.view_toolbar.view.*
import kotlinx.android.synthetic.main.view_topbutton.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

open class RecyclerViewBaseActivity : BaseActivity(), Observer<Any> {
    private val TAG = "RecyclerViewBaseActivity"

    protected open val viewModel: BaseViewModel by lazy {
        ViewModelProvider(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return BaseViewModel(application) as T
            }
        }).get(BaseViewModel::class.java)
    }

    override fun onStart() {
        super.onStart()
        GoogleAnalytics.getInstance(this).reportActivityStart(this)
    }

    override fun onStop() {
        super.onStop()
        GoogleAnalytics.getInstance(this).reportActivityStop(this)
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_ongoing
    }

    override fun initModel() {
        viewModel.getMainResponseLiveData().observe(this, this)
    }

    override fun initLayout() {
        initToolbar()
        initTabView()
        initMainView()
        initFooterView()
    }

    private fun initFooterView() {
        moveTopView?.setOnClickListener {
            if (nestedScrollView != null) {
                nestedScrollView.scrollTo(0, 0)
            } else {
                recyclerView?.layoutManager?.scrollToPosition(0)
            }
        }
    }

    override fun initErrorView() {
        errorView?.refreshButton?.setOnClickListener {
            if (getNetworkInfo(context) == null) {
                return@setOnClickListener
            }
            errorView?.visibility = View.GONE
            requestServer()
        }

        errorView?.goDownloadEpButton?.setOnClickListener {
            val intent = Intent(context, LibraryActivity::class.java)
            intent.putExtra("currentCategory", 2)
            startActivity(intent)
            finish()
        }
    }

    override fun requestServer() {
        if (getNetworkInfo(context) == null) {
            errorView?.visibility = View.VISIBLE
            return
        }
        if (viewModel.isShowProgress) showProgress(context)
        viewModel.requestMain()
    }

    override fun initTracker() {
        // Get tracker.
        val tracker = (application as KJKomicsApp).getTracker(KJKomicsApp.TrackerName.APP_TRACKER)
        tracker?.setScreenName(getString(R.string.str_home))
        tracker?.send(HitBuilders.ScreenViewBuilder().build())
    }

    override fun onChanged(t: Any?) {
        if (t == null) {
            hideProgress()
            return
        }
        when (t) {
            is More -> {
                if ("00" == t.retcode) {
                    setMainContentView(t)
                } else {
                    t.msg?.let {
                        CommonUtil.showToast(it, context)
                    }
                }
                viewModel.isFirstEnter = false
            }
            is Episode -> {
                if ("00" == t.retcode) {
                    setMainContentView(t)
                } else {
                    t.msg?.let {
                        CommonUtil.showToast(it, context)
                    }
                }
            }
            is Coin -> {
                if ("00" == t.retcode) {
                    setMainContentView(t)
                } else {
                    t.msg?.let {
                        CommonUtil.showToast(it, context)
                    }
                }
            }
            is News -> {
                if ("00" == t.retcode) {
                    setMainContentView(t)
                } else {
                    t.msg?.let {
                        CommonUtil.showToast(it, context)
                    }
                }
            }
            is Comment -> {
                if ("00" == t.retcode) {
                    setMainContentView(t)
                } else {
                    t.msg?.let {
                        CommonUtil.showToast(it, context)
                    }
                }
            }
            is Genre -> {
                if ("00" == t.retcode) {
                    setMainContentView(t)
                } else {
                    t.msg?.let {
                        CommonUtil.showToast(it, context)
                    }
                }
            }
            is CashHistory -> {
                if ("00" == t.retcode) {
                    setMainContentView(t)
                } else {
                    t.msg?.let {
                        CommonUtil.showToast(it, context)
                    }
                }
            }
            is Event -> {
                if ("00" == t.retcode) {
                    setMainContentView(t)
                } else {
                    t.msg?.let {
                        CommonUtil.showToast(it, context)
                    }
                }
            }
        }
        hideProgress()
    }

    open fun setMainContentView(body: Any) {
        if (viewModel.isRefresh) {
            viewModel.items.clear()
        }
        when (body) {
            is More -> {
                if (body.list.isNullOrEmpty()) {
                    showEmptyView(body.msg)
                    return
                }
                viewModel.totalPage = body.tot_pages
                body.list?.let {
                    viewModel.items.addAll(it)
                    recyclerView?.adapter?.notifyDataSetChanged()
                }

                totalCountTextView?.text =
                    getString(R.string.str_total) + " ${body.tot_count}"

                toolbarTitleString = body.list_title ?: ""
                toolbar.toolbarTitle.text = toolbarTitleString
            }
            is Episode -> {
                body.list?.list?.let {
                    viewModel.items.addAll(it)
                    recyclerView?.adapter?.notifyDataSetChanged()
                }
            }
            is EpisodeMore -> {
//                viewModel.totalPage = body.tot_pages
                body.list.let {
                    viewModel.items.addAll(it)
                    recyclerView?.adapter?.notifyDataSetChanged()
                }
            }
            is Coin -> {
                (context as CoinActivity).setCashView(body.cash ?: 0,
                    body.bonus_cash ?: 0,
                    body.cash_info)
                if (viewModel is CoinViewModel) {
                    (viewModel as CoinViewModel).cancelMsg = body.cancel
                    (viewModel as CoinViewModel).failMsg = body.fail
                }
                body.product_list?.let {
                    viewModel.items.addAll(it)
                    recyclerView?.adapter?.notifyDataSetChanged()
                }
            }
            is News -> {
                if (body.list.isNullOrEmpty()) {
                    showEmptyView(body.msg)
                    return
                }
                viewModel.totalPage = body.tot_pages
                body.list?.let {
                    viewModel.items.addAll(it)
                    recyclerView?.adapter?.notifyDataSetChanged()
                    val stringBuilder = StringBuilder()
                    viewModel.items.forEach { newsItem ->
                        if (newsItem is DataNews) {
                            if (newsItem.isview == "0") {
                                // 안읽은것만 읽음 처리로 전달

//                              AF|10004,AF|10002,AF|10006,UW|1 이런 string (|와 , 구분된)으로 넘겨줘. 데이터 확인하고 서버 수정할테니까.
                                stringBuilder
                                    .append(newsItem.ntype)
                                    .append("|")
                                    .append(newsItem.nid)
                                    .append(",")
                            }
                        }
                    }
                    if (stringBuilder.isEmpty()) return@let
                    val paramString = stringBuilder.substring(0, stringBuilder.length - 1)
                    if (paramString.isNotEmpty()) {
                        (context as MyNewsActivity).setReadParams(paramString)
                    }
                }
            }
            is Comment -> {
                if ((viewModel as CommentViewModel).type == "like" || (viewModel as CommentViewModel).type == "unlike") {
                    if (viewModel.items.isNullOrEmpty()) return
                    val selectedPosition = (viewModel as CommentViewModel).selectPosition
                    (viewModel.items[selectedPosition] as DataComment).like_cnt = body.like_cnt
                    (viewModel.items[selectedPosition] as DataComment).islike = if (body.state == "L") "1" else "0"
                    recyclerView?.adapter?.notifyDataSetChanged()
                    return
                } else if ((viewModel as CommentViewModel).type == "report") {
                    showToast(body.msg, context)
                    (context as CommentReportActivity).finish()
                    return
                } else if ((viewModel as CommentViewModel).type == "reg") {
                    (context as CommentActivity).setRegSortText()
                }
                if (viewModel is CommentViewModel) {
                    if (viewModel.isReload) {
                        if (context is CommentActivity) {
                            (context as CommentActivity).reloadRequestComment()
                            viewModel.isReload = false
                            return
                        }
                    }
                }
//                if (body.list.isNullOrEmpty()) {
////                    showToast(body.msg, context)
//                    showEmptyView(body.msg)
////                    recyclerView?.adapter?.notifyDataSetChanged()
////                    return
//                }
                viewModel.totalPage = body.tot_pages
                body.list?.let {
                    viewModel.items.addAll(it)
                    recyclerView?.adapter?.notifyDataSetChanged()
                }
                (context as CommentActivity).initHeaderView(body.tot_count)
            }
            is Genre -> {
                if (body.list.isNullOrEmpty()) {
                    showEmptyView(body.msg)
                    return
                }
                body.list?.let {
                    viewModel.items.addAll(it)
                    recyclerView?.adapter?.notifyDataSetChanged()
                }
            }
            is CashHistory -> {
                if (body.list.isNullOrEmpty()) {
                    showEmptyView(body.msg)
                    return
                }
                viewModel.totalPage = body.tot_pages
                body.list?.let {
                    viewModel.items.addAll(it)
                    recyclerView?.adapter?.notifyDataSetChanged()
                }
            }
            is Event -> {
                if (body.list.isNullOrEmpty()) {
                    showEmptyView(body.msg)
                    return
                }
                viewModel.totalPage = body.tot_pages
                body.list?.let {
                    viewModel.items.addAll(it)
                    recyclerView?.adapter?.notifyDataSetChanged()
                }
            }
        }
    }

    open fun initMainView() {
        recyclerView?.let { initRecyclerView() }
        nestedScrollView?.let { initNestedScrollView() }
        topButton?.setOnClickListener {
            if (nestedScrollView != null) {
                nestedScrollView.scrollTo(0, 0)
            } else {
                recyclerView?.layoutManager?.scrollToPosition(0)
            }
        }
    }

    private fun initNestedScrollView() {
        nestedScrollView?.setOnScrollChangeListener { v: NestedScrollView, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int ->
            if (scrollY > CODE.VISIBLE_NESTEDSCROLL_TOPBUTTON_Y) {
                topButton?.visibility = View.VISIBLE
            } else {
                topButton?.visibility = View.GONE
            }
            if (context is SeriesActivity) return@setOnScrollChangeListener
            if (v.getChildAt(0).bottom <= (v.height + scrollY)) {
                Log.e(TAG, "End of NestedScrollView")
                if (viewModel.page < viewModel.totalPage) {
                    Log.e(TAG, "다음페이지 로딩")
                    viewModel.isRefresh = false
                    viewModel.page++
                    requestServer()
                }
            }
        }
    }

    open fun initRecyclerView() {
        recyclerView?.layoutManager = LinearLayoutManager(context)
        recyclerView?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (getCurrentItem(recyclerView) > CODE.VISIBLE_LIST_TOPBUTTON_CNT) {
                    topButton?.visibility = View.VISIBLE
                } else {
                    topButton?.visibility = View.GONE
                }
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                if (context is ViewerActivity) return
                if (context is MoreActivity) return
                if (context is TicketHistoryActivity) return
                if (context is SeriesActivity) return
                if (context is MyNewsActivity) return
                if ((recyclerView.tag)?.toString() == "series") return
                if (!recyclerView.canScrollVertically(1)) {
                    if (viewModel.page < viewModel.totalPage) {
                        viewModel.isRefresh = false
                        viewModel.page++
                        requestServer()
                    }
                }
            }
        })
        initRecyclerViewAdapter()
    }

    open fun initRecyclerViewAdapter() {
        recyclerView?.adapter =
            CommonRecyclerViewAdapter(
                    viewModel.items,
                    recyclerViewItemLayoutId
                )

        if (viewModel.tabIndex != 4) {
            (recyclerView?.adapter as RecyclerViewBaseAdapter).setOnItemClickListener(object :
                RecyclerViewBaseAdapter.OnItemClickListener {
                override fun onItemClick(item: Any?, position: Int) {
                    when (item) {
                        is DataBook -> {
                            val intent = Intent(context, SeriesActivity::class.java).apply {
                                putExtra("sid", item.sid)
                                putExtra("title", item.title)
                            }
                            startActivity(intent)
                        }
                        is DataReport -> {
                            (context as CommentReportActivity).resetReportSelect(position)
                        }
                        is DataNews -> {
                            when (item.atype) {
                                "M" -> finish()
                                "H" -> {
                                    val intent = Intent(context, SeriesActivity::class.java).apply {
                                        putExtra("sid", item.sid)
                                        putExtra("title", item.title)
                                    }
                                    startActivity(intent)
                                }
                                "G" -> {
                                    if (read(context, CODE.LOCAL_loginYn, "N").equals("Y", ignoreCase = true)) {
                                        val intent = Intent(context, LibraryActivity::class.java)
                                        intent.putExtra("tabIndex", 1)
                                        startActivity(intent)
                                    } else {
                                        goLoginAlert(context)
                                    }
                                }
                            }
                        }
                        is DataEvent -> {
                            CommonUtil.setEventAction(context, item)
                        }
                    }
                }
            })

            (recyclerView?.adapter as RecyclerViewBaseAdapter).setOnSubscribeClickListener(object :
                RecyclerViewBaseAdapter.OnSubscribeClickListener {
                override fun onItemClick(item: Any, position: Int, selected: Boolean) {
                    if (read(context, CODE.LOCAL_loginYn, "N").equals("N", ignoreCase = true)) {
                        goLoginAlert(context)
                        return
                    }
                    if (item is DataBook) {
                        var action = if (selected) {
                            "S"
                        } else {
                            "C"
                        }
                        requestSubscribe(item.title, item.sid ?: "", action)
                    }
                }
            })

            (recyclerView?.adapter as RecyclerViewBaseAdapter).setOnCommentReportClickListener(
                object : RecyclerViewBaseAdapter.OnCommentReportClickListener {
                    override fun onItemClick(item: Any, position: Int) {
                        if (read(context, CODE.LOCAL_loginYn, "N").equals("N", ignoreCase = true)) {
                            goLoginAlert(context)
                            return
                        }
                        if (item is DataComment) {
                            val intent = Intent(context, CommentReportActivity::class.java).apply {
                                if (viewModel is CommentViewModel) {
                                    putExtra("sid", (viewModel as CommentViewModel).sid)
                                    putExtra("seq", item.seq)
                                }
                            }
                            startActivity(intent)
                        }
                    }
                })

            (recyclerView?.adapter as RecyclerViewBaseAdapter).setOnDelteItemClickListener(
                object : RecyclerViewBaseAdapter.OnDeleteItemClickListener {
                    override fun onItemClick(item: Any, position: Int) {
                        if (read(context, CODE.LOCAL_loginYn, "N").equals("N", ignoreCase = true)) {
                            goLoginAlert(context)
                            return
                        }
                        if (item is DataComment) {
                            // 댓글 삭제 요청
                            if (viewModel is CommentViewModel) {
                                (viewModel as CommentViewModel).type = "del"
                                (viewModel as CommentViewModel).seq = item.seq ?: "0"
                                (viewModel as CommentViewModel).p = item.seq ?: "0"
                                viewModel.isReload = true
                                requestServer()
                            }
                        }
                    }
                })

            (recyclerView?.adapter as RecyclerViewBaseAdapter).setOnLikeClickListener (
                object : RecyclerViewBaseAdapter.OnLikeClickListener {
                    override fun onItemClick(item: Any, position: Int) {
                        if (item is DataComment) {
                            if (read(context, CODE.LOCAL_loginYn, "N").equals("N", ignoreCase = true)) {
                                goLoginAlert(context)
                                return
                            }
                            // 좋아요 요청
                            if (viewModel is CommentViewModel) {
                                if (item.islike == "0") {
                                    (viewModel as CommentViewModel).type = "like"
                                } else {
                                    (viewModel as CommentViewModel).type = "unlike"
                                }
                                (viewModel as CommentViewModel).seq = item.seq ?: "0"
                                (viewModel as CommentViewModel).p = item.seq ?: "0"
                                (viewModel as CommentViewModel).selectPosition = position
                                (viewModel as CommentViewModel).isShowProgress = false
                                (viewModel as CommentViewModel).isRefresh = false
                                requestServer()
                            }
                        }
                    }
                })
        }
    }

    private fun initTabView() {
        resetTabState()
        when (viewModel.tabIndex) {
            1 -> onGoingButton?.isSelected = true
            2 -> waitButton?.isSelected = true
            3 -> rankingButton?.isSelected = true
            4 -> genreButton?.isSelected = true
        }
        homeButton?.setOnClickListener(this)
        onGoingButton?.setOnClickListener(this)
        waitButton?.setOnClickListener(this)
        rankingButton?.setOnClickListener(this)
        genreButton?.setOnClickListener(this)
    }

    private fun resetTabState() {
        onGoingButton?.isSelected = false
        waitButton?.isSelected = false
        rankingButton?.isSelected = false
        genreButton?.isSelected = false
    }

    private fun requestSubscribe(title: String?, sid: String, action: String) {
        val api = ServerUtil.service.setUserProfileApi(
            read(context, CODE.CURRENT_LANGUAGE, "en"),
            "subscribe", p = action,  sid = sid
        )
        api.enqueue(object : Callback<Default?> {
            override fun onResponse(
                call: Call<Default?>,
                response: Response<Default?>
            ) {
                try {
                    if (response.isSuccessful) {
                        val item = response.body()
                        if ("00" == item!!.retcode) {
                            if ("S" == action) {
                                val eventValue: MutableMap<String, Any?> =
                                    HashMap()
                                eventValue["af_content"] = title + " (" + read(
                                    context,
                                    CODE.CURRENT_LANGUAGE,
                                    "en"
                                ) + ")"
                                eventValue["af_content_id"] = sid
                                setAppsFlyerEvent(context, "af_subscribe", eventValue)
                            }
                        } else if ("202" == item.retcode) {
                            goCoinAlert(context)
                        } else {
                            if ("" != item.msg) {
                                showToast(item.msg, context)
                            }
                        }
                        hideProgress()
                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                    hideProgress()
                }
            }

            override fun onFailure(call: Call<Default?>, t: Throwable) {
                hideProgress()
                try {
//                    checkNetworkConnection(context, t, errorView)
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    /**
     * 푸시수신 설정
     */
    protected fun requestPushNoti(action: String) {
        val api: Call<Default> = ServerUtil.service.setUserProfileApi(
            read(context, CODE.CURRENT_LANGUAGE, "en"),
            "notify_push", p = action, advid = KJKomicsApp.AD_Id
        )
        api.enqueue(object : Callback<Default> {
            override fun onResponse(
                call: Call<Default>,
                response: Response<Default>
            ) {
                try {
                    if (response.isSuccessful) {
                        val item: Default? = response.body()
                        if ("00" == item?.retcode) {
                            if ("C" == action) {
//                                actBinding.btnSwPush.setSelected(false)
                                CommonUtil.write(context, CODE.LOCAL_RECIEVE_PUSH, "0")
                            } else if ("203" == item.retcode) {
                                goLoginAlert(context)
                            } else {
//                                actBinding.btnSwPush.setSelected(true)
                                CommonUtil.write(context, CODE.LOCAL_RECIEVE_PUSH, "1")
                            }
                        } else {
                            if ("" != item?.msg) {
                                showToast(item?.msg, context)
                            }
                        }
                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<Default?>, t: Throwable) {
                try {
                    checkNetworkConnection(context, t, errorView)
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
        })
    }
}