package com.krosskomics.common.adapter

import android.graphics.Color
import android.graphics.Paint
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.krosskomics.R
import com.krosskomics.comment.activity.CommentActivity
import com.krosskomics.common.data.*
import com.krosskomics.common.holder.BaseItemViewHolder
import com.krosskomics.mainmenu.activity.MainMenuActivity
import com.krosskomics.series.activity.SeriesActivity
import com.krosskomics.util.CODE
import com.krosskomics.util.CommonUtil
import com.krosskomics.util.CommonUtil.read
import com.krosskomics.util.CommonUtil.setGlideImage
import com.krosskomics.util.CommonUtil.setGlideImageRound
import com.krosskomics.viewer.activity.ViewerActivity
import kotlinx.android.synthetic.main.item_cash_history.view.*
import kotlinx.android.synthetic.main.item_coin.view.*
import kotlinx.android.synthetic.main.item_comment.view.*
import kotlinx.android.synthetic.main.item_comment_report.view.*
import kotlinx.android.synthetic.main.item_content.view.*
import kotlinx.android.synthetic.main.item_download_ep.view.*
import kotlinx.android.synthetic.main.item_event.view.*
import kotlinx.android.synthetic.main.item_faq.view.*
import kotlinx.android.synthetic.main.item_gift.view.*
import kotlinx.android.synthetic.main.item_gift.view.ovalImageView
import kotlinx.android.synthetic.main.item_more.view.*
import kotlinx.android.synthetic.main.item_more.view.deleteView
import kotlinx.android.synthetic.main.item_more.view.descTextView
import kotlinx.android.synthetic.main.item_more.view.epDownloadCntTextView
import kotlinx.android.synthetic.main.item_more.view.subscribeImageView
import kotlinx.android.synthetic.main.item_mynews.view.*
import kotlinx.android.synthetic.main.item_ongoing.view.genreTextView
import kotlinx.android.synthetic.main.item_ongoing.view.mainImageView
import kotlinx.android.synthetic.main.item_ongoing.view.titleTextView
import kotlinx.android.synthetic.main.item_ongoing.view.writerTextView
import kotlinx.android.synthetic.main.item_ranking.view.*
import kotlinx.android.synthetic.main.item_ranking_a.view.*
import kotlinx.android.synthetic.main.item_search_recent.view.*
import kotlinx.android.synthetic.main.item_series.view.*
import kotlinx.android.synthetic.main.item_series.view.img_ep_title
import kotlinx.android.synthetic.main.item_series.view.txt_ep_title
import kotlinx.android.synthetic.main.item_series_grid.view.*
import kotlinx.android.synthetic.main.item_ticket_history.view.*
import kotlinx.android.synthetic.main.item_underline.view.*
import kotlinx.android.synthetic.main.item_view_episode.view.*
import kotlinx.android.synthetic.main.item_vline.view.*
import kotlinx.android.synthetic.main.item_waitfree.view.*
import kotlinx.android.synthetic.main.view_content_like.view.*
import kotlinx.android.synthetic.main.view_content_tag_right.view.*
import kotlinx.android.synthetic.main.view_dim.view.*
import kotlinx.android.synthetic.main.view_ep_free_tag.view.*
import kotlinx.android.synthetic.main.view_ep_lock_tag.view.*
import kotlinx.android.synthetic.main.view_ep_purchase_select.view.*
import kotlinx.android.synthetic.main.view_new_up_tag.view.*
import kotlinx.android.synthetic.main.view_remain_tag.view.*
import kotlinx.android.synthetic.main.view_series_not_purchase.view.*
import kotlinx.android.synthetic.main.view_series_wop_tag.view.*
import kotlinx.android.synthetic.main.view_ticket.view.*
import kotlinx.android.synthetic.main.view_update_series.view.*

open class RecyclerViewBaseAdapter(private val items: ArrayList<*>, private val layoutRes: Int) :
    RecyclerView.Adapter<RecyclerViewBaseAdapter.BaseItemHolder>() {

    private var onClickListener: OnItemClickListener? = null
    private var onDeleteClickListener: OnDeleteItemClickListener? = null
    private var onDownloadClickListener: OnDownloadClickListener? = null
    private var onDownloadCancelClickListener: OnDownloadCancelClickListener? = null
    private var onSubscribeClickListener: OnSubscribeClickListener? = null
    private var onCommentReportClickListener: OnCommentReportClickListener? = null
    private var onLikeClickListener: OnLikeClickListener? = null
    private var onGiftGetClickListener: OnGiftGetClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseItemHolder {
        val view = LayoutInflater.from(parent.context).inflate(layoutRes, parent, false)
        return BaseItemHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: BaseItemHolder, position: Int) {
        holder.setData(items[position], position)
    }

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener) {
        this.onClickListener = onItemClickListener
    }

    fun setOnDelteItemClickListener(onItemClickListener: OnDeleteItemClickListener) {
        this.onDeleteClickListener = onItemClickListener
    }

    fun setOnDownloadClickListener(onItemClickListener: OnDownloadClickListener) {
        this.onDownloadClickListener = onItemClickListener
    }

    fun setOnDownloadCancelClickListener(onItemClickListener: OnDownloadCancelClickListener) {
        this.onDownloadCancelClickListener = onItemClickListener
    }

    fun setOnSubscribeClickListener(onItemClickListener: OnSubscribeClickListener) {
        this.onSubscribeClickListener = onItemClickListener
    }

    fun setOnCommentReportClickListener(onItemClickListener: OnCommentReportClickListener) {
        this.onCommentReportClickListener = onItemClickListener
    }

    fun setOnLikeClickListener(onItemClickListener: OnLikeClickListener) {
        this.onLikeClickListener = onItemClickListener
    }

    fun setOnGiftGetClickListener(onItemClickListener: OnGiftGetClickListener) {
        this.onGiftGetClickListener = onItemClickListener
    }

    inner class BaseItemHolder(itemView: View) : BaseItemViewHolder(itemView) {
        override fun setData(item: Any?, position: Int) {
            try {
                itemView.apply {
//                animation = AnimationUtils.loadAnimation(context, R.anim.list_item_slide_down)
                    setOnClickListener {
                        onClickListener?.onItemClick(item, position)
                    }
                    when (item) {
                        is DataBook -> {
                            mainImageView?.let {
                                if (it.tag == null) {
                                    setGlideImage(itemView.context, item.image ?: "", it)
                                } else {
                                    val roundCorner = it.tag.toString()
                                    setGlideImageRound(itemView.context, item.image ?: "", it, roundCorner.toInt())
                                }
                            }

                            titleTextView?.text = item.title
                            writerTextView?.text = item.writer1
                            if (item.genre1.isNullOrEmpty()) {
                                genreDotTextView?.visibility = View.GONE
                            } else {
                                genreDotTextView?.visibility = View.VISIBLE
                            }
                            genreTextView?.text = item.genre1
                            descTextView?.text = item.dp_txt

                            if (item.like_cnt.isNullOrEmpty()) {
                                contentLikeView?.visibility = View.GONE
                            } else {
                                contentLikeView?.visibility = View.VISIBLE
                                likeCountTextView?.text = item.like_cnt
                                tv_like_count?.text = item.like_cnt
                            }
                            if (item.isnew == "1") {
                                newTagImageView?.visibility = View.VISIBLE
                                upTagImageView?.visibility = View.GONE
                            } else {
                                newTagImageView?.visibility = View.GONE
                                if (item.isupdate == "1") {
                                    upTagImageView?.visibility = View.VISIBLE
                                } else {
                                    upTagImageView?.visibility = View.GONE
                                }
                            }
                            if (item.epCount == 0) {
                                epDownloadCntTextView?.visibility = View.GONE
                            } else {
                                epDownloadCntTextView?.text = itemView.context.getString(R.string.str_episodes_seq_format_small1, item.epCount)
                                epDownloadCntTextView?.visibility = View.VISIBLE
                            }
                            // 기다무
                            if (item.iswop == "1") {
                                remainTagView?.visibility = View.VISIBLE
                                remainTagView?.isSelected = false
                                remainTagTextView?.text = item.dp_wop_term
                            } else {
                                if (item.dp_pub_day.isNullOrEmpty()) {
                                    remainTagView?.visibility = View.GONE
                                } else {
                                    remainTagView?.isSelected = true
                                    remainTagTextView?.text = item.dp_pub_day
                                    remainTagView?.visibility = View.VISIBLE
                                }
                            }
                            // ranking
                            if (context is MainMenuActivity) {
                                when (position) {
                                    0 -> rankingImageView?.setImageResource(R.drawable.kk_ranking_1)
                                    1 -> rankingImageView?.setImageResource(R.drawable.kk_ranking_2)
                                    2 -> {
                                        rankingImageView?.setImageResource(R.drawable.kk_ranking_3)
                                        defaultUnderLineView?.visibility = View.GONE
                                        dummyView?.visibility = View.VISIBLE
                                    }
                                }
                                rankingTextView?.text = (position + 1).toString()
                            }
                            // genre
                            if (item.issub.isNullOrEmpty()) {
                                subscribeImageView?.visibility = View.GONE
                            } else {
                                subscribeImageView?.visibility = View.VISIBLE
                                subscribeImageView?.isSelected = item.issub != "0"
                                subscribeImageView?.setOnClickListener {
                                    if (read(context, CODE.LOCAL_loginYn, "N").equals("N")) {
                                        CommonUtil.goLoginAlert(context)
                                        return@setOnClickListener
                                    }
                                    subscribeImageView.isSelected = !it.isSelected
                                    onSubscribeClickListener?.onItemClick(
                                        item,
                                        position,
                                        subscribeImageView.isSelected
                                    )
                                }
                            }

                            // library
                            if (item.isCheckVisible) {
                                deleteView?.visibility = View.VISIBLE
                                deleteView?.isEnabled = item.isChecked
                                deleteView.setOnClickListener {
                                    onDeleteClickListener?.onItemClick(item, position)
                                }
                            } else {
                                deleteView?.visibility = View.GONE
                            }
                        }
                        is DataEpisode -> {
                            img_ep_title?.controller = CommonUtil.getDraweeController(
                                context, item.image,
                                200, 200
                            )
                            txt_ep_title?.text = item.ep_title
                            if (itemView.context is SeriesActivity) {
                                val viewModel = (itemView.context as SeriesActivity).viewModel
                                if (item.isunlocked == "1") {
                                    if (item.ep_type == "F") {
                                        //free 아이콘 표시. 기다무 아이콘 표시 안함, 자물쇠 아이콘 표시 안함
                                        freeTagImageView.visibility = View.VISIBLE
                                        wopImageView.visibility = View.GONE
                                        lockImageView?.visibility = View.GONE
                                    } else {
                                        //free,자물쇠,기다무 아이콘 표시 안함
                                        freeTagImageView.visibility = View.GONE
                                        wopImageView.visibility = View.GONE
                                        lockImageView?.visibility = View.GONE
                                    }
                                    if (viewModel.listViewType == 1) {  // list type
                                        if (read(context, CODE.LOCAL_loginYn, "N").equals(
                                                "Y",
                                                ignoreCase = true
                                            )
                                        ) {
                                            Log.e("jj", "item.ep_title : " + item.ep_title)
                                            Log.e("jj", "item.isdownload : " + item.isdownload)
                                            downloadImageView?.isSelected = "0" != item.isdownload
                                            downloadImageView?.visibility = View.VISIBLE
                                            downloadImageView?.setOnClickListener(View.OnClickListener {
                                                onDownloadClickListener?.onItemClick(item, position)
                                            })
                                            if (item.download_progress > 0) {
                                                downloadImageView?.visibility = View.GONE
//                                        circleView.max = item.download_max
                                                circleView.visibility = View.VISIBLE
                                                circleView.progress = item.download_progress
                                                circleView.setOnClickListener(View.OnClickListener {
                                                    onDownloadCancelClickListener?.onItemClick(
                                                        item,
                                                        position
                                                    )
                                                })
                                            } else {
                                                circleView.visibility = View.GONE
                                                downloadImageView?.visibility = View.VISIBLE
                                            }
                                        }
                                    }

                                    if (viewModel.itemViewMode == 0) {
                                        notPurchaseView?.visibility = View.GONE
                                    } else {
                                        notPurchaseView?.visibility = View.VISIBLE
                                    }
                                } else {
                                    downloadImageView?.visibility = View.GONE
                                    circleView?.visibility = View.GONE
                                    if (viewModel.seriesItem.iswop == "1") {
                                        if (item.ep_type == "C") {
                                            //free,기다무,자물쇠 아이콘 표시 안함
                                            freeTagImageView.visibility = View.GONE
                                            wopImageView.visibility = View.GONE
                                            lockImageView?.visibility = View.GONE
                                        } else if (item.ep_type == "W") {
                                            //기다무 아이콘 표시. free,자물쇠 아이콘 표시 안함
                                            freeTagImageView.visibility = View.GONE
                                            wopImageView.visibility = View.VISIBLE
                                            lockImageView?.visibility = View.GONE
                                        } else if (item.ep_type == "F") {
                                            //free 아이콘 표시. 기다무,자물쇠 아이콘 표시 안함
                                            freeTagImageView.visibility = View.VISIBLE
                                            wopImageView.visibility = View.GONE
                                            lockImageView?.visibility = View.GONE
                                        }
                                    } else {
                                        if (item.ep_type == "C") {
                                            //자물쇠 아이콘 표시. free, 기다무 아이콘 표시 안함.
                                            freeTagImageView.visibility = View.GONE
                                            wopImageView.visibility = View.GONE
                                            lockImageView?.visibility = View.VISIBLE
                                        } else if (item.ep_type == "F") {
                                            //free 아이콘 표시. 기다무,자물쇠 아이콘 표시 안함
                                            freeTagImageView.visibility = View.VISIBLE
                                            wopImageView.visibility = View.GONE
                                            lockImageView?.visibility = View.GONE
                                        } else {
                                            freeTagImageView.visibility = View.GONE
                                            wopImageView.visibility = View.GONE
                                            lockImageView?.visibility = View.GONE
                                        }
                                        // 티켓
//                                    if (itemView.context is SeriesActivity) {
//                                        if (viewModel.selectEpItem.rticket > 0) {
//                                            if (item.ep_seq < viewModel.selectEpItem.except_ep_seq) {
//                                                ticketImageView?.visibility = View.VISIBLE
//                                                ticketImageView?.isSelected = false
//                                            } else {
//                                                ticketImageView?.visibility = View.GONE
//                                            }
//                                        } else {
//                                            if (viewModel.selectEpItem.sticket > 0) {
//                                                if (item.ep_seq.toInt() < viewModel.selectEpItem.except_ep_seq.toInt()) {
//                                                    ticketImageView?.visibility = View.VISIBLE
//                                                    ticketImageView?.isSelected = true
//                                                } else {
//                                                    ticketImageView?.visibility = View.GONE
//                                                }
//                                            } else {
//                                                ticketImageView?.visibility = View.GONE
//                                            }
//                                        }
//                                    }
                                    }
                                    // 구매
                                    if (item.isCheckVisible) {
//                                dimView?.visibility = View.GONE
                                        selectView?.visibility = View.VISIBLE
                                        if (item.isChecked) {
                                            checkImageView?.visibility = View.VISIBLE
                                        } else {
                                            checkImageView?.visibility = View.GONE
                                        }
                                    } else {
//                                dimView?.visibility = View.VISIBLE
                                        selectView?.visibility = View.GONE
                                    }
                                    notPurchaseView?.visibility = View.GONE
                                }
                                if (item.isupdate == "1") {
                                    updateImageView.visibility = View.VISIBLE
                                } else {
                                    updateImageView.visibility = View.GONE
                                }
                                when (viewModel.listViewType) {
                                    0 -> {
                                        txt_update?.text = item.dp_tile_txt
                                        if (item.isread == "0") {
                                            epGridContentView.setBackgroundColor(Color.TRANSPARENT)
                                        } else {
                                            epGridContentView.setBackgroundColor(Color.parseColor("#0c000000"))
                                        }
                                    }
                                    1 -> {
                                        txt_showDate?.text = item.dp_list_txt
                                        if (item.isread == "0") {
                                            epListContentView?.setBackgroundColor(Color.TRANSPARENT)
                                        } else {
                                            epListContentView?.setBackgroundColor(Color.parseColor("#07000000"))
                                        }
                                    }
                                }
                            } else if (itemView.context is ViewerActivity) {
                                txt_ep_title.isSelected = item.isEpSelect
                                epImgContainer.isSelected = item.isEpSelect
                                if (position == itemCount - 1) {
                                    vLineView?.visibility = View.GONE
                                } else {
                                    vLineView?.visibility = View.VISIBLE
                                }
                            }
                        }
                        is DataCoin -> {
                            tv_coin.text = item.coin + " + "

                            if (item.bonus_coin == "0") {
                                tv_bonus_coin.setTextColor(Color.parseColor("#828282"))
                            } else {
                                tv_bonus_coin.setTextColor(Color.parseColor("#e02020"))
                            }
                            tv_bonus_coin.text = item.bonus_coin

                            if (item.discount == 0) {
                                discountView.visibility = View.GONE
                                priceMarginView.visibility = View.VISIBLE
                            } else {
                                tv_origin_price.text = item.original_price
                                tv_origin_price.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
                                tv_discount_rate.text = "${item.discount}" + "%"
                                discountView.visibility = View.VISIBLE
                                priceMarginView.visibility = View.GONE
                            }
                            tv_price.text = item.price

                            tv_product_name.text = item.product_name
                            if (TextUtils.isEmpty(item.product_text)) {
                                tv_product_text.visibility = View.GONE
                            } else {
                                tv_product_text.text = item.product_text
                                tv_product_text.visibility = View.VISIBLE
                                tv_product_text.isSelected = item.limit_product == "1"
                            }
                            coinImageView?.let {
                                Glide.with(context)
                                    .load(item.image)
                                    .into(it)
                            }
                        }
                        is DataBanner -> {
                            img_ep_title?.controller = CommonUtil.getDraweeController(
                                context, item.image,
                                200, 200
                            )
                            txt_ep_title?.text = item.title
                        }
                        is DataFile -> {
                            if (!TextUtils.isEmpty(item.image)) {
                                img_ep_title?.setImageURI("file://" + item.image)
                            }
                            if (item.isCheckVisible) {
                                deleteView?.visibility = View.VISIBLE
                                deleteView?.isEnabled = item.isChecked
                                deleteView.setOnClickListener {
                                    onDeleteClickListener?.onItemClick(item, position)
                                }
                            } else {
                                deleteView?.visibility = View.GONE
                            }
                            txt_ep_title?.text = item.ep_title
//                    tv_show_date?.text = item.ep_show_date
                            tv_expire_date?.text = item.expireDate
                        }
                        is DataGift -> {
                            mainImageView?.let {
                                Glide.with(itemView.context)
                                    .load(item.image)
                                    .circleCrop()
                                    .into(it)
                            }
                            titleTextView.text = item.title
                            giftTextView.text = item.dp_gift
                            dDayTextView.text = item.dp_dday
                            remainTimeTextView.text = item.dp_date
                            ovalImageView.isSelected = item.isget != "0"
                            if (item.isget == "0") {
                                getImageView.visibility = View.VISIBLE
                                getImageView?.setOnClickListener {
                                    onGiftGetClickListener?.onItemClick(item, position)
                                }
                            } else {
                                getImageView.visibility = View.GONE
                            }
                        }
                        is DataNews -> {
                            newsTextView.text = item.title
                            newsDescTextView.text = item.body
                            regDateTextView.text = item.reg_date
                            itemView.isSelected = item.isview != "1"
                            when (item.type) {
                                "notify" -> {
                                    newsIcImageView.setImageResource(R.drawable.kk_selector_news_icon)
                                }
                                "ticket" -> {
                                    newsIcImageView.setImageResource(R.drawable.kk_selector_news_ticket_icon)
                                }
                                "cash" -> {
                                    newsIcImageView.setImageResource(R.drawable.kk_selector_news_cash_icon)
                                }
                                "waitorpay" -> {
                                    newsIcImageView.setImageResource(R.drawable.kk_selector_news_waitfree_icon)
                                }
                                else -> {
                                    newsIcImageView.setImageResource(R.drawable.kk_selector_news_icon)
                                }
                            }
                        }
                        is DataComment -> {
                            nicknameTextView.text = item.nick
                            commentDateTextView?.text = item.dp_reg_date
                            commentTextView?.text = item.comment
                            commnetLikeCountTextView?.text = item.like_cnt
                            if (item.isregister == "0") {
                                deleteTextView.visibility = View.GONE
                            } else {
                                deleteTextView.visibility = View.VISIBLE
                                deleteTextView.setOnClickListener {
                                    // 댓글 삭제 요청
                                    onDeleteClickListener?.onItemClick(item, position)
                                }
                            }
                            reportImageView?.setOnClickListener {
                                onCommentReportClickListener?.onItemClick(item, position)
                            }
                            likeCountView.isSelected = item.islike == "1"
                            likeCountView?.setOnClickListener {
                                onLikeClickListener?.onItemClick(item, position)
                            }
                            if (item.profile_picture.isNullOrEmpty()) {
                                profileImageView.setImageResource(R.drawable.kk_default_profile)
                            } else {
                                Glide.with(itemView.context)
                                    .load(item.profile_picture)
                                    .circleCrop()
                                    .into(profileImageView)
                            }
                            if (item.isbest == "0") {
                                bestTagImageView.visibility = View.GONE
                            } else {
                                bestTagImageView.visibility = View.VISIBLE
                            }
                            if ((itemView.context as CommentActivity).viewModel.eid == "0") {
                                deleteTextView.visibility = View.GONE
                                commentEpSeqTextView.text = context.getString(R.string.str_episode_seq_format, item.ep_seq)
                                commentEpSeqTextView.visibility = View.VISIBLE
                            } else {
                                commentEpSeqTextView.visibility = View.GONE
                            }
                        }
                        is DataReport -> {
                            ReportTextView?.text = item.title
                            isSelected = item.isSelect
                            if (position < itemCount - 1) {
                                underLineView.visibility = View.VISIBLE
                            } else {
                                underLineView.visibility = View.GONE
                            }
                        }
                        is String -> {  // recent tag
                            titleTextView?.text = item
                            deleteView.setOnClickListener {
                                onDeleteClickListener?.onItemClick(item, position)
                            }
                        }
                        is DataCashHistory -> {
                            historyDateTextView?.text = item.reg_date
                            dateTextView?.text = item.reg_date
                            cashTextView?.text = item.cash
                            dpTextView?.text = item.dp_txt
                            if (item.cashtype == "B") {
                                bonusImageView?.visibility = View.VISIBLE
                            } else {
                                bonusImageView?.visibility = View.GONE
                            }
                            if (item.title.isNullOrEmpty()) {
                                titleTextView?.visibility = View.GONE
                            } else {
                                titleTextView?.text = item.title
                                titleTextView?.visibility = View.VISIBLE
                            }
                            // ticket history
                            if (item.ticket.isNullOrEmpty()) {
                                ticketTextView?.visibility = View.GONE
                            } else {
                                when (item.rent_type) {
                                    "T" -> ticketTextView.setTextColor(Color.parseColor("#b9d511"))
                                    "W" -> ticketTextView.setTextColor(Color.parseColor("#6236ff"))
                                }
                                ticketTextView?.text = item.ticket
                                ticketTextView?.visibility = View.VISIBLE
                            }
                            if (item.episode.isNullOrEmpty()) {
                                epTextView?.visibility = View.GONE
                            } else {
                                epTextView?.text = item.episode
                                epTextView?.visibility = View.VISIBLE
                            }
                        }
                        is DataEvent -> {
                            mainImageView?.let {
                                Glide.with(itemView.context)
                                    .load(item.image)
                                    .into(it)
                            }
                            titleTextView.text = item.event_title
                            eventPeriodTextView.text = "${item.start_date} ~ ${item.end_date}"
                        }
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(item: Any?, position: Int)
    }

    interface OnDeleteItemClickListener {
        fun onItemClick(item: Any, position: Int)
    }

    interface OnDownloadClickListener {
        fun onItemClick(item: Any, position: Int)
    }

    interface OnDownloadCancelClickListener {
        fun onItemClick(item: Any, position: Int)
    }

    interface OnSubscribeClickListener {
        fun onItemClick(item: Any, position: Int, selected: Boolean)
    }

    interface OnCommentReportClickListener {
        fun onItemClick(item: Any, position: Int)
    }

    interface OnLikeClickListener {
        fun onItemClick(item: Any, position: Int)
    }

    interface OnGiftGetClickListener {
        fun onItemClick(item: Any, position: Int)
    }
}