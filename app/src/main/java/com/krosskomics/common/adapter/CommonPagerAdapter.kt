package com.krosskomics.common.adapter

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.krosskomics.coin.fragment.CashHistoryFragment
import com.krosskomics.genre.fragment.GenreDetailFragment
import com.krosskomics.library.fragment.GiftBoxFragment
import com.krosskomics.library.fragment.LibraryFragment
import com.krosskomics.mainmenu.fragment.GenreFragment
import com.krosskomics.mainmenu.fragment.OnGoingFragment
import com.krosskomics.mainmenu.fragment.RankingFragment
import com.krosskomics.mainmenu.fragment.WaitFreeFragment
import com.krosskomics.notice.activity.NoticeActivity
import com.krosskomics.util.CODE
import com.krosskomics.webview.WebViewFragment

class CommonPagerAdapter(
    fa: FragmentActivity,
    val size: Int,
    private val adapterType: Int,
    private val nseq: String? = ""
) : FragmentStateAdapter(fa) {
    override fun getItemCount(): Int {
        return size
    }

    override fun createFragment(position: Int): Fragment {
        return when(adapterType) {
            0 -> GenreDetailFragment().apply {
                val bundle = Bundle()
                bundle.putString("listType", "genre")
                bundle.putInt("position", position)
                arguments = bundle
            }
            1 -> {
                if (position == 0) {
                    LibraryFragment()
                } else {
                    GiftBoxFragment()
                }
            }
            2 -> {
                CashHistoryFragment().apply {
                    val bundle = Bundle()
                    if (position == 0) {
                        bundle.putString("listType", "cash_charge")
                    } else {
                        bundle.putString("listType", "cash_use")
                    }
                    arguments = bundle
                }
            }
            3 -> {
                when (position) {
                    0 -> {
                        OnGoingFragment()
                    }
                    1 -> {
                        WaitFreeFragment()
                    }
                    2 -> {
                        GenreFragment()
                    }
                    else -> {
                        RankingFragment()
                    }
                }
            }
            else -> {
                WebViewFragment().apply {
                    val bundle = Bundle()
                    if (position == 0) {
                        var url = CODE.NOTICE_URL
    //                            https://dev-boeisl6039-web.krosskomics.com/webview/html/notice?nseq=nseq값
                        if (nseq?.isNotEmpty() == true) {
                            url += "?nseq=${nseq}"
                        }
                        bundle.putString("url", url)
                    } else {
                        bundle.putString("url", CODE.FAQ_URL)
                    }
                    arguments = bundle
                }
            }
        }
    }
}