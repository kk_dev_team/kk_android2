package com.krosskomics.common.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.krosskomics.R
import com.krosskomics.coin.activity.CoinActivity
import com.krosskomics.common.viewmodel.FragmentBaseViewModel
import kotlinx.android.synthetic.main.fragment_library.*
import kotlinx.android.synthetic.main.fragment_library.loadingView
import kotlinx.android.synthetic.main.fragment_library.recyclerView
import kotlinx.android.synthetic.main.item_coin_first.*
import kotlinx.android.synthetic.main.view_empty_common.emptyView
import kotlinx.android.synthetic.main.view_empty_common.view.*

abstract class BaseFragment : Fragment(), Observer<Any> {
    protected var recyclerViewItemLayoutId = 0

    protected open val viewModel: FragmentBaseViewModel by lazy {
        ViewModelProvider(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return FragmentBaseViewModel(requireContext()) as T
            }
        }).get(FragmentBaseViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(getLayoutId(),container,false)
        val ani = AnimationUtils.loadAnimation(context, R.anim.slide_in_left)
        view.animation = ani
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initModel()
        initLayout()
        requestServer()
        initErrorView()
    }

    abstract fun getLayoutId(): Int
    abstract fun initModel()
    abstract fun initLayout()
    abstract fun requestServer()
    abstract fun initErrorView()

    open fun getCurrentItem(recyclerView: RecyclerView): Int {
        return (recyclerView.layoutManager as LinearLayoutManager)
            .findFirstVisibleItemPosition()
    }

    open fun showMainView() {
        mainView?.visibility = View.VISIBLE
        nestedScrollView?.visibility = View.VISIBLE
        recyclerView?.visibility = View.VISIBLE
        emptyView?.visibility = View.GONE
    }

    open fun showEmptyView(msg: String?) {
        mainView?.visibility = View.GONE
        recyclerView?.visibility = View.GONE
        nestedScrollView?.visibility = View.GONE
        emptyView?.visibility = View.VISIBLE
        emptyView?.errorTitle?.text = msg
    }

    open fun showProgress(context: Context) {
        loadingView?.visibility = View.VISIBLE
    }

    open fun hideProgress() {
        loadingView?.visibility = View.GONE
    }

    open fun goCoinAlert(context: Context?) {
        val intent = Intent(context, CoinActivity::class.java)
        startActivity(intent)
    }
}