package com.krosskomics.common.fragment

import android.content.Intent
import android.util.Log
import android.view.View
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.krosskomics.R
import com.krosskomics.coin.activity.CashHistoryActivity
import com.krosskomics.common.adapter.CommonRecyclerViewAdapter
import com.krosskomics.common.adapter.RecyclerViewBaseAdapter
import com.krosskomics.common.data.*
import com.krosskomics.common.model.*
import com.krosskomics.common.viewmodel.FragmentBaseViewModel
import com.krosskomics.library.activity.LibraryActivity
import com.krosskomics.library.viewmodel.LibraryViewModel
import com.krosskomics.mainmenu.activity.MainMenuActivity
import com.krosskomics.mainmenu.adapter.GenreAdapter
import com.krosskomics.mainmenu.adapter.RankingAdapter
import com.krosskomics.mainmenu.adapter.WaitFreeAdapter
import com.krosskomics.mainmenu.adapter.WaitFreeTermAdapter
import com.krosskomics.mainmenu.viewmodel.MainMenuViewModel
import com.krosskomics.series.activity.SeriesActivity
import com.krosskomics.util.CODE
import com.krosskomics.util.CommonUtil
import com.krosskomics.util.ServerUtil
import kotlinx.android.synthetic.main.activity_genre_detail.*
import kotlinx.android.synthetic.main.activity_library.*
import kotlinx.android.synthetic.main.activity_main_content.*
import kotlinx.android.synthetic.main.fragment_genre.*
import kotlinx.android.synthetic.main.fragment_genre.recyclerView
import kotlinx.android.synthetic.main.fragment_genre_detail.*
import kotlinx.android.synthetic.main.fragment_genre_detail.nestedScrollView
import kotlinx.android.synthetic.main.fragment_library.*
import kotlinx.android.synthetic.main.fragment_waitfree.*
import kotlinx.android.synthetic.main.item_underline_1dp.*
import kotlinx.android.synthetic.main.view_network_error.*
import kotlinx.android.synthetic.main.view_network_error.view.*
import kotlinx.android.synthetic.main.view_topbutton.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

open class RecyclerViewBaseFragment : BaseFragment() {
    override val viewModel: FragmentBaseViewModel by lazy {
        ViewModelProvider(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return FragmentBaseViewModel(requireContext()) as T
            }
        }).get(FragmentBaseViewModel::class.java)
    }

    override fun getLayoutId(): Int {
        recyclerViewItemLayoutId = R.layout.item_more
        return R.layout.activity_more
    }

    override fun initModel() {
        viewModel.getMainResponseLiveData().observe(this, this)
    }

    override fun initLayout() {
        initMainView()
    }

    override fun requestServer() {
        if (isAdded) {
            try {
                if (CommonUtil.getNetworkInfo(requireContext()) == null) {
                    errorView?.visibility = View.VISIBLE
                    return
                }
                showProgress(requireContext())
                viewModel.requestMain()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun onChanged(t: Any?) {
        if (t == null) {
            hideProgress()
            return
        }
        when (t) {
            is More -> {
                if ("00" == t.retcode) {
                    setMainContentView(t)
                } else {
                    t.msg?.let {
                        CommonUtil.showToast(it, context)
                    }
                }
            }
            is Episode -> {
                if ("00" == t.retcode) {
                    setMainContentView(t)
                } else {
                    t.msg?.let {
                        CommonUtil.showToast(it, context)
                    }
                }
            }
            is EpisodeMore -> {
                if ("00" == t.retcode) {
                    setMainContentView(t)
                } else {
                    t.msg?.let {
                        CommonUtil.showToast(it, context)
                    }
                }
            }
            is Coin -> {
                if ("00" == t.retcode) {
                    setMainContentView(t)
                } else {
                    t.msg?.let {
                        CommonUtil.showToast(it, context)
                    }
                }
            }
            is Search -> {
                if ("00" == t.retcode) {
                    setMainContentView(t)
                } else {
                    t.msg?.let {
                        CommonUtil.showToast(it, context)
                    }
                }
            }
            is News -> {
                if ("00" == t.retcode) {
                    setMainContentView(t)
                } else {
                    t.msg?.let {
                        CommonUtil.showToast(it, context)
                    }
                }
            }
            is Comment -> {
                if ("00" == t.retcode) {
                    setMainContentView(t)
                } else {
                    t.msg?.let {
                        CommonUtil.showToast(it, context)
                    }
                }
            }
            is Genre -> {
                if ("00" == t.retcode) {
                    setMainContentView(t)
                } else {
                    t.msg?.let {
                        CommonUtil.showToast(it, context)
                    }
                }
            }
            is CashHistory -> {
                if ("00" == t.retcode) {
                    setMainContentView(t)
                } else {
                    t.msg?.let {
                        CommonUtil.showToast(it, context)
                    }
                }
            }
            is Gift -> {
                if ("00" == t.retcode) {
                    setMainContentView(t)
                } else {
                    t.msg?.let {
                        CommonUtil.showToast(it, context)
                    }
                }
            }
        }
        hideProgress()
    }

    open fun initMainView() {
        recyclerView?.let { initRecyclerView() }
        nestedScrollView?.let { initNestedScrollView() }
        topButton?.setOnClickListener {
            if (nestedScrollView != null) {
                nestedScrollView.scrollTo(0, 0)
            } else {
                recyclerView?.layoutManager?.scrollToPosition(0)
            }
        }
    }

    open fun initNestedScrollView() {
        nestedScrollView?.setOnScrollChangeListener { v: NestedScrollView, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int ->
            if (context is LibraryActivity) {
                if ((context as LibraryActivity).isVisibleToolbarDone()) return@setOnScrollChangeListener
                if ((context as LibraryActivity).currentCategory == 2) return@setOnScrollChangeListener
            }
            if (scrollY == 0) {
                if (context is MainMenuActivity) return@setOnScrollChangeListener
                underLineView?.visibility = View.GONE
            } else {
                underLineView?.visibility = View.VISIBLE
            }
            if (scrollY > CODE.VISIBLE_NESTEDSCROLL_TOPBUTTON_Y) {
                topButton?.visibility = View.VISIBLE
            } else {
                topButton?.visibility = View.GONE
            }
            if (v.getChildAt(0).bottom <= (v.height + scrollY)) {
                Log.e("jj", "End of NestedScrollView")
                if (viewModel.page < viewModel.totalPage) {
                    Log.e("jj", "다음페이지 로딩")
                    viewModel.isRefresh = false
                    viewModel.page++
                    requestServer()
                }
            }
        }
    }

    open fun initRecyclerView() {
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (getCurrentItem(recyclerView) > CODE.VISIBLE_LIST_TOPBUTTON_CNT) {
                    topButton?.visibility = View.VISIBLE
                } else {
                    topButton?.visibility = View.GONE
                }
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                if (!recyclerView.canScrollVertically(1)) {
                    if (activity is MainMenuActivity) return
                    if (context is LibraryActivity) {
                        if ((context as LibraryActivity).isVisibleToolbarDone()) return
                        if ((context as  LibraryActivity).currentCategory == 2) return
                    }
                    Log.e("jj", "바닥")
                    if (viewModel.page < viewModel.totalPage) {
                        viewModel.page++
                        viewModel.isRefresh = false
                        Log.e("jj", "다음페이지 로딩")
                        requestServer()
                    }
                }
            }
        })
        initRecyclerViewAdapter()
    }

    open fun initRecyclerViewAdapter() {
        recyclerView?.adapter =
            when (viewModel.tabIndex) {
                3 -> RankingAdapter(
                    viewModel.items,
                    recyclerViewItemLayoutId
                )
                else -> CommonRecyclerViewAdapter(
                    viewModel.items,
                    recyclerViewItemLayoutId
                )
            }

//        if (viewModel.tabIndex != 4) {
            (recyclerView.adapter as RecyclerViewBaseAdapter).apply {
                setOnItemClickListener(object : RecyclerViewBaseAdapter.OnItemClickListener {
                    override fun onItemClick(item: Any?, position: Int) {
                        when (item) {
                            is DataBook -> {
                                val intent = Intent(context, SeriesActivity::class.java).apply {
                                    putExtra("sid", item.sid)
                                    putExtra("title", item.title)
                                }
                                startActivity(intent)
                            }
                            is DataNotice -> {
                                expandNoticeItem(position)
                            }
                            is DataGift -> {
                                if (item.gift_type == "R" || item.gift_type == "S") {
                                    val intent = Intent(context, SeriesActivity::class.java).apply {
                                        putExtra("sid", item.sid)
                                        putExtra("title", item.title)
                                    }
                                    startActivity(intent)
                                }
                            }
                        }
                    }
                })

                setOnDelteItemClickListener(object : RecyclerViewBaseAdapter.OnDeleteItemClickListener {
                    override fun onItemClick(item: Any, position: Int) {
                        if (item is DataBook) {
                            // remove request
                        }
                    }
                })

                (recyclerView?.adapter as RecyclerViewBaseAdapter).setOnSubscribeClickListener(object : RecyclerViewBaseAdapter.OnSubscribeClickListener {
                    override fun onItemClick(item: Any, position: Int, selected: Boolean) {
                        if (item is DataBook) {
                            var action = if (selected) {
                                "S"
                            } else {
                                "C"
                            }
                            requestSubscribe(item.sid ?: "", action)
                        }
                    }
                })
            }
//        }
    }

    override fun initErrorView() {
        errorView?.refreshButton?.setOnClickListener {
            if (CommonUtil.getNetworkInfo(requireContext()) == null) {
                return@setOnClickListener
            }
            errorView.visibility = View.GONE
            requestServer()
        }

        errorView?.goDownloadEpButton?.setOnClickListener {
            val intent = Intent(context, LibraryActivity::class.java)
            intent.putExtra("currentCategory", 2)
            startActivity(intent)
        }
    }

    private fun expandNoticeItem(position: Int) {
        viewModel.items.forEachIndexed { index, item ->
            if (item is DataNotice) {
                item.isSelect = index == position
            }
        }
        recyclerView.adapter?.notifyDataSetChanged()
    }

    open fun setMainContentView(body: Any) {
        if (viewModel.isRefresh) {
            viewModel.items.clear()
        }
        when (body) {
            is More -> {
                if (body.list.isNullOrEmpty()) {
                    showEmptyView(body.msg)
                    if (viewModel is LibraryViewModel) {
                        (viewModel as LibraryViewModel).isDeletMode = false
                    }

                    return
                }
                viewModel.totalPage = body.tot_pages
                body.list?.let {
                    showMainView()
                    viewModel.items.addAll(it)
                    if (viewModel is LibraryViewModel) {
                        if ((viewModel as LibraryViewModel).isDeletMode) {
                            viewModel.items.forEach { moreItem ->
                                if (moreItem is DataBook) {
                                    moreItem.isCheckVisible = true
                                    moreItem.isChecked = true
                                }
                            }
                        }
                        initRecyclerViewAdapter()
                        (viewModel as LibraryViewModel).isDeletMode = false
                    } else {
                        recyclerView.adapter?.notifyDataSetChanged()
                    }
                    if (viewModel.isRefresh) {
                        if (nestedScrollView != null) {
                            nestedScrollView.scrollTo(0, 0)
                        } else {
                            recyclerView?.layoutManager?.scrollToPosition(0)
                        }
                    }

                    viewModel.isFirstEnter = false
                }
                // 기다무
                if (viewModel.isRefresh) return
                if (viewModel is MainMenuViewModel) {
                    when (viewModel.tabIndex) {
                        1 -> {
                            body.wop_term?.let {
                                if (viewModel is MainMenuViewModel) {
                                    if ((viewModel as MainMenuViewModel).waitFreeTermItems.isNullOrEmpty()) {
                                        (viewModel as MainMenuViewModel).waitFreeTermItems = arrayListOf()
                                        val item = DataWaitFreeTerm()
                                        item.dp_wop_term_text = ""
                                        item.isSelect = true
                                        item.p_wop_term = ""
                                        item.dp_wop_term_num = "All"
                                        (viewModel as MainMenuViewModel).waitFreeTermItems?.add(item)
                                        (viewModel as MainMenuViewModel).waitFreeTermItems?.addAll(it)
                                        initWaitFreeTermRecyclerView()
                                    }
                                }
                            }
                        }
                        2 -> {
                            body.genre?.let {
                                if ((viewModel as MainMenuViewModel).genreItems.isNullOrEmpty()) {
                                    (viewModel as MainMenuViewModel).genreItems = arrayListOf()
                                    it.getOrNull(0)?.isSelect = true
                                    (viewModel as MainMenuViewModel).genreItems?.addAll(it)
                                    initGenreRecyclerView()
                                }
                            }
                        }
                    }
                }
            }
            is Gift -> {
                if (body.list.isNullOrEmpty()) {
                    showEmptyView(body.msg)
                    return
                }
                viewModel.totalPage = body.tot_pages
                body.list?.let {
                    showMainView()
                    viewModel.items.addAll(it)
                    recyclerView?.adapter?.notifyDataSetChanged()
                    var unReadSize = 0
                    it.forEach { item ->
                        if (item.isget == "0") unReadSize++
                    }
                    (activity as LibraryActivity).setTabLayoutMediator(unReadSize)
                }
            }
            is News -> {
                if (body.list.isNullOrEmpty()) {
                    showEmptyView(body.msg)
                    return
                }
                viewModel.totalPage = body.tot_pages
                body.list?.let {
                    viewModel.items.addAll(it)
                    recyclerView?.adapter?.notifyDataSetChanged()
                }
            }
            is Notice -> {
                if (body.list.isNullOrEmpty()) {
                    showEmptyView(body.msg)
                    return
                }
                viewModel.totalPage = body.tot_pages
                body.list?.let {
                    viewModel.items.addAll(it)
                    recyclerView?.adapter?.notifyDataSetChanged()
                }
            }
            is Genre -> {
                body.list?.let {
                    viewModel.items.addAll(it)
                    recyclerView?.adapter?.notifyDataSetChanged()
                }
            }
            is CashHistory -> {
                if (context is CashHistoryActivity) {
                    (context as CashHistoryActivity).setCashView(body.cash, body.bonus_cash)
                }
                if (body.list.isNullOrEmpty()) {
                    showEmptyView(body.msg)
                    return
                }
                viewModel.totalPage = body.tot_pages
                body.list?.let {
                    viewModel.items.addAll(it)
                    recyclerView?.adapter?.notifyDataSetChanged()
                }
            }
        }
        listCountTextView?.text = "${getString(R.string.str_total)} ${viewModel.items.size}"
    }

    fun initWaitFreeTermRecyclerView() {
        remainRecyclerView.apply {
            adapter = WaitFreeTermAdapter(
                (viewModel as MainMenuViewModel).waitFreeTermItems!!
            )
            (adapter as WaitFreeTermAdapter).setOnItemClickListener(object : WaitFreeTermAdapter.OnItemClickListener {
                override fun onItemClick(item: Any?, position: Int) {
                    if (item is DataWaitFreeTerm) {
                        viewModel.isRefresh = true
                        viewModel.page = 1
                        viewModel.param2 = item.p_wop_term
                        (viewModel as MainMenuViewModel).waitFreeTermItems?.forEachIndexed { index, dataWaitFreeTerm ->
                            dataWaitFreeTerm.isSelect = index == position
                        }
                        (adapter as WaitFreeTermAdapter).notifyDataSetChanged()
                        requestServer()
                    }
                }
            })
        }
    }

    fun initGenreRecyclerView() {
        genreRecyclerView.apply {
            adapter = GenreAdapter(
                (viewModel as MainMenuViewModel).genreItems!!
            )
            (adapter as GenreAdapter).setOnItemClickListener(object : GenreAdapter.OnItemClickListener {
                override fun onItemClick(item: Any?, position: Int) {
                    if (item is DataGenre) {
                        viewModel.isRefresh = true
                        viewModel.page = 1
                        viewModel.param2 = item.p_genre
                        (viewModel as MainMenuViewModel).genreItems?.forEachIndexed { index, dataGenre ->
                            dataGenre.isSelect = index == position
                        }
                        (adapter as GenreAdapter).notifyDataSetChanged()
                        requestServer()
                    }
                }
            })
        }
    }

//    override fun showEmptyView(msg: String?) {
//        super.showEmptyView(msg)
//        emptyView?.apply {
//            errorTitle.text = getString(R.string.msg_empty_gift)
//            errorMsg.visibility = View.GONE
//            goSeriesButton.visibility = View.GONE
//        }
//    }

    fun requestSubscribe(sid: String, action: String) {
        val api = ServerUtil.service.setUserProfileApi(
            CommonUtil.read(context, CODE.CURRENT_LANGUAGE, "en"),
            "subscribe", p = action,  sid = sid
        )
        api.enqueue(object : Callback<Default?> {
            override fun onResponse(
                call: Call<Default?>,
                response: Response<Default?>
            ) {
                try {
                    if (response.isSuccessful) {
                        val item = response.body()
                        if ("00" == item!!.retcode) {

                        } else if ("202" == item.retcode) {
                            goCoinAlert(context)
                        } else {
                            if ("" != item.msg) {
                                CommonUtil.showToast(item.msg, context)
                            }
                        }
                        hideProgress()
                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                    hideProgress()
                }
            }

            override fun onFailure(call: Call<Default?>, t: Throwable) {
                hideProgress()
                try {
//                    checkNetworkConnection(context, t, errorView)
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
        })
    }
}