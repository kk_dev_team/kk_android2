package com.krosskomics.common.model

class PurchaseEpisode {
//    "retcode": "00",
//    "msg": "success",
//    "image": "https://cdn.krosskomics.com/c/i/914365/1571040248239.jpg",
//    "title": "She Hates Me",
//    "episode": "Episode 30",
//    "unlock_txt": "0hours Rental",
//    "unlock_date": "2021-02-24 18:15:07",
    var retcode: String? = null
    var msg: String? = null
    var image: String? = null
    var title: String? = null
    var episode: String? = null
    var unlock_txt: String? = null
    var unlock_date: String? = null
    var cash: String? = null
    var view_episode: String? = null
}