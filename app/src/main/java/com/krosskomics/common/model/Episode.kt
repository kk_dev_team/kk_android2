package com.krosskomics.common.model

import com.krosskomics.common.data.*

class Episode : Default() {
    var series: DataSeries? = null

//    var list: ArrayList<DataEpisode> = arrayListOf()
    var list: DataEpList? = null

    var episode: DataEpisode? = null
    var episode_list: DataEpList? = null
    var recomend: DataMainContents? = null
    var wop_use: String? = null
    var lang_txt: DataSeriesLanguage? = null
    var upbc: Int? = null    //소장시 보너스캐시 사용가능 여부 '1' 가능, '0' 불가능
}