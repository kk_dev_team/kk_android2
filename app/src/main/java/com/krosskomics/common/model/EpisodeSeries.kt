package com.krosskomics.common.model

import com.krosskomics.common.data.DataEpList
import com.krosskomics.common.data.DataEpisode
import com.krosskomics.common.data.DataMainContents
import com.krosskomics.common.data.DataSeries

class EpisodeSeries : Default() {
    var series: DataSeries? = null

//    var list: ArrayList<DataEpisode> = arrayListOf()
    var list: DataEpList? = null

    var episode: DataEpisode? = null
    var episode_list: DataEpList? = null
    var recomend: DataMainContents? = null
}