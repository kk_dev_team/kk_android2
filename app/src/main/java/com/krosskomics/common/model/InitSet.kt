package com.krosskomics.common.model

import com.krosskomics.common.data.DataAge
import com.krosskomics.common.data.DataBFL
import com.krosskomics.common.data.DataBanner
import com.krosskomics.common.data.DataLoginGenre
import com.krosskomics.data.DataAlert
import com.krosskomics.data.DataLanguage
import com.krosskomics.data.DataLogout
import java.util.*
import kotlin.collections.ArrayList

class InitSet : Default() {
//    "retcode": "00",
//    "msg": "success",
//    "run_seq": 2026,
//    "ispushnotify": "1",
//    "genre": [
//    "bfl":{
//        "image":"https://cdn.krosskomics.com/i/asset/bflogin.2021010100.png",
//        "text":"Welcome to\nKross Komics!"
//    },
//    "popup":[
//    ]
    var ispushnotify: String? = null
    var run_seq: Long = 0
    var genre: ArrayList<DataLoginGenre>? = null
    var genreSignup = ArrayList<DataLoginGenre>()
    var age: ArrayList<DataAge>? = null
    var popup: ArrayList<DataBanner>? = null
    var lang: MutableList<DataLanguage>? = null
    var bfl: DataBFL? = null
    var app_version: String? = null
    var logout: ArrayList<DataLogout>? = null
    var data_alert: ArrayList<DataAlert>? = null
}