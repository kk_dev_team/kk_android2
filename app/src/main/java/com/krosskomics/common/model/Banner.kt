package com.krosskomics.common.model

import com.krosskomics.common.data.DataBanner
import com.krosskomics.common.data.DataMainBanner

class Banner : Default() {
//    "banner":{
//        "main":{
//            "rolling":4,
//            "list":[]
//        },
//        "middle":{},
//        "floating":{}
//    },
    var main: DataMainBanner? = null
    var middle: DataBanner? = null
    var floating: DataBanner? = null
}