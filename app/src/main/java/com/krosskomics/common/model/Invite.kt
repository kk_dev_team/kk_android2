package com.krosskomics.common.model

class Invite: Default() {
    var invite_link: String? = null
    var back_image: String? = null
    var info: String? = null
}