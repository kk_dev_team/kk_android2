package com.krosskomics.common.model

import com.krosskomics.common.data.DataCoin
import java.util.*

class Coin : Default() {
//    "retcode": "00",
//    "msg": "success",
//    "cash": 13600,
//    "bonus_cash": "0",

//    "product_list": [
//    {
//        "product_id": "kk_coin_4",
//        "product_name": "500",
//        "coin": 500,
//        "bonus_coin": 0,
//        "product_text": "Best Start",
//        "image": "https://cdn.krosskomics.com/i/asset/cash_1.png",
//        "price": "₹ 12"
    var cash: Int? = 0
    var bonus_cash: Int? = 0
    var product_list: ArrayList<DataCoin>? = null
    var cash_info: String? = null
    var cancel: String? = null  // 결제 취소
    var fail: String? = null    // 결제 실패

    // paytm
//    orderid, mid, txnToken, amount, callbackurl
    var orderid: String? = null
    var mid: String? = null
    var txnToken: String? = null
    var amount: String? = null
    var callbackurl: String? = null

    // 결제 완료 후
    var user_coin = 0
    var price: String? = null
    var currency: String? = null
    var first_order: String? = null
    var order_id = 0
}