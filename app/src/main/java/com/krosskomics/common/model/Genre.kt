package com.krosskomics.common.model

import com.krosskomics.common.data.DataGenre
import java.util.*

class Genre {
    var msg: String? = null
    var retcode: String? = null
    var list: ArrayList<DataGenre>? = null
}