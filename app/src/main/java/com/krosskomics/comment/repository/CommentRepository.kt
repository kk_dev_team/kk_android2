package com.krosskomics.comment.repository

import android.content.Context
import com.krosskomics.common.model.Comment
import com.krosskomics.common.repository.CommonRepository
import com.krosskomics.util.CODE
import com.krosskomics.util.CommonUtil
import com.krosskomics.util.ServerUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class CommentRepository : CommonRepository(){
    // comment 리스트, 신고
//    t	list
//    sid	시리즈 아이디
//    eid	에피소드 아이디
//    s	sort
//    page	페이지
//    c	댓글 내용 (300자 이내)
//    p	댓글 번호 (list 상 seq)
//    r	신고 구분
    fun requestMain(context: Context, type: String, sid: String, eid: String? = "", sort: String, page: Int,
                    c: String?, p: String?, r: String?) {
        val api: Call<Comment> = ServerUtil.service.getCommentList(
            CommonUtil.read(context, CODE.CURRENT_LANGUAGE, "en"),
            type, sid, eid, sort, page, c, p, r
        )
        api.enqueue(object : Callback<Comment> {
            override fun onResponse(call: Call<Comment>, response: Response<Comment>) {
                mainLiveData.postValue(response.body());
            }

            override fun onFailure(call: Call<Comment>, t: Throwable) {
                mainLiveData.postValue(null)
            }
        })
    }
}