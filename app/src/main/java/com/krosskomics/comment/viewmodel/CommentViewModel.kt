package com.krosskomics.comment.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import com.krosskomics.comment.repository.CommentRepository
import com.krosskomics.common.viewmodel.BaseViewModel

class CommentViewModel(application: Application): BaseViewModel(application) {
    private val repository = CommentRepository()
    private val mainResponseLiveData = repository.getMainResponseLiveData()
    var type = "list"
    var sid = "0"
    var eid = ""
    var sort = "t"   //t:top, r:recent
    var seq = "0"
    var selectPosition = 0

    var c = ""
    var p = ""
    var r = ""

    // comment 리스트, 신고
//    t	list
//    sid	시리즈 아이디
//    eid	에피소드 아이디
//    s	sort
//    page	페이지
//    c	댓글 내용 (300자 이내)
//    p	댓글 번호 (list 상 seq)
//    r	신고 구분
    override fun requestMain() {
        repository.requestMain(getApplication(), type, sid, eid, sort, page, c, p, r)
    }

    override fun getMainResponseLiveData(): LiveData<Any> {
        return mainResponseLiveData
    }

    fun resetParemeter() {
        type = ""
        sid = ""
        eid = ""
        sort = ""
        c = ""
        p = ""
        r = ""
    }
}