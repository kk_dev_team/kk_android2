package com.krosskomics.settings.activity

import android.content.Intent
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.view.View
import com.krosskomics.R
import com.krosskomics.common.activity.ToolbarTitleActivity
import com.krosskomics.common.model.Default
import com.krosskomics.util.CODE
import com.krosskomics.util.CODE.PW_MIN_LENGTH
import com.krosskomics.util.CommonUtil
import com.krosskomics.util.ServerUtil
import kotlinx.android.synthetic.main.activity_change_pw.*
import kotlinx.android.synthetic.main.activity_change_pw.confirmButton
import kotlinx.android.synthetic.main.activity_change_pw.hideImageView
import kotlinx.android.synthetic.main.activity_change_pw_result.*
import kotlinx.android.synthetic.main.view_network_error.*
import retrofit2.Call
import retrofit2.Response

class ChangePwActivity : ToolbarTitleActivity() {
    private val TAG = "ChangePwActivity"

    override fun getLayoutId(): Int {
        return R.layout.activity_change_pw
    }

    override fun initTracker() {
        setTracker(getString(R.string.str_account))
    }

    override fun initLayout() {
        toolbarTitleString = getString(R.string.str_account)
        super.initLayout()

        initView()
    }

    override fun requestServer() {}

    private fun initView() {
        currentPwEditText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
            }

            override fun afterTextChanged(s: Editable) {
                confirmButton.isEnabled = (s.length >= PW_MIN_LENGTH)
            }
        })

        hideImageView.setOnClickListener {
            it.isSelected = !it.isSelected
            if (it.isSelected) {
                currentPwEditText.inputType = InputType.TYPE_CLASS_TEXT
            } else {
                currentPwEditText.inputType =
                    InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
            }
        }
        confirmButton.setOnClickListener {
            val currentPw = currentPwEditText.text.toString()
            if (currentPw.isNullOrEmpty()) return@setOnClickListener
            requestCheckPw(currentPw)
        }
    }

    private fun requestCheckPw(password: String) {
        val api = ServerUtil.service.checkCurrentPwApi(
            CommonUtil.read(context, CODE.CURRENT_LANGUAGE, "en"),
            t = "verify_pass", p = password)
        api.enqueue(object : retrofit2.Callback<Default> {
            override fun onResponse(call: Call<Default>, response: Response<Default>) {
                try {
                    if (response.isSuccessful) {
                        val item = response.body()
                        if ("00" == item?.retcode) {
                            val intent = Intent(context, ChangePwActivityResult::class.java)
                            intent.putExtra("currentPw", currentPwEditText.text.toString())
                            startActivity(intent)

                            finish()
                        } else {
                            if ("" != item?.msg) {
                                verifyPwEditText.isSelected = true
                                resultTextView.visibility = View.VISIBLE
                                resultTextView.text = item?.msg
                            }
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<Default>, t: Throwable) {
                hideProgress()
                try {
                    checkNetworkConnection(context, null, errorView)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }
}