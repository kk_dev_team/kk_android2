package com.krosskomics.settings.activity

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.krosskomics.R
import com.krosskomics.common.activity.ToolbarTitleActivity
import com.krosskomics.common.model.Default
import com.krosskomics.util.CODE
import com.krosskomics.util.CommonUtil
import com.krosskomics.util.ServerUtil
import kotlinx.android.synthetic.main.activity_change_nickname.*
import kotlinx.android.synthetic.main.view_network_error.*
import retrofit2.Call
import retrofit2.Response

class ChangeNickNameActivity : ToolbarTitleActivity() {
    private val TAG = "ChangeNickNameActivity"

    override fun getLayoutId(): Int {
        return R.layout.activity_change_nickname
    }

    override fun initTracker() {
        setTracker(getString(R.string.str_account))
    }

    override fun initLayout() {
        toolbarTitleString = getString(R.string.str_account)
        viewModel.listType = "change_nick"
        super.initLayout()

        initView()
    }

    override fun requestServer() {}

    private fun initView() {
        newNickNameEditText.hint = CommonUtil.read(context, CODE.LOCAL_Nickname, "")
        newNickNameEditText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
            }

            override fun afterTextChanged(s: Editable) {
                newNickNameEditText.isSelected = false
                resultTextView.visibility = View.GONE
                nicknameLengthTv.text = "${s.length} / 20"
                confirmButton.isEnabled = s.length in 6..20
            }
        })
        nicknameLengthTv.text = "0 / 20"
        confirmButton.setOnClickListener {
            // 닉네임 변경 api 호충
            requestChangeNickName(newNickNameEditText.text.toString())
            CommonUtil.downKeyboard(context, newNickNameEditText)
        }
    }

    private fun requestChangeNickName(nickName: String) {
        val api = ServerUtil.service.setUserProfileApi(
            CommonUtil.read(context, CODE.CURRENT_LANGUAGE, "en"),
            t = viewModel.listType, p = nickName)
        api.enqueue(object : retrofit2.Callback<Default> {
            override fun onResponse(call: Call<Default>, response: Response<Default>) {
                try {
                    if (response.isSuccessful) {
                        val item = response.body()
                        if ("00" == item?.retcode) {
                            CommonUtil.write(context, CODE.LOCAL_Nickname, nickName)
                            newNickNameEditText.hint = CommonUtil.read(context, CODE.LOCAL_Nickname, "")
                            if ("" != item?.msg) {
                                CommonUtil.showToast(item?.msg, context)
                            }
                        } else {
                            if ("" != item?.msg) {
                                newNickNameEditText.isSelected = true
                                resultTextView.visibility = View.VISIBLE
                                resultTextView.text = item?.msg
//                            CommonUtil.showToast(item?.msg, context)
                            }
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<Default>, t: Throwable) {
                try {
                    checkNetworkConnection(context, t, errorView)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }
}