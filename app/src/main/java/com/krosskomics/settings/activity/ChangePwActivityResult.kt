package com.krosskomics.settings.activity

import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.view.View
import com.krosskomics.R
import com.krosskomics.common.activity.ToolbarTitleActivity
import com.krosskomics.common.model.Default
import com.krosskomics.util.CODE
import com.krosskomics.util.CODE.PW_MIN_LENGTH
import com.krosskomics.util.CommonUtil
import com.krosskomics.util.ServerUtil
import kotlinx.android.synthetic.main.activity_change_email.*
import kotlinx.android.synthetic.main.activity_change_pw_result.*
import kotlinx.android.synthetic.main.activity_change_pw_result.confirmButton
import kotlinx.android.synthetic.main.activity_change_pw_result.resultTextView
import kotlinx.android.synthetic.main.view_network_error.*
import retrofit2.Call
import retrofit2.Response

class ChangePwActivityResult : ToolbarTitleActivity() {
    private val TAG = "ChangePwActivityResult"

    var currentPw: String? = ""

    override fun getLayoutId(): Int {
        return R.layout.activity_change_pw_result
    }

    override fun initTracker() {
        setTracker(getString(R.string.str_account))
    }

    override fun initLayout() {
        toolbarTitleString = getString(R.string.str_account)
        viewModel.listType = "change_pass"
        super.initLayout()

        initView()
    }

    override fun requestServer() {}

    override fun initModel() {
        currentPw = intent.getStringExtra("currentPw")
        super.initModel()
    }

    private fun initView() {
        newPwEditText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
            }

            override fun afterTextChanged(s: Editable) {
                if (verifyPwEditText.isSelected) verifyPwEditText.isSelected = false
                resultTextView.visibility = View.GONE
                confirmButton.isEnabled = (s.length >= PW_MIN_LENGTH &&
                        verifyPwEditText.text.toString().length >= PW_MIN_LENGTH)
            }
        })

        verifyPwEditText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
            }

            override fun afterTextChanged(s: Editable) {
                if (verifyPwEditText.isSelected) verifyPwEditText.isSelected = false
                resultTextView.visibility = View.GONE
                confirmButton.isEnabled = (s.length >= PW_MIN_LENGTH &&
                        newPwEditText.text.toString().length >= PW_MIN_LENGTH)
            }
        })

        hideImageView.setOnClickListener {
            it.isSelected = !it.isSelected
            if (it.isSelected) {
                newPwEditText.inputType = InputType.TYPE_CLASS_TEXT
            } else {
                newPwEditText.inputType =
                    InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
            }
        }
        confirmButton.setOnClickListener {
            if (newPwEditText.text.toString().trim() != verifyPwEditText.text.toString().trim()) {
                verifyPwEditText.isSelected = true
                resultTextView.visibility = View.VISIBLE
                return@setOnClickListener
            }
            // 비밀번호 변경 api 호충
            requestChangePw(verifyPwEditText.text.toString(), currentPw ?: "")
        }
    }

    private fun requestChangePw(password: String, currentPassword: String) {
        val api = ServerUtil.service.setUserProfileApi(
            CommonUtil.read(context, CODE.CURRENT_LANGUAGE, "en"),
            t = viewModel.listType, p = password, c = currentPassword)
        api.enqueue(object : retrofit2.Callback<Default> {
            override fun onResponse(call: Call<Default>, response: Response<Default>) {
                try {
                    if (response.isSuccessful) {
                        val item = response.body()
                        if ("00" == item?.retcode) {
                            if (item.msg?.isNotEmpty() == true){
                                CommonUtil.showToast(item.msg, context)
                            }
                            finish()
                        } else {
                            if ("" != item?.msg) {
                                verifyPwEditText.isSelected = true
                                resultTextView.visibility = View.VISIBLE
                                resultTextView.text = item?.msg
                            }
                        }
                        hideProgress()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    hideProgress()
                }
            }

            override fun onFailure(call: Call<Default>, t: Throwable) {
                hideProgress()
                try {
                    checkNetworkConnection(context, null, errorView)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }
}