package com.krosskomics.settings.activity

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.krosskomics.R
import com.krosskomics.common.activity.ToolbarTitleActivity
import com.krosskomics.common.model.Default
import com.krosskomics.util.CODE
import com.krosskomics.util.CommonUtil
import com.krosskomics.util.ServerUtil
import kotlinx.android.synthetic.main.activity_change_email.*
import kotlinx.android.synthetic.main.activity_change_email.confirmButton
import kotlinx.android.synthetic.main.activity_change_email.resultTextView
import kotlinx.android.synthetic.main.activity_change_nickname.*
import kotlinx.android.synthetic.main.view_network_error.*
import retrofit2.Call
import retrofit2.Response

class ChangeEmailActivity : ToolbarTitleActivity() {
    private val TAG = "ChangeEmailActivity"

    override fun getLayoutId(): Int {
        return R.layout.activity_change_email
    }

    override fun initTracker() {
        setTracker(getString(R.string.str_account))
    }

    override fun initLayout() {
        toolbarTitleString = getString(R.string.str_account)
        viewModel.listType = "change_email"
        super.initLayout()

        initView()
    }

    override fun requestServer() {}

    private fun initView() {
        newEmailEditText.hint = CommonUtil.read(context, CODE.LOCAL_email, "")
        newEmailEditText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
            }

            override fun afterTextChanged(s: Editable) {
                if (newEmailEditText.isSelected) newEmailEditText.isSelected = false
                resultTextView.visibility = View.GONE
                confirmButton.isEnabled = CommonUtil.emailCheck(s.toString()) && s.length >= 6
            }
        })
        confirmButton.setOnClickListener {
            if (newEmailEditText.text.isNullOrEmpty()) return@setOnClickListener
            // 이메일 변경 api 호충
            requestChangeProfile(newEmailEditText.text.toString())
            CommonUtil.downKeyboard(context, newEmailEditText)
        }
    }

    private fun requestChangeProfile(email: String) {
        val api = ServerUtil.service.setUserProfileApi(
            CommonUtil.read(context, CODE.CURRENT_LANGUAGE, "en"),
            t = viewModel.listType, p = email)
        api.enqueue(object : retrofit2.Callback<Default> {
            override fun onResponse(call: Call<Default>, response: Response<Default>) {
                try {
                    if (response.isSuccessful) {
                        val item = response.body()
                        if ("00" == item?.retcode) {
                            CommonUtil.write(context, CODE.LOCAL_email, email)
                            newEmailEditText.hint = CommonUtil.read(context, CODE.LOCAL_email, "")
                            CommonUtil.showToast(item.msg, context)
                        } else {
                            if ("" != item?.msg) {
                                newEmailEditText.isSelected = true
                                resultTextView.visibility = View.VISIBLE
                                resultTextView.text = item?.msg
                            }
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<Default>, t: Throwable) {
                try {
                    checkNetworkConnection(context, t, errorView)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }
}