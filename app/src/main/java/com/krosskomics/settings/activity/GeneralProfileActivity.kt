package com.krosskomics.settings.activity

import android.view.View
import com.krosskomics.R
import com.krosskomics.common.activity.ToolbarTitleActivity
import com.krosskomics.util.CODE
import com.krosskomics.util.CommonUtil
import kotlinx.android.synthetic.main.activity_general_profile.*

class GeneralProfileActivity : ToolbarTitleActivity() {
    private val TAG = "GeneralProfileActivity"

    override fun getLayoutId(): Int {
        return R.layout.activity_general_profile
    }

    override fun initTracker() {
        setTracker(getString(R.string.str_account))
    }

    override fun initLayout() {
        toolbarTitleString = getString(R.string.str_account)
        super.initLayout()

        initView()
    }

    override fun requestServer() {}

    private fun initView() {
        nicknameTextView.text = CommonUtil.read(context, CODE.LOCAL_Nickname, "")
        emailTextView.text = CommonUtil.read(context, CODE.LOCAL_email, "")
        languageTextView.text = CommonUtil.read(context, CODE.CURRENT_LANGUAGE, "")
        snsView.visibility = View.VISIBLE
        when (CommonUtil.read(context, CODE.LOCAL_loginType, "")) {
            CODE.LOGIN_TYPE_FACEBOOK -> {
                linkedAccountImageView.setImageResource(R.drawable.kk_facebook_profile)
                linkedAccountTextView.text = CommonUtil.read(context, CODE.Local_oprofile, "")
            }
            CODE.LOGIN_TYPE_GOOGLE -> {
                linkedAccountImageView.setImageResource(R.drawable.kk_google_profile)
                linkedAccountTextView.text = CommonUtil.read(context, CODE.Local_oprofile, "")
            }
            else -> {
                snsView.visibility = View.GONE
            }
        }
    }
}