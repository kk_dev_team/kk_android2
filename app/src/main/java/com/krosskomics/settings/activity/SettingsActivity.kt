package com.krosskomics.settings.activity

import android.content.Intent
import android.graphics.Color
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.krosskomics.KJKomicsApp
import com.krosskomics.KJKomicsApp.Companion.LATEST_APP_VERSION_CODE
import com.krosskomics.R
import com.krosskomics.common.activity.ToolbarTitleActivity
import com.krosskomics.common.model.Default
import com.krosskomics.home.activity.MainActivity
import com.krosskomics.notice.activity.NoticeActivity
import com.krosskomics.util.CODE
import com.krosskomics.util.CommonUtil
import com.krosskomics.util.CommonUtil.logout
import com.krosskomics.util.CommonUtil.read
import com.krosskomics.util.CommonUtil.showToast
import com.krosskomics.util.ServerUtil.service
import kotlinx.android.synthetic.main.activity_mynews.*
import kotlinx.android.synthetic.main.activity_settings.*
import kotlinx.android.synthetic.main.view_network_error.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SettingsActivity : ToolbarTitleActivity() {
    private val TAG = "SettingsActivity"

    override fun getLayoutId(): Int {
        return R.layout.activity_settings
    }

    override fun initTracker() {
        setTracker(getString(R.string.str_settings))
    }

    override fun initLayout() {
        toolbarTitleString = getString(R.string.str_settings)
        super.initLayout()

        setLoginSignView()
        setAppVersionView()
        initMainView()
    }

    override fun requestServer() {}

    private fun setLoginSignView() {
        generalProfileView.setOnClickListener(this)
        changeNicknameView.setOnClickListener(this)
        changeEmailView.setOnClickListener(this)
        deleteAccountView.setOnClickListener(this)
        faqView.setOnClickListener(this)
        if (read(context, CODE.LOCAL_loginYn, "N").equals("Y", ignoreCase = true)) {
            loginSignupView.visibility = View.GONE
            if (read(context, CODE.LOCAL_loginType, "") == CODE.LOGIN_TYPE_KROSS) {
                changePwView.visibility = View.VISIBLE
                changePwUnderline.visibility = View.VISIBLE
                changePwView.setOnClickListener(this)
            } else {
                changePwView.visibility = View.GONE
                changePwUnderline.visibility = View.GONE
            }
        } else {
            accountLoginView.visibility = View.GONE
            notificationView.visibility = View.GONE
            notificationUnderbar.visibility = View.GONE
            loginSignupView.visibility = View.VISIBLE
            loginSignupView.setOnClickListener(this)
        }
    }

    private fun setAppVersionView() {
        try {
            var spannableString = SpannableString(KJKomicsApp.INIT_SET.app_version)
            spannableString.setSpan(UnderlineSpan(), 0, spannableString?.length, 0)
            latestVerTextView.text = spannableString
//            if (LATEST_APP_VERSION_CODE <= CommonUtil.getVersionCode(context)) {
//                latestVerTextView.setTextColor(Color.parseColor("#c6c6c6"))
//            } else {
//                latestVerTextView.setTextColor(Color.BLACK)
//            }
            spannableString = SpannableString(CommonUtil.getAppVersion(context))
            spannableString.setSpan(UnderlineSpan(), 0, spannableString.length, 0)
            currentVerTextView.text = spannableString

            latestVerTextView.setOnClickListener {
//                if (LATEST_APP_VERSION_CODE <= CommonUtil.getVersionCode(context)) {
//                    return@setOnClickListener
//                }
                CommonUtil.moveAppMarket(context, CODE.MARKET_URL)
//                finish()
//                // 팝업띄움
//                showUpdateAlert()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun initMainView() {
        pushNotiSwitch.isChecked = read(context, CODE.LOCAL_RECIEVE_PUSH, "1") == "1"
        pushNotiSwitch.setOnCheckedChangeListener { _, isCheck ->
            if (isCheck) {
                requestPushNoti("S")
            } else {
                requestPushNoti("C")
            }
        }
    }

    private fun requestDeleteAccount() {
        val api = service.setAccountKross(
            read(context, CODE.CURRENT_LANGUAGE, "en"),
            "delete_account", null
        )
        api.enqueue(object : Callback<Default> {
            override fun onResponse(
                call: Call<Default>,
                response: Response<Default>
            ) {
                try {
                    if (response.isSuccessful) {
                        val item = response.body()
                        if ("00" == item!!.retcode) {
                            if ("" != item.msg) {
                                showToast(item.msg, context)
                            }
                            logout(context)
                            val intent = Intent(context, MainActivity::class.java)
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            startActivity(intent)
                        } else if ("203" == item.retcode) {
                            goLoginAlert(context)
                        } else {
                            if ("" != item.msg) {
                                showToast(item.msg, context)
                            }
                        }
                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<Default?>, t: Throwable) {
                try {
                    checkNetworkConnection(context, t, errorView)
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    /**
     * 버전 팝업
     */
    private fun showUpdateAlert() {
        try {
            val innerView =
                layoutInflater.inflate(R.layout.dialog_app_version, null)
            val dialog = initDialog(innerView)
            val tvMsg1 = innerView.findViewById<TextView>(R.id.currentVerTextView)
            val tvMsg2 = innerView.findViewById<TextView>(R.id.latestVerTextView)
            val btnConfirm =
                innerView.findViewById<TextView>(R.id.downloadTextView)

            tvMsg1.text = CommonUtil.getAppVersion(context)
            tvMsg2.text = CommonUtil.getAppVersion(context)

            btnConfirm.setOnClickListener {
                dialog.dismiss()
                CommonUtil.moveAppMarket(context, CODE.MARKET_URL)
                finish()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun showDeleteAccountAlert() {
        try {
            val innerView =
                layoutInflater.inflate(R.layout.dialog_default, null)
            val dialog = initDialog(innerView)
            val tvTitle = innerView.findViewById<TextView>(R.id.tv_title)
            val msgTextView = innerView.findViewById<TextView>(R.id.tv_msg)
            val btnConfirm =
                innerView.findViewById<Button>(R.id.btn_confirm)
            val btnCancel =
                innerView.findViewById<Button>(R.id.btn_cancel)
            tvTitle.text = getString(R.string.str_delete_account_question)
            msgTextView.text = getString(R.string.str_delete_account_notice)
            btnCancel.setOnClickListener { dialog.dismiss() }
            btnConfirm.setOnClickListener {
                dialog.dismiss()
                // 계정 삭제
                requestDeleteAccount()
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    override fun onClick(v: View?) {
        when(v?.id) {
            R.id.generalProfileView -> startActivity(Intent(context, GeneralProfileActivity::class.java))
            R.id.changeNicknameView -> startActivity(Intent(context, ChangeNickNameActivity::class.java))
            R.id.changeEmailView -> {
                startActivity(Intent(context, ChangeEmailActivity::class.java))
            }
            R.id.changePwView -> {
                if (CommonUtil.read(context, CODE.LOCAL_loginType, "") == CODE.LOGIN_TYPE_KROSS) {
                    startActivity(Intent(context, ChangePwActivity::class.java))
                } else {
                    CommonUtil.showToast(getString(R.string.msg_not_available_sns), context)
                }
            }
            R.id.deleteAccountView -> {
                showDeleteAccountAlert()
            }
            R.id.loginSignupView -> {
                goLoginAlert(context)
            }
            R.id.faqView -> {
                val intent = Intent(context, NoticeActivity::class.java)
                intent.putExtra("tabIndex", 1)
                startActivity(intent)
            }
        }
    }
}